<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataPayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pay', function (Blueprint $table) {

            $table->id();        
            $table->timestamp('fecha_transaccion')->useCurrent();
            $table->text('documento');
            $table->text('tipo_documento');
            $table->text('nombre');
            $table->text('apellido');
            $table->text('email');
            $table->text('numero');

            $table->text('referencia');
            $table->text('descripcion');

            $table->text('currency');
            $table->text('total');

            $table->text('valor_total');
            $table->integer('numero_transaccion');
            $table->text('entidad_recaudadora');
            $table->text('estado_transaccion');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pay');
    }
}
