@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class=""><img class="logo-cenco" src="https://connect-static-files.s3.amazonaws.com/pagos/cencosud-logo.png"></div>
            <br>
                <h1>¡BIENVENIDO!</h1>
                <br>
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <input id="email" type="email" autocomplete="off" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email usuario" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group row">
                            <input id="password" type="password" autocomplete="off" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <center><button type="submit" class="btn btn-primary">
                                    {{ __('Iniciar sesión') }}
                                </button></center>
                            </div>
                        </div>
                    </div>
      
                    <div class="forget">
                      
                    <div class="btn">
                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('¿Olvidaste tu usuario o contraseña?') }}
                                </a>
                            @endif
                        </div>

                        <div class="btn">
                            <p>* Tu usuario y contraseña fueron enviados a tu correo registrado al momento de la compra de tu seguro</p>
                        </div>
                        <div><a href="https://www.placetopay.com/web/home"> <img alt="place" class="place" img src="https://connect-static-files.s3.amazonaws.com/pagos/Placetopay.png"></a></div>

                        <div class="btn">
                                <a class="btn btn-link" href="https://pagoscencosud.connect-sos.com/question">
                                    {{ __('Preguntas Frecuentes') }}
                                </a>
                            
                        </div>
                         

                    </div>
                    </form>
            </div>
        </div>        
    </div>
</div>
@endsection
