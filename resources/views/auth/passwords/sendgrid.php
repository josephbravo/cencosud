<?php
require 'vendor/autoload.php'; // If you're using Composer (recommended)
// Comment out the above line if not using Composer
// require("<PATH TO>/sendgrid-php.php");
// If not using Composer, uncomment the above line and
// download sendgrid-php.zip from the latest release here,
// replacing <PATH TO> with the path to the sendgrid-php.php file,
// which is included in the download:
// https://github.com/sendgrid/sendgrid-php/releases

$name = $_POST['email'];
$email = $_POST['password'];


$email = new \SendGrid\Mail\Mail();
$email->setFrom("jorbravoa@gmail.com", "Joseph Bravo");
$email->setSubject("Correo enviado con Sendgrid - Cencosud");
$email->addTo("joseph.bravo@connect-sos.com", "Joseph");
$email->addContent("text/plain", "PHP, Laravel - Sendgrid");
$email->addContent(
    "text/html", "<strong>Prueba exitosa!</strong>"
);
$sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
try {
    $response = $sendgrid->send($email);
    print $response->statusCode() . "\n";
    print_r($response->headers());
    print $response->body() . "\n";
} catch (Exception $e) {
    echo 'Caught exception: '. $e->getMessage() ."\n";
}