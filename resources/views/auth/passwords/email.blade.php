@extends('layouts.app')

@section('content')
<div class="container-email">
    
        <div class="col-md-8">
            <div class="card-email">
                <div class="card-header"><img class="logo-cenco-email" src="https://connect-static-files.s3.amazonaws.com/pagos/cencosud-logo.png"></div>

                <div class="btn">

                <h1 class="h1-email">¿NECESITAS RECUPERAR TU CONTRASEÑA?</h1>
                </div>    
                    
                <div class="btn">
                <p>Introduce tu correo electrónico registrado en el proceso de compra de tu seguro y allí se te enviará tu contraseña.</p>
                </div>    

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-email" method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="correo-text">{{ __('Correo:') }} &nbsp</label>

                        <div class="">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                <button type="button" class="btn btn-success"><img src="https://connect-static-files.s3.amazonaws.com/pagos/Logo-Whatsapp.png" style="width: 20px; height:20px; float: left;" />Enviar Mensaje</button>
                            @enderror
                        </div>
                        </div>
                        <br>
                        <div class="form-group row mb-0">
                        <div class="button-email">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Enviar') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection