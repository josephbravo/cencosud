@extends('base.init')

@section('content')

<div class="bgsizeadu" id="contain">
<div class="container">
     <div class="row2">
          <div class="col-lg-3">
          </div>

          <div class="col-lg-1">
               <div class="img-adu">
                    <img class="adu-titulo" img src="https://connect-static-files.s3.amazonaws.com/pagos/adu_precio_1220.png">
               </div>
          </div>

          <div class="col-lg">
               <div class="img-adu">
                    <img class="adu-precio-res" img src="https://connect-static-files.s3.amazonaws.com/pagos/adu_precio_1220-white.png">
               </div>
          </div>

          <div class="col-lg-1">
          <div  class="table-prod">
          <center><div class="formulario">

               <h2><b>ADQUIERE TU SEGURO AQUÍ:</b></h2>
               <br>
               <div>
                    
               <center><button type="button" class="btn btn-primary">
               <a href="#openModal" class="a-white">COMPRAR 100% ONLINE</a>
               </button></center>
               </div>&nbsp
               <div>
               <center><button type="button" class="btn btn-primary">
               <a href="#openModalTwo" class="a-white">QUIERO QUE ME LLAMEN</a>
               </button></center>
               </div><center>
               <br>
               <div>
               <center>
               <p class="p-vig">La póliza inicia vigencia el 16 de cada mes si adquieres tu póliza entre el 1 y el 15, o el 1° del mes siguiente si la adquieres entre el 16 y último día de cada mes.
               <br>
               En el transcurso de 8 días hábiles estaremos enviando a tu correo electrónico la póliza y las condiciones particulares incluyendo las exclusiones y condiciones particulares del producto</p>
               </center>
               </div>
               <center>
               <br>
               <center>
                    
               <a href="https://www.placetopay.com/web/home"> <img class="place" img src="https://connect-static-files.s3.amazonaws.com/pagos/Placetopay.png"> </a>
               </center>
               <center>
               <br>
               <a href="https://pagoscencosud.connect-sos.com/question">Preguntas Frecuentes</a>
               </center>
                    </div>

          </div> 
          </div>
          </div>
     </div>
</div>

<div class="funnel-container">
<div class="funnel-fluid">
<div class="funnel-content-medical">

          <h2 class="h2-container">Nuestro producto Asistencia Médica Domiciliara busca acompañarte y acompañar a tu familia en los
          momentos más delicados desde la comodidad de tu casa en caso de accidente o enfermedad.</h2>

          <h2 class="h2-container-up">QUE CUBRE TU SEGURO ASISTENCIA MÉDICA DOMICILIARIA</h2>

<div class="content-opt">
     <div class="row">
          <div class="col-md-4">
               <div class="opt-adu">
               <img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/icono_asistencia_medica.svg">
               <p class="p-asist-opt">ASISTENCIA MÉDICA<br>DOMICILIARIA (ilimitado)</p>

               <p>Disposición de un médico para que<br> adelante la consulta en su domicilio.</p>
               </div>     
          </div>

          <div class="col-md-4">
                <div class="opt-adu">
               <img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/icono_asistencia_telefonica.svg">
               <p class="p-asist-opt">ORIENTACIÓN MÉDICA<br>TELEFÓNICA (ilimitado)</p>

               <p>Orientación médica básica las 24 horas<br> del día los 365 días del año.</p>
               </div> 
          </div>

          <div class="col-md-4">
               <div class="opt-adu">
               <img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/icono_asistencia_traslados.svg">
               <p class="p-asist-opt">TRASLADOS MÉDICOS<br>DE URGENCIAS (ilimitado)</p>

               <p>A criterio del médico tratante, la Compañía adelantará los contactos y coordinación para el traslado del asegurado, asumiendo los gastos de traslado hasta el centro hospitalario más cercano.</p>
               </div> 
          </div>
     </div>
</div>

</div>
</div>
</div>


<div class="funnel-container-plans">
<div class="funnel-fluid-plans">
<div class="funnel-content-medical-plans">

<h2 class="h2-container-up-2">PLANES Y TARIFAS</h2>
<h2 class="h2-container">Desde <b>$15.717</b> mensuales</h2>

<div class="content-plans">
     <div class="row">
     
          <div class="col-md-6">
               <div class="opt-plan">
               <img class="img-plan" img src="https://connect-static-files.s3.amazonaws.com/pagos/planes_1.jpg">

               </div>     
          </div>
          <div class="col-md-6">
                <div class="opt-plan-2">
               <img class="img-plan" img src="https://connect-static-files.s3.amazonaws.com/pagos/planes_2.jpg">
          </div>

     </div>
</div>

<h2 class="h2-container-up-2">NUESTRO SEGURO DE ASISTENCIA MÉDICA A DOMICILIO</h2>
<h2 class="h2-container">Una vida tan emocionante merece cuidarse.</h2>
     </div>
     </div>
</div>
</div>


<div class="funnel-container-last">
<div class="funnel-fluid">
<div class="funnel-content-medical-last">
     
     <h2 class="h2-container-up-2">NO CUBRE</h2>
     <h2 class="h2-container">La asistencia y gastos por enfermedades o estados patológicos producidos por la ingestión voluntaria de drogas,
     sustancias tóxicas, ingesta de alcohol, narcóticos o medicamentos adquiridos sin prescripción médica.</h2>

     <h2 class="h2-container-up-2">CON EL RESPALDO DE</h2>

     <img class="liberty" img src="https://connect-static-files.s3.amazonaws.com/pagos/respaldo_liberty.svg">


<div class="container-boton">
     <div class="row">
          <div class="col-md-2">
               <div class="opt-plan">
               <img class="gallager" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
               </div>     
          </div>

          <div class="col-md-8">
               <div class="opt-plan">
               <center><p class="cont-last-text">Aplican condiciones y restricciones en caso de tener alguna inquietud comunicarse a línea de atención de Seguros Cencosud.</p></center>
               </div>     
          </div>

          <div class="col-md-2">
               <div class="opt-plan">
               </div>     
          </div>
     </div>
</div>



<div id="openModal" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
          <br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">

          <div class="formulariotwo">
		<form method="post" action="{{route('adu.store')}}" onsubmit="return Validate()">
                    @csrf
                    
          <h1>ADQUIERE TU SEGURO AQUÍ</h1>

               <div class="btn-group">
          &nbsp&nbsp<div>
                    <input name="nombre" pattern="[A-Za-z ]*" title="Campo inválido" onkeypress="return validar(event)" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre" required>
                         
                    </div>&nbsp
                    <div>
                         
                    <input name="apellido" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Apellido" required>
                    </div>
               </div>

               <div class="form-group">     
               </div>

               <div class="btn-group">
               <p class="date-born">En qué fecha naciste?</p>
                    <div>
                    <input name="date" type="date" id="start" class="form-control" name="trip-start" min="1910-01-01" max="2002-12-31">
                    </div>
               </div>

               <div class="form-group">     
               </div>

               <div class="form-group">
                    <select id="choice" name="tipdocumento" onchange="choice2(this)"  class="form-control" placeholder="PRODUCTO - PRECIO" id="select_tipdoc" required>
                         <option value="">Tipo documento</option>
                         <option value="1">CC</option>
                         <option value="2">CE</option>
                         <option value="3">PAS</option>
                    </select>
               </div>

               <div class="form-group">
                    <input id="ident" pattern="[A-Za-z ]*[0-9]{5,12}" title="Campo inválido. Minimo 5 dígitos máximo 12 dígitos" maxlength="12" name="documento" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Numero documento" required>
               </div>

               <div class="form-group">
                    <select name="genero" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="">Genero</option>
                         <option value="Masculino">Masculino</option>
                         <option value="Femenino">Femenino</option>
                    </select>    
               </div>

               <div class="form-group">
                    <input name="eps" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput2" placeholder="EPS" required>
               </div>

               <div class="form-group">
                    <select name="afiliacion" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="">Tipo Afiliacion</option>
                         <option value="Beneficiario">Beneficiario</option>
                         <option value="Cotizante">Cotizante</option>
                    </select>
               </div>

               <div class="form-group">
                    <select id="reg" name="regimen" class="form-control" placeholder="Regimen" onchange="choice1(this)" required>
                         <option value="">Régimen</option>
                         <option value="1">Contributivo</option>
                         <option value="2">Subsidiado</option>                    
                    </select>    
               </div>

               <div class="form-group">
                    <input pattern="^[0-9]{7,10}$" title="Campo inválido. Minimo 7 dígitos máximo 10 dígitos" maxlength="10" name="celular" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Teléfono" required>
               </div>

               <div class="form-group">
                    <input name="email" type="email" class="form-control" id="formGroupExampleInput2" placeholder="Correo electrónico" required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>

               <div class="form-group">
                    <select name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="">Ciudad</option>
                         <option value="1">Bogotá-Cundinamarca</option>
                         <option value="2">Medellín-Antioquia</option>
                         <option value="3">Cali-Valle</option>
                         <option value="4">Cartagena-Bolivar</option>
                    </select>
               </div>

               <div class="form-group">
                    <input name="direccion" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu dirección de residencia" required>
               </div>

               <div class="form-group">
                    <select name="ocupacion" class="form-control" placeholder="Cuál es tu ocupación" required>
                         @foreach ($ocupacions as $ocupacion)
                              <option value="{{ $ocupacion->id }}">{{ $ocupacion->ocupacion }}</option>
                         @endforeach
                    </select>
               </div>

               <div class="form-group">
               <select id="select" name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                    <option value="0">Seleccione</option>
                    <option value="15717">Asistencia Medica Domiciliaria - A $15.717</option>
               </select>
                                   
               </div>
               
               <div class="btn-group">
                    
               <div>
                    <center>
                         <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
                    </center>
               </div>&nbsp
               <div>
                    <center>
                         <p class="p-terms2"> Al hacer click en el botón "COMPRAR 100% ONLINE" aceptas los <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Autorizaciones+Solicitud+Asistencia+Medica+Liberty.pdf"> terminos, condiciones</a>, <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Poli%CC%81tica+de+cookies+Cencosud.pdf">politica de cookies</a> y <a target="_blank" href="https://cencosud.zonic3.com/habeas-data">políticas de tratamiento de datos</a> de Seguros Cencosud </p>
                    <center>
               </div>
               
               </div>
               
               <center><div class="btn-group">
               <div>
                         
                    <button type="submit" class="btn btn-primary">
                    <a class="a-white">Enviar solicitud</a>
                    </button>
                         
                    </div>&nbsp
               </div></center>

               <div class="form-group">
               <img class="gallager-form" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
               </div>

                    
                    </div>
               </form>
               </div>
               </div>
	</div>
</div>

<div id="openModalTwo" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
          <br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">

          <div class="formulariotwo">
		<form method="post" action="{{route('digital.store')}}" onsubmit="">
                    @csrf
                    
          <h1>SOLICITA TU ASESORÍA</h1>
          <div class="btn-group">
               &nbsp&nbsp<div>
                         <input name="nombre" onkeypress="return validar(event)" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre" required>
                    </div>&nbsp
                    <div>
                         
                              <input name="apellido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Apellido" required>
                         </center>
                    </div>
          </div>&nbsp

               <div class="form-group">
                    <input oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" name="telefono" type="number" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu número celular" required>
               </div>
               <div class="form-group">
                    <input name="email" type="email" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu correo electrónico" required required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>
               <div class="form-group">
                    <input oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" name="documento" type="number" class="form-control" id="formGroupExampleInput2" placeholder="Número de identificación" required>
               </div>
               <div class="form-group">
               <select id="select2" name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                    <option value="0">Seleccione</option>
                    <option value="15717">Asistencia Medica Domiciliaria - A $15.717</option>
               </select>&nbsp

               <div class="btn-group">
                    
               <div>
                    <center>
                         <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
                    </center>
                    </div>&nbsp
               <div>
                    <center>
                         <p class="p-terms2"> Al hacer click en el botón "Enviar solicitud" aceptas los <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Autorizaciones+Solicitud+Asistencia+Medica+Liberty.pdf"> terminos, condiciones </a>, <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Poli%CC%81tica+de+cookies+Cencosud.pdf">politica de cookies</a> y <a target="_blank" href="https://cencosud.zonic3.com/habeas-data">políticas de tratamiento de datos</a> de Seguros Cencosud </p>
                    <center>
               </div>
               </div>
               <center><div class="btn-group">
               <div>
                    <button type="button" class="btn btn-primary">
                         <a class="a-white">Enviar solicitud</a>
                    </button>
                         
                    </div>&nbsp
               </div></center>

               <div class="form-group">
               <img class="gallager-form" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
               </div>
                                   
               </div>

          </form>
     </div>
     </div>
     </div>
     </div>


     <div id="openModalThree" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
          <br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">

          <div class="formulariotwo">
                    
          <h1>¡GRACIAS POR TU SOLICITUD!</h1>
          
               &nbsp&nbsp<div>
                         <h2 class="h2-orange">TE ESTAREMOS LLAMANDO EN EL TRANSCURSO DEL DÍA</h2>
                         
                    </div>&nbsp
                    <div>
                         <h2 class="h2-orange">EN EL SIGUIENTE HORARIO</h2>
                         </center>
                    </div>
                    <div class="opt">
                         <h2>Lunes a viernes<br>8:00 a.m a 5:00 p.m</h2>
                    </div>&nbsp
                    <div class="opt">
                         <h2>Sábados<br>8:00 a.m a 12:00 p.m</h2>
                    </div>&nbsp


                    <div class="content-opt">
               <div class="row">
                    <div class="col-md-1">
                         <div class="opt-h">
                         </div>     
                    </div>

                    <div class="col-md-9">
                         <div class="opt">
                         <center><a href="https://api.whatsapp.com/send?phone=573208899730&text=&source=&data=&app_absent="><img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/redes_whatsapp.svg">
                         <p class="p-asist-opt-tel">Si quieres asesoría  puedes comunicarte a nuestro WhatsApp</p></a></center>
                         </div> 
                    </div>

                    <div class="col-md-1">
                         <div class="opt-h">
                         </div> 
                    </div>
                    </div>
               </div>

     </div>
     </div>
     </div>
     </div>


</div>
</div>
</div>

<script type="text/javascript">
     function Validate()
     {
          var e = document.getElementById("select");
          var strUser = e.options[e.selectedIndex].value;

          var d = document.getElementById("reg");
          var strReg = d.options[d.selectedIndex].value;

          var t = document.getElementById("choice");
          var strUser3 = t.options[t.selectedIndex].value;

          var strUser1 = e.options[e.selectedIndex].text;

          if(strUser3==1){
               alert("Escogiste CC");
               return false;
          }

          if(strReg==2){
               alert("Escogiste régimen Subsidiado. Por condiciones del producto solo se permite Contributivo");
               return false;
          }

          if(strUser==0)
          {
               alert("Porfavor selecciona un valor");
               return false;
          }else{
               return true;
          }
     }
</script>

<script type="text/javascript">
     function Validate2()
     {
          var e = document.getElementById("select2");
          var strUser = e.options[e.selectedIndex].value;

          var strUser1 = e.options[e.selectedIndex].text;
          if(strUser==0)
          {
               alert("Porfavor selecciona un valor");
               return false;
          }else{
               return true;
          }
     }
</script>

<script type="text/javascript">
     function Validate_tipdoc()
     {
          var e = document.getElementById("select_tipdoc");
          var strUser = e.options[e.selectedIndex].value;

          var strUser1 = e.options[e.selectedIndex].text;
          if(strUser==0)
          {
               alert("Porfavor selecciona un valor");
               return false;
          }else{
               return true;
          }
     }
</script>

<script>
function choice1(select) {
     if (select.value==2)
     {
     alert("Escogiste régimen Subsidiado. Por condiciones del producto solo se permite Contributivo");
     }else{
     
     }
}
</script>


<script>
function choice2(select) {
     if (select.value==1)
     {
     alert("Escogiste CC");
     document.getElementById("ident").pattern="^[1-9][0-9]{3,9}$";
     document.getElementById("ident").title="Campo inválido. Minimo 4 dígitos máximo 10 dígitos";
     document.getElementById("ident").maxlength=10;
     /*document.getElementById("ident").type="number";*/
     }
     if (select.value==2)
     {
     alert("Escogiste CE");
     document.getElementById("ident").pattern="([a-zA-Z]{1,5})?[1-9][0-9]{3,7}$";
     document.getElementById("ident").title="Campo inválido. CE: Letras y dígitos";
     document.getElementById("ident").maxlength=10;
     
     }
     if (select.value==3)
     {
     alert("Escogiste PAS");
     document.getElementById("ident").pattern="^[a-zA-Z0-9_]{4,16}$";
     document.getElementById("ident").title="Campo inválido. PAS: Letras y dígitos";
     document.getElementById("ident").maxlength=10;
     
     }
     
     else{
     
     }
}
</script>

@endsection