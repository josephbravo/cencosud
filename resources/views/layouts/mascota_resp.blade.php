@extends('base.init')

@section('content')

<main>
     <section id="banner">
          <img class="bannermovile" src="https://connect-static-files.s3.amazonaws.com/pagos/banner_mascotas-mobile.jpg">
          <img class="bannerbig" src="https://connect-static-files.s3.amazonaws.com/pagos/iframe_Cencosud_mascotas_1220.jpg">
          <div class="contenedor">
          <img class="price-mst" src="https://connect-static-files.s3.amazonaws.com/pagos/mascotas_precio_aling.png">
          </div>
     </section>

     <section id="adquiere">
     <div class="formulario-mascota-resp">

     <h2><b>ADQUIERE TU SEGURO AQUÍ:</b></h2><br>
     <div>
          <center>
          <button type="submit" class="btn btn-primary">
          <a href="#openModal" class="a-white"><b>COMPRAR 100% ONLINE</b></a>
          </button>
          </center>
     </div>

     <div>
          <center>
          <button type="submit" class="btn btn-primary">
          <a href="#openModalTwo" class="a-white"><b>QUIERO QUE ME LLAMEN</b></a>
          </button>
          </center>
     </div><br>
     
     <div>
          <center>
          <p class="p-vig">La póliza inicia vigencia una vez se efectué el pago de la primera cuota y las Asistencias para tu Mascotas quedan activas a las 72 horas de iniciado su Seguro.
          <br>
          En el transcurso de 8 días hábiles estaremos enviando a tu correo electrónico la póliza y las condiciones particulares incluyendo las exclusiones y condiciones particulares del producto.</p>
          </center>
     </div>

     <center><br>
     <a href="https://www.placetopay.com/web/home"> <img class="place" img src="https://connect-static-files.s3.amazonaws.com/pagos/Placetopay.png"> </a>
     </center>
     <center><br>
     
     <a href="https://pagoscencosud.connect-sos.com/question">Preguntas Frecuentes</a>
     </center>
     </div>

     <h2 class="h2-container-mst">Por que ellos son integrantes importantes de la familia, pensamos en un seguro que pueda cubrirlo en los momentos donde más necesitas ayuda,<br>
               en caso de una enfermedad de tu peludo, de un viaje tuyo y que no tengas donde dejarlo, este producto esta pensado para ti.</h2>

     <h2 class="h2-container-up" id="slide1">RECIBE ASISTENCIAS PARA TUS PELUDOS COMO:</h2>
     
     <div class="container-mst">

          <ul class="slider">

          <li id="slide1">
          <div class="opt-adu-resp">
               <center><img class="icon-adu1" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_gastos_medicos.svg" alt="" />
               <p class="p-asist-opt">GASTOS MÉDICOS</p>
               <p>Por enfermedad o accidente hasta por $500.000 pesos.</p>
     
          </div> 
          </li>
          <li id="slide2">
          <div class="opt-adu-resp">
               <center><img class="icon-adu1" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_paseo_caninos.svg" alt="" />
               <p class="p-asist-opt">HASTA $150.000</p>
               <p>En servicio de paseo de caninos en caso de hospitalización del asegurado.</p>
               
               </div> 
          </li>
          <li id="slide3">
          <div class="opt-adu-resp">
               <center><img class="icon-adu1" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_consulta_veterinaria.svg" alt="" />
               <p class="p-asist-opt">HASTA $100.000</p>
               <p>Por consulta médica veterinaria</p><br>
               </div>
          </li>
          <li id="slide4">
          <div class="opt-adu-resp">
               <center><img class="icon-adu1" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_controles_domicilio.svg" alt="" />
               <p class="p-asist-opt">CONTROLES MÉDICOS<br>A DOMICILIO</p>
               <p>Hasta $60.000 por evento.</p>
               </div>
          </li>
          </ul>

     </div>

     <div class="menu-slide">

          <ul class="menu">
          <li>
               <a href="#slide1" class="slidec">1</a>
          </li>
          <li>
               <a href="#slide2">2</a>
          </li>
          <li>
               <a href="#slide3">3</a>
          </li>
          <li>
               <a href="#slide4">4</a>
          </li>

          </ul>
     </div>
     </section>

     <section id="section-imgs-adu"> <!-- class: funnel-container-plans -->
     <h2 class="p-asist-opt" id="slide5" id="slide6">ADEMÁS RECIBIRÁS COBERTURAS PARA TI COMO</h2>
     <h2 class="h2-container-up-2">PLANES Y TARIFAS</h2>
     <h2 class="h2-container">Desde <b>$34.200</b> mensuales</h2><br>

     <div class="container-mst-2">

          <ul class="slider">

          <li id="slide5">
          <div class="opt-mst-resp">
               <center>              
               <p class="p-asist-opt" id="plana">PLAN A</p><hr class="hr-plan">
               <p>Muerte Accidental</p>
               <p class="text-tariff">$5’273.500</p><hr class="hr-plan">
               <p>Desmembración por accidente</p>
               <p class="text-tariff">$5’273.500</p><hr class="hr-plan">
               <p>ITP por accidente</p>
               <p class="text-tariff">$5’273.500</p><hr class="hr-plan">
               <p>Programa Premium de asistencia<br>
               a mascotas (Axa)<br>
               Incluida</p><hr class="hr-plan">
               <p>Prima mensual básico</p>
               <p class="text-tariff">$34.200</p>
               <hr class="hr-plan">
               <p>Responsabilidad Civil Extracontractual</p>
               <p class="text-tariff">$5’273.500</p><hr class="hr-plan">
               <p>RCE</p>
               <p class="text-tariff">$8.900</p>  
               </center>
     
          </div> 
          </li>
          <li id="slide6">
          <div class="opt-mst-resp">
               <center>               
               <p class="p-asist-opt" id="planb">PLAN B</p><hr class="hr-plan">
               <p>Muerte Accidental</p>
               <p class="text-tariff">$10'547.100</p><hr class="hr-plan">
               <p>Desmembración por accidente</p>
               <p class="text-tariff">$10'547.100</p><hr class="hr-plan">
               <p>ITP por accidente</p>
               <p class="text-tariff">$10'547.100</p><hr class="hr-plan">
               <p>Programa Premium de asistencia<br>a mascotas (Axa)</p>
               <p>Incluida</p><hr class="hr-plan">
               <p>Prima mensual básico</p>
               <p class="text-tariff">$36.900</p><hr class="hr-plan">
               <p>Responsabilidad Civil Extracontractual</p>
               <p class="text-tariff">$5’273.500</p><hr class="hr-plan">
               <p>RCE</p>
               <p class="text-tariff">$8.900</p></center>
               
               </div> 
          </li>
          <li id="slide7">
          <div class="opt-mst-resp">
               <center><p class="p-asist-opt" id="planc">PLAN C</p><hr class="hr-plan">
               <p>Muerte Accidental</p>
               <p class="text-tariff">$15'820.600</p><hr class="hr-plan">
               <p>Desmembración por accidente</p>
               <p class="text-tariff">$15'820.600</p><hr class="hr-plan">
               <p>ITP por accidente</p>
               <p class="text-tariff">$15'820.600</p><hr class="hr-plan">
               <p>Programa Premium de asistencia<br>a mascotas (Axa)</p>
               <p>Incluida</p><hr class="hr-plan">
               <p>Prima mensual básico</p>
               <p class="text-tariff">$39.500</p>
               <hr class="hr-plan">
               <p>Responsabilidad Civil Extracontractual</p>
               <p class="text-tariff">$5’273.500</p><hr class="hr-plan">
               <p>RCE</p>
               <p class="text-tariff">$8.900</p></center>
               </div>
          </li>

          </ul>

     </div>

     <div class="menu-slide">

          <ul class="menu">
          <li>
               <a href="#slide5">A</a>
          </li>
          <li>
               <a href="#slide6">B</a>
          </li>
          <li>
               <a href="#slide7">C</a>
          </li>


          </ul>
     </div>
     </section>

     <section id="section-imgs-mst">
     <div class="container-plans-adu"> <!-- class: content-plans -->
     <h2 class="h2-container-up-2">RESPONSABILIDAD CIVIL</h2><br>

     <article> <!-- opt-plan   img: img-plan-->
          <center><img class="img-mst-section" img src="https://connect-static-files.s3.amazonaws.com/pagos/responsabilidad_civil_1.jpg"></center>
     </article>   

     <article> <!-- opt-plan2 -->
          <center><img class="img-mst-section" img src="https://connect-static-files.s3.amazonaws.com/pagos/responsabilidad_civil_2.jpg"></center>
     </article>

     </div><br>

     <h2 class="h2-container-up-2" id="slide8">ASISTENCIA</h2>
     <h2 class="h2-container">A continuación encontrará las asistencias disponibles para este seguro</h2><br>

     <div class="container-mst-3">

          <ul class="slider">

          <li id="slide8">
          <div class="opt-adu-resp">
               <center><img class="icon-mst1" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_asistencia_domicilio_mst.svg" alt="" />
               <p><br>• Hasta $250.000 en domicilio de medicamentos para tu mascota (no incluye el valor de los medicamentos).</p>
     
          </div> 
          </li>
          <li id="slide9">
          <div class="opt-adu-resp">
               <center><img class="icon-mst1" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_hotel_mst.svg" alt="" />
               <p><br>• Hasta $250.000 para hotel veterinario en caso de que el asegurado viaje o sea hospitalizado</p><br>
               </div> 
          </li>
          <li id="slide10">
          <div class="opt-adu-resp">
               <center><img class="icon-mst1" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_exequias_mst.svg" alt="" />
               <p><br>• Asistencia exequial para mascotas hasta por $450.000.</p>
               </div><br>
          </li>
          <li id="slide11">
          <div class="opt-adu-resp">
               <center><img class="icon-mst1" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_vacunas_mst.svg" alt="" />
               <p><br>• Hasta $100.000 por asistencia perdida de mascotas.
               <br>• Informe de vacunación.</p>

               </div>
          </li>
          </ul>

     <div class="menu-slide-3">

          <ul class="menu">
          <li>
               <a href="#slide8">1</a>
          </li>
          <li>
               <a href="#slide9">2</a>
          </li>
          <li>
               <a href="#slide10">3</a>
          </li>
          <li>
               <a href="#slide11">4</a>
          </li>

          </ul>
     </div>
     </section>

     <h2 class="h2-container-up-2">CON EL RESPALDO DE</h2>
     <center><img class="liberty" img src="https://connect-static-files.s3.amazonaws.com/pagos/logos_chubb.svg"></center><br>
     <center><div class="opt-plan">
     <p>Aplican condiciones y restricciones en caso de tener alguna inquietud comunicarse a línea de atención de Seguros Cencosud.</p></center>
     </div> 

     <article>
     <img class="gallager" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
     </article> 
     </section>

<!-- OpenModal 1 -->     
     
     <div id="openModal" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a><br>
     <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">

     <div class="formulariotwo">
     <form method="post" action="{{route('mascota.store')}}" onsubmit="return value()">@csrf
                    
          <h1>ADQUIERE TU SEGURO AQUÍ</h1>

          <div class="btn-group">&nbsp&nbsp
               <div>
                    <input name="nombre" pattern="[A-Za-z ]*" title="Campo inválido" onkeypress="return validar(event)" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre" required>
                         
               </div>&nbsp
               <div>
                         
                    <input name="apellido" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Apellido" required>
               </div>
               </div>

               <div class="form-group">     
               </div>

               <div class="btn-group">
               <p class="date-born">En qué fecha naciste?</p>
                    <div>
                    <input name="date" type="date" id="start" max="{{ $max }}" min="{{ $min }}" class="form-control" name="trip-start">
                    </div>
               </div>

               <div class="form-group">     
               </div>

               <div class="form-group">
               <select name="tipdocumento" class="form-control" onchange="choice_doc(this)"  placeholder="PRODUCTO - PRECIO" required>
                    <option value="-">Tipo documento</option>
                    <option value="CC">CC</option>
                    <option value="CE">CE</option>
                    <option value="PPN">PAS</option>
               </select>
               </div>

               <div class="form-group">
                    <input id="ident" pattern="[A-Za-z ]*[0-9]{5,12}" title="Campo inválido. Minimo 5 dígitos máximo 10 dígitos" maxlength="10" name="documento" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Numero documento" required>
               </div>

               <div class="form-group">
               <select name="genero" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                    <option value="">Genero</option>
                    <option value="Masculino">Masculino</option>
                    <option value="Femenino">Femenino</option>
               </select>    
               </div>

               <div class="form-group">
                    <input pattern="^[0-9]{7,10}$" title="Campo inválido. Minimo 7 dígitos máximo 10 dígitos" maxlength="10" name="celular" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Teléfono" required>
               </div>

               <div class="form-group">
                    <input name="email" type="email" class="form-control" id="formGroupExampleInput2" placeholder="Correo electrónico" required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>

               <div class="form-group">
               <select name="ciudad" class="form-control" placeholder="Ciudad" required>
                    <option value="">Ciudad</option>
                    <option value="1">Bogotá-Cundinamarca</option>
                    <option value="2">Medellín-Antioquia</option>
                    <option value="3">Cali-Valle</option>
                    <option value="4">Cartagena-Bolivar</option>
               </select>
               </div>

               <div class="form-group">
                    <input name="direccion" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu dirección de residencia" required>
               </div>

               <div class="form-group">
               <select name="profesion" class="form-control" placeholder="Cuál es tu ocupación" required>
               <option value="0">Profesión</option>
                    @foreach ($ocupacions as $ocupacion)
               <option value="{{ $ocupacion->id }}">{{ $ocupacion->ocupacion }}</option>
                    @endforeach
               </select>
               </div>

               <div class="form-group">
               <select name="tipomascota" class="form-control" placeholder="Tipo de mascota" required>
                    <option value="">Tipo de mascota</option>
                    <option value="Perro">Perro</option>
                    <option value="Gato">Gato</option>
               </select>
               </div>

               <div class="form-group">
                    <input name="razamascota" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Raza de tu mascota" required>
               </div>

               <div class="form-group">
                    <input name="nombremascota" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Nombre de tu mascota" required>
               </div>
               <div class="form-group">
               <select name="edadmascota" class="form-control" placeholder="Edad de tu mascota" required>
                    <option value="">Edad de tu mascota</option>
                    <option value="3 meses">3 meses</option>
                    <option value="4 meses">4 meses</option>
                    <option value="5 meses">5 meses</option>
                    <option value="6 meses">6 meses</option>
                    <option value="7 meses">7 meses</option>
                    <option value="8 meses">8 meses</option>
                    <option value="9 meses">9 meses</option>
                    <option value="10 meses">10 meses</option>
                    <option value="11 meses">11 meses</option>
                    <option value="1 año">1 año</option>
                    <option value="2 años">2 años</option>
                    <option value="3 años">3 años</option>
                    <option value="4 años">4 años</option>
                    <option value="5 años">5 años</option>
                    <option value="6 años">6 años</option>
                    <option value="7 años">7 años</option>
                    <option value="8 años">8 años</option>
                    <option value="9 años">9 años</option>
                    <option value="11 años">11 años</option>
                    <option value="12 años">12 años</option>
               </select>
               </div>

               <div class="form-group">
                    <select id="select" name="plan" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="0">Seleccione</option>
                         <option value="33631">Mascota Premium - A $34.200</option>
                         <option value="36226">Mascota Premium - B $36.900</option>
                         <option value="38821">Mascota Premium - C $39.500</option>
                         <option value="42298">Mascota Premium + RC - A $43.100</option>
                         <option value="44893">Mascota Premium + RC - B $45.800</option>
                         <option value="47488">Mascota Premium + RC - C $48.400</option>
                    </select>
               </div>
               <div class="btn-group">
                    
               <div>
                    <center>
                         <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
                    </center>
               </div>&nbsp
               <div>
                    <center>
                         <p class="p-terms2"> Al hacer click en el botón "Enviar solicitud" aceptas los <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Autorizaciones+Solicitud+Mascota+Plus++Mejorado+Chubb.pdf">terminos, condiciones</a>, <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Poli%CC%81tica+de+cookies+Cencosud.pdf">politica de cookies</a> y <a target="_blank" href="https://cencosud.zonic3.com/habeas-data">políticas de tratamiento de datos</a> de Seguros Cencosud </p>
                    <center>
               </div>
               </div>
               <center><div class="btn-group">
               <div>
                         
                    <button type="submit" class="btn btn-primary">
                         {{ __('Enviar solicitud') }}
                    </button>
               
                    </div>&nbsp
               </div></center>

               <div class="form-group">
               <img class="gallager-form" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
               </div>   
               </div>
               </form>
               </div>
          </div>
	</div>
</div>

<!-- OpenModal 2 -->

<div id="openModalTwo" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
          <br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">

          <div class="formulariotwo">
		<form method="post" action="{{route('digital.store')}}" onsubmit="return validate2()">@csrf

               <h1>SOLICITA TU ASESORÍA</h1>
               <div class="btn-group">&nbsp&nbsp
                    
               <div>
                    <input name="nombre" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre" required>
               </div>&nbsp
               <div>
                    <input name="apellido" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Apellido" required>
               </center>
               </div>
               </div>&nbsp

               <div class="form-group">
                    <input oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" name="telefono" type="number" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu número celular" required>
               </div>
               <div class="form-group">
                    <input name="email" type="email" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu correo electrónico" required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>
               <div class="form-group">
                    <input oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" name="documento" type="number" class="form-control" id="formGroupExampleInput2" placeholder="Número de identificación" required>
               </div>
               <div class="form-group">
               <select id="select2"  name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                    <option value="0">Seleccione</option>
                    <option value="33631">Mascota Premium - A $34.200</option>
                    <option value="36226">Mascota Premium - B $36.900</option>
                    <option value="38821">Mascota Premium - C $39.500</option>
               </select>&nbsp

               <div class="btn-group">
                    <input type="hidden" name="producto" value="MST">
                    <div>
                    <center>
                         <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
                    </center>
                    </div>&nbsp
               <div>
                    <center>
                         <p class="p-terms2"> Al hacer click en el botón "Enviar solicitud" aceptas los <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Autorizaciones+Solicitud+Mascota+Plus++Mejorado+Chubb.pdf">terminos, condiciones</a>, <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Poli%CC%81tica+de+cookies+Cencosud.pdf">politica de cookies</a>  y <a target="_blank" href="https://cencosud.zonic3.com/habeas-data">políticas de tratamiento de datos</a> de Seguros Cencosud </p>
                    <center>
                    </div>
               </div>
               <center>
               <div class="btn-group">
                    <div>
                         <button type="submit" class="btn btn-primary">
                              <a class="a-white">Enviar solicitud</a>
                         </button>
                    </div>&nbsp
               </div>
               </center>

               <div class="form-group">
               <img class="gallager-form" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
               </div>

               </div>
          </form>
     </div>
     </div>
     </div>
     </div>

<!-- OpenModal 3 -->

     <div id="openModalThree" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
          <br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">

          <div class="formulariotwo">
                    
          <h1>¡GRACIAS POR TU SOLICITUD!</h1>
          
               &nbsp&nbsp<div>
                         <h2 class="h2-orange">TE ESTAREMOS LLAMANDO EN EL TRANSCURSO DEL DÍA</h2>
                         
                    </div>&nbsp
                    <div>
                         <h2 class="h2-orange">EN EL SIGUIENTE HORARIO</h2>
                         </center>
                    </div>
                    <div class="opt">
                         <h2>Lunes a viernes<br>8:00 a.m a 5:00 p.m</h2>
                    </div>&nbsp
                    <div class="opt">
                         <h2>Sábados<br>8:00 a.m a 12:00 p.m</h2>
                    </div>&nbsp


               <div class="content-opt">
               <div class="row">
               <div class="col-md-1">
                    <div class="opt-h">
                    </div>     
               </div>

               <div class="col-md-9">
                    <div class="opt">
                    <center><a href="https://api.whatsapp.com/send?phone=573057342036&text=&source=&data=&app_absent="><img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/redes_whatsapp.svg">
                    <p class="p-asist-opt-tel">Si quieres asesoría  puedes comunicarte a nuestro WhatsApp</p></a></center>
                    </div> 
               </div>

               <div class="col-md-1">
                    <div class="opt-h">
                    </div> 
               </div>
               </div>
               </div>
     </div>
     </div>
     </div>
     </div>
</div>
</div>
</div>

<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/value.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/validate2.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/choice_doc.js"></script>
</main>

@endsection