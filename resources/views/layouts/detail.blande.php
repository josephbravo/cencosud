@extends('layouts.init-prod')
@section('contenido')

    <form method="post" action="/next">  
    @csrf
        <div class="row justify-content-center">
            <div class="col-sm-7">
                <div class="form-groups">
                    <label for="usuario">Usuario</label>
                    <input type="text" class="form-control" name="usuario" id="usuario" placeholder="Usuario">
                </div>

                <div class="form-groups">
                    <label for="contraseña">Contraseña</label>
                    <input type="text" class="form-control" name="contraseña" id="contraseña" placeholder="Contraseña">
                </div>

            </div>
            <div class="col-sm-7 text-center">
                <button class="btn btn-primary btn-block" type="submit">Iniciar sesión</button>
            </div>



        </div>
</form>

@endsection