@extends('layouts.init-prod')
@section('contenido')

<div class="prince-item">

<h1>Preguntas Frecuentes 
</h1>

<!--Accordion wrapper-->
<div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

  <!-- Accordion card -->
  <div class="card">

    <!-- Card header -->
    <div class="card-header" role="tab" id="headingOne1">
      <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
        aria-controls="collapseOne1">
        <h2 class="mb-0">
        ¿Qué es PlacetoPay? <i class="fas fa-angle-down rotate-icon"></i>
        </h2>
      </a>
    </div>

    <!-- Card body -->
    <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1"
      data-parent="#accordionEx">
      <div class="card-body">
      <p>PlacetoPay es la plataforma de pagos electrónicos que usa Cencosud Colombia S.A para procesar en línea las transacciones generadas en la tienda virtual con las formas de pago habilitadas para tal fin.</p>
      </div>
    </div>

  </div>
  <!-- Accordion card -->

  <!-- Accordion card -->
  <div class="card">

    <!-- Card header -->
    <div class="card-header" role="tab" id="headingTwo2">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
        aria-expanded="false" aria-controls="collapseTwo2">
        <h2 class="mb-0">
        ¿Cómo puedo pagar? <i class="fas fa-angle-down rotate-icon"></i>
        </h2>
      </a>
    </div>

    <!-- Card body -->
    <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
      data-parent="#accordionEx">
      <div class="card-body">
      <p>En la tienda virtual de Cencosud Colombia S.A usted podrá realizar su pago utilizando: Tarjetas de Crédito de las siguientes franquicias: MasterCard y Visa.</p>
      </div>
    </div>

  </div>
  <!-- Accordion card -->


  
  <!-- Accordion card -->
  <div class="card">

    <!-- Card header -->
    <div class="card-header" role="tab" id="headingThree3">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
        aria-expanded="false" aria-controls="collapseThree3">
        <h2 class="mb-0">
        ¿Es seguro ingresar mis datos bancarios en este sitio web? <i class="fas fa-angle-down rotate-icon"></i>
        </h2>
      </a>
    </div>

    <!-- Card body -->
    <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3"
      data-parent="#accordionEx">
      <div class="card-body">
      <p>Para proteger tus datos Cencosud Colombia S.A delega en PlacetoPay la captura de la información sensible. Nuestra plataforma de pagos cumple con los estándares exigidos por la norma internacional PCI DSS de seguridad en transacciones con tarjeta de crédito. Además tiene certificado de seguridad SSL expedido por GeoTrust una compañía Verisign, el cual garantiza comunicaciones seguras mediante la encriptación de todos los datos hacia y desde el sitio; de esta manera te podrás sentir seguro a la hora de ingresar la información de su tarjeta.</p></br>
            <p>Durante el proceso de pago, en el navegador se muestra el nombre de la organización autenticada, la autoridad que lo certifica y la barra de dirección cambia a color verde. Estas características son visibles de inmediato, dan garantía y confianza para completar la transacción en PlacetoPay.</p></br>
                <p>PlacetoPay también cuenta con el monitoreo constante de McAfee Secure y la firma de mensajes electrónicos con Certicámara.
                PlacetoPay es una marca de la empresa colombiana EGM Ingeniería Sin Fronteras S.A.S.</p>
      </div>
    </div>

  </div>
  <!-- Accordion card -->

</div>
<!-- Accordion wrapper -->














  <!-- Accordion card -->
  <div class="card">

    <!-- Card header -->
    <div class="card-header" role="tab" id="headingFour4">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFour4"
        aria-expanded="false" aria-controls="collapseFour4">
        <h2 class="mb-0">
        ¿Puedo realizar el pago cualquier día y a cualquier hora? <i class="fas fa-angle-down rotate-icon"></i>
        </h2>
      </a>
    </div>

    <!-- Card body -->
    <div id="collapseFour4" class="collapse" role="tabpanel" aria-labelledby="headingFour4"
      data-parent="#accordionEx">
      <div class="card-body">
      <p>Sí, en Cencosud Colombia S.A podrás realizar tus compras en línea los 7 días de la semana, las 24 horas del día a sólo un clic de distancia.</p>
      </div>
    </div>

  </div>
  <!-- Accordion card -->




    <!-- Accordion card -->
    <div class="card">

<!-- Card header -->
<div class="card-header" role="tab" id="headingFive5">
  <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFive5"
    aria-expanded="false" aria-controls="collapseFive5">
    <h2 class="mb-0">
    ¿Puedo cambiar la forma de pago? <i class="fas fa-angle-down rotate-icon"></i>
    </h2>
  </a>
</div>

<!-- Card body -->
<div id="collapseFive5" class="collapse" role="tabpanel" aria-labelledby="headingFive5"
  data-parent="#accordionEx">
  <div class="card-body">
  <p>Si aún no has finalizado tu pago, podrás volver al paso inicial y elegir la forma de pago que prefieras. Una vez finalizada la compra no es posible cambiar la forma de pago.</p>
  </div>
</div>

</div>
<!-- Accordion card -->



    <!-- Accordion card -->
    <div class="card">

<!-- Card header -->
<div class="card-header" role="tab" id="headingSix6">
  <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseSix6"
    aria-expanded="false" aria-controls="collapseFive5">
    <h2 class="mb-0">
    ¿Pagar electrónicamente tiene algún valor para mí como comprador? <i class="fas fa-angle-down rotate-icon"></i>
    </h2>
  </a>
</div>

<!-- Card body -->
<div id="collapseSix6" class="collapse" role="tabpanel" aria-labelledby="headingSix6"
  data-parent="#accordionEx">
  <div class="card-body">
  <p>No, los pagos electrónicos realizados a través de PlacetoPay no generan costos adicionales para el comprador.</p>
  </div>
</div>

</div>
<!-- Accordion card -->

    <!-- Accordion card -->
    <div class="card">

<!-- Card header -->
<div class="card-header" role="tab" id="headingSeven7">
  <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseSeven7"
    aria-expanded="false" aria-controls="collapseSeven7">
    <h2 class="mb-0">
    ¿Qué debo hacer si mi transacción no concluyó? <i class="fas fa-angle-down rotate-icon"></i>
    </h2>
  </a>
</div>

<!-- Card body -->
<div id="collapseSeven7" class="collapse" role="tabpanel" aria-labelledby="headingSeven7"
  data-parent="#accordionEx">
  <div class="card-body">
  <p>En primera instancia deberás revisar si llegó un mail de confirmación del pago en tu cuenta de correo electrónico (la inscrita en el momento de realizar el pago), en caso de no haberlo recibido, deberás contactar a info@estacion.ec para confirmar el estado de la transacción.</p>
  </div>
</div>

</div>
<!-- Accordion card -->





    <!-- Accordion card -->
    <div class="card">

<!-- Card header -->
<div class="card-header" role="tab" id="headingEighth8">
  <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseEighth8"
    aria-expanded="false" aria-controls="collapseEighth8">
    <h2 class="mb-0">
    ¿Qué debo hacer si no recibí el comprobante de pago?<i class="fas fa-angle-down rotate-icon"></i>
    </h2>
  </a>
</div>

<!-- Card body -->
<div id="collapseEighth8" class="collapse" role="tabpanel" aria-labelledby="headingEighth8"
  data-parent="#accordionEx">
  <div class="card-body">
  <p>Por cada transacción aprobada a través de PlacetoPay, recibirás un comprobante del pago con la referencia de compra en la dirección de correo electrónico que indicaste al momento de pagar. Si no lo recibes, podrás contactar a info@estacion.ec, para solicitar el reenvío del comprobante a la misma dirección de correo electrónico registrada al momento de pagar.</p>
  </div>
</div>

</div>
<!-- Accordion card -->

</div>

<form method="GET" action="https://seguroscencosud.co">

<div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
            <center><button type="submit" class="btn btn-primary">
            <a class="a2" href="https://seguroscencosud.co" >{{ __('Volver') }}</a>
            </button></center>
            </div>
            
    </div></form>


@endsection