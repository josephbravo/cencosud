@extends('base.init')

@section('content')
<main>
     <section id="banner">
          <img class="bannermovile" src="https://connect-static-files.s3.amazonaws.com/pagos/banner_adu-mobile.jpg">
          <img class="bannerbig" src="https://connect-static-files.s3.amazonaws.com/pagos/iframe_Cencosud-adu_1220.jpg">
          
          <div class="contenedor">
               <img class="price-adu" src="https://connect-static-files.s3.amazonaws.com/pagos/adu_precio_1220.png">
          </div>
     </section>

     <section id="adquiere">
     <div class="formulario-mascota-resp">
          <h2><b>ADQUIERE TU SEGURO AQUÍ:</b></h2><br>
     <div>
          <center>
          <button type="submit" class="btn btn-primary">
          <a href="#openModal" class="a-white"><b>COMPRAR 100% ONLINE</b></a>
          </button>
          </center>
     </div>
     <div>
          <center>
          <button type="submit" class="btn btn-primary">
          <a href="#openModalTwo" class="a-white"><b>QUIERO QUE ME LLAMEN</b></a>
          </button>
          </center>
     </div><br>
     <div>
          <center>
          <p class="p-vig">La póliza inicia vigencia el 16 de cada mes si adquieres tu póliza entre el 1 y el 15, o el 1° del mes siguiente si la adquieres entre el 16 y ultimo día de cada mes.
          <br>
          En el transcurso de 8 días hábiles estaremos enviando a tu correo electrónico la póliza y las condiciones particulares incluyendo las exclusiones y condiciones particulares del producto.</p>
          </center>
     </div>

     <center><br>
     <a href="https://www.placetopay.com/web/home"> <img class="place" img src="https://connect-static-files.s3.amazonaws.com/pagos/Placetopay.png"> </a>
     </center>

     <center><br>
     <a href="https://pagoscencosud.connect-sos.com/question">Preguntas Frecuentes</a>
     </center>
     </div>

     <h2 class="h2-container">Nuestro producto Asistencia Médica Domiciliara busca acompañarte y acompañar a tu familia en los momentos más delicados desde la comodidad de tu casa en caso de accidente o enfermedad.</h2>

     <h2 class="h2-container-up">QUE CUBRE TU SEGURO ASISTENCIA MÉDICA DOMICILIARIA</h2>

     <div class="container-adu">
     <ul class="slider">

          <li id="slide1">
          <div class="opt-adu-resp">
               <center><img class="icon-aduicon" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_asistencia_medica.svg" alt="" />
               <p class="p-asist-opt">ASISTENCIA MÉDICA<br>DOMICILIARIA (ilimitado)</p>
               <p>Disposición de un médico para que<br> adelante la consulta en su domicilio.</p></center>
          </div> 
          </li>

          <li id="slide2">
          <div class="opt-adu-resp">
               <center><img class="icon-aduicon" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_asistencia_telefonica.svg" alt="" />
               <p class="p-asist-opt">ORIENTACIÓN MÉDICA<br>TELEFÓNICA (ilimitado)</p>
               <p>Orientación médica básica las 24 horas<br> del día los 365 días del año.</p></center>
          </div> 
          </li>

          <li id="slide3">
          <div class="opt-adu-resp">
               <center><img class="icon-aduicon" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_asistencia_traslados.svg" alt="" />
               <p class="p-asist-opt">TRASLADOS MÉDICOS<br>DE URGENCIAS (ilimitado)</p>
               <p>A criterio del médico tratante, la Compañía adelantará los contactos y coordinación para el traslado del asegurado, asumiendo los gastos de traslado hasta el centro hospitalario más cercano.</p></center>
          </div>
          </li>
     </ul>
     </div>

     <div class="menu-slide">
          <ul class="menu">
          <li>
               <a href="#slide1">1</a>
          </li>
          <li>
               <a href="#slide2">2</a>
          </li>
          <li>
               <a href="#slide3">3</a>
          </li>

          </ul>
     </div>
     </section>

     <section id="section-imgs-adu"> <!-- class: funnel-container-plans -->
          <h2 class="h2-container-up-2">PLANES Y TARIFAS</h2>
          <h2 class="h2-container">Desde <b>$15.717</b> mensuales</h2>

          <div class="container-plans-adu"> <!-- class: content-plans -->
               <article> <!-- opt-plan   img: img-plan-->
                    <img img src="https://connect-static-files.s3.amazonaws.com/pagos/planes_1.jpg">
               </article>   

               <article> <!-- opt-plan2 -->
                    <img img src="https://connect-static-files.s3.amazonaws.com/pagos/planes_2.jpg">
               </article> 
          </div>

          <h2 class="h2-container-up-2">NUESTRO SEGURO DE ASISTENCIA MÉDICA A DOMICILIO</h2>
          <h2 class="h2-container">Una vida tan emocionante merece cuidarse.</h2>
     </section>

     <section id="section-last"> <!-- funnel-container-last -->
          <h2 class="h2-container-up-2">NO CUBRE</h2>
          <h2 class="h2-container">La asistencia y gastos por enfermedades o estados patológicos producidos por la ingestión voluntaria de drogas, sustancias tóxicas, ingesta de alcohol, narcóticos o medicamentos adquiridos sin prescripción médica.</h2><br>
          <h2 class="h2-container-up-2">CON EL RESPALDO DE</h2>
          <center><img class="liberty" img src="https://connect-static-files.s3.amazonaws.com/pagos/respaldo_liberty.svg"></center><br>
          
          <center><div class="opt-plan">
          <p>Aplican condiciones y restricciones en caso de tener alguna inquietud comunicarse a línea de atención de Seguros Cencosud.</p></center>
          </div> 

          <article>
          <img class="gallager" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
          </article> 
     </section>

<!-- OpenModal 1 -->

     <div id="openModal" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a><br>
     <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">
     <div class="formulariotwo">
		<form method="post" action="{{route('adu.store')}}" onsubmit="return validate()">@csrf

          <h1>ADQUIERE TU SEGURO AQUÍ</h1>
               <div class="btn-group">&nbsp&nbsp
               <div>
                    <input name="nombre" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre" required>   
               </div>&nbsp
               <div>   
                    <input name="apellido" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Apellido" required>
               </div>
               </div>

               <div class="form-group">     
               </div>

               <div class="btn-group">
               <p class="date-born">En qué fecha naciste?</p>
                    <div>
                    <input name="date" type="date" id="start" class="form-control" name="trip-start" min="1910-01-01" max="2002-12-31">
                    </div>
               </div>

               <div class="form-group">     
               </div>

               <div class="form-group">
                    <select name="tipdocumento" class="form-control" onchange="choice_doc(this)" placeholder="PRODUCTO - PRECIO" id="select_tipdoc" required>
                         <option value="">Tipo documento</option>
                         <option value="CC">CC</option>
                         <option value="CE">CE</option>
                         <option value="PPN">PAS</option>
                    </select>
               </div>

               <div class="form-group">
                    <input id="ident" pattern="[A-Za-z ]*[0-9]{5,12}" title="Campo inválido. Minimo 5 dígitos máximo 10 dígitos" maxlength="10" name="documento" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Numero documento" required>
               </div>

               <div class="form-group">
                    <select name="genero" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="">Genero</option>
                         <option value="Masculino">Masculino</option>
                         <option value="Femenino">Femenino</option>
                    </select>    
               </div>

               <div class="form-group">
                    <input name="eps" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput2" placeholder="EPS" required>
               </div>

               <div class="form-group">
                    <select name="afiliacion" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="">Tipo Afiliacion</option>
                         <option value="Titular">Beneficiario</option>
                         <option value="Beneficiario">Cotizante</option>
                    </select>    
               </div>

               <div class="form-group">
                    <select id="reg" name="regimen" class="form-control" placeholder="Regimen" onchange="choice_reg(this)" required>
                         <option value="">Régimen</option>
                         <option value="1">Contributivo</option>
                         <option value="2">Subsidiado</option>                    
                    </select>    
               </div>

               <div class="form-group">
                    <input pattern="^[0-9]{7,10}$" title="Campo inválido. Minimo 7 dígitos máximo 10 dígitos" maxlength="10" name="celular" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Teléfono" required>
               </div>

               <div class="form-group">
                    <input name="email" type="email" class="form-control" id="formGroupExampleInput2" placeholder="Correo electrónico" required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>

               <div class="form-group">
                    <select name="ciudad" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="">Ciudad</option>
                         <option value="1">Bogotá-Cundinamarca</option>
                         <option value="2">Medellín-Antioquia</option>
                         <option value="3">Cali-Valle</option>
                         <option value="4">Cartagena-Bolivar</option>
                    </select>
               </div>

               <div class="form-group">
                    <input name="direccion" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu dirección de residencia" required>
               </div>

               <div class="form-group">
                    <select name="profesion" class="form-control" placeholder="Cuál es tu ocupación" required>
                         <option value="">Profesión</option>
                         @foreach ($ocupacions as $ocupacion)
                         <option value="{{ $ocupacion->id }}">{{ $ocupacion->ocupacion }}</option>
                         @endforeach
                    </select>
               </div>

               <div class="form-group">
                    <select id="select" name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="0">Seleccione</option>
                         <option value="15717">Asistencia Medica Domiciliaria - A $15.717</option>
                    </select>
               </div>
               
               <div class="btn-group">
               <div>
               <center>
                    <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
               </center>
               </div>&nbsp
               <div>
               <center>
                    <p class="p-terms2"> Al hacer click en el botón "COMPRAR 100% ONLINE" aceptas los <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Autorizaciones+Solicitud+Asistencia+Medica+Liberty.pdf"> terminos, condiciones</a>, <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Poli%CC%81tica+de+cookies+Cencosud.pdf">politica de cookies</a> y <a target="_blank" href="https://cencosud.zonic3.com/habeas-data">políticas de tratamiento de datos</a> de Seguros Cencosud </p>
               <center>
               </div>
               </div>
               
               <center><div class="btn-group">
               <div>  
                    <button type="submit" class="btn btn-primary">
                    <a class="a-white">Enviar solicitud</a>
                    </button>
               </div>&nbsp
               </div></center>

               <div class="form-group">
               <img class="gallager-form" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
               </div>
                    </div>
               </form>
               </div>
          </div>
	</div>
</div>

<!-- OpenModal 2 -->

<div id="openModalTwo" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a><br>
     <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">
     <div class="formulariotwo">
		<form method="post" action="{{route('digital.store')}}" onsubmit="return validate2()">@csrf
                    
               <h1>SOLICITA TU ASESORÍA</h1>
               <div class="btn-group">&nbsp&nbsp
                    <div>
                         <input name="nombre" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre" required>
                    </div>&nbsp
                    <div>       
                         <input name="apellido" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Apellido" required>
                    </div>
               </div>&nbsp

               <div class="form-group">
                    <input oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" name="telefono" type="number" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu número celular" required>
               </div>
               <div class="form-group">
                    <input name="email" type="email" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu correo electrónico" required required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>
               <div class="form-group">
                    <input oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" name="documento" type="number" class="form-control" id="formGroupExampleInput2" placeholder="Número de identificación" required>
               </div>
               <div class="form-group">
               <select id="select2" name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                    <option value="0">Seleccione</option>
                    <option value="15717">Asistencia Medica Domiciliaria - A $15.717</option>
               </select>&nbsp

               <div class="btn-group">
                    <input type="hidden" name="producto" value="ADU">
                    <div>
                    <center>
                         <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
                    </center>
                    </div>&nbsp
                    <div>
                    <center>
                         <p class="p-terms2"> Al hacer click en el botón "Enviar solicitud" aceptas los <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Autorizaciones+Solicitud+Asistencia+Medica+Liberty.pdf"> terminos, condiciones </a>, <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Poli%CC%81tica+de+cookies+Cencosud.pdf">politica de cookies</a> y <a target="_blank" href="https://cencosud.zonic3.com/habeas-data">políticas de tratamiento de datos</a> de Seguros Cencosud </p>
                    <center>
                    </div>
               </div>
               <center><div class="btn-group">
               <div>
                    <button type="submit" class="btn btn-primary">
                         <a class="a-white">Enviar solicitud</a>
                    </button>
                         
               </div>&nbsp
               </div></center>

               <div class="form-group">
               <img class="gallager-form" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
               </div>                
               </div>
          </form>
     </div>
     </div>
     </div>
     </div>

<!-- OpenModal 3 -->

     <div id="openModalThree" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a><br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">
          <div class="formulariotwo">
                    
          <h1>¡GRACIAS POR TU SOLICITUD!</h1>&nbsp&nbsp
               <div>
                    <h2 class="h2-orange">TE ESTAREMOS LLAMANDO EN EL TRANSCURSO DEL DÍA</h2>    
               </div>&nbsp
               <div>
                    <h2 class="h2-orange">EN EL SIGUIENTE HORARIO</h2>
                    </center>
               </div>
               <div class="opt">
                    <h2>Lunes a viernes<br>8:00 a.m a 5:00 p.m</h2>
               </div>&nbsp
               <div class="opt">
                    <h2>Sábados<br>8:00 a.m a 12:00 p.m</h2>
               </div>&nbsp

               <div class="content-opt">
               <div class="row">
               <div class="col-md-1">
                    <div class="opt-h">
                         </div>     
                    </div>
                    <div class="col-md-9">
                    <div class="opt">
                    <center><a href="https://api.whatsapp.com/send?phone=573057342036&text=&source=&data=&app_absent="><img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/redes_whatsapp.svg">
                    <p class="p-asist-opt-tel">Si quieres asesoría  puedes comunicarte a nuestro WhatsApp</p></a></center>
                    </div> 
               </div>

               <div class="col-md-1">
                    <div class="opt-h">
                    </div> 
               </div>
               </div>
          </div>
     </div>
     </div>
     </div>
     </div>
</div>
</div>
</div>

<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/validate.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/choice_reg.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/choice_doc.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/validate2.js"></script>

</main>
@endsection