@extends('base.init2')

@section('content')

<section id="section-last-adu"> <!-- funnel-contact -->
     <div class="autos-compra">
          <h1 class="h2-autos-w">YA CASI TERMINAMOS</h1>
     </div>
</section>

<section class="section-pay">
<div class="cart-compra">

<h2 class="h2-container-up" id="h2-cartpay">Proceso de pago</h2>

<p>Por favor llena los campos con la información correspondiente</p>

<div class="formularioauto">
     <form method="post" action="{{route('moto.store')}}" onsubmit="return validate()" id="form-pay">@csrf

     <div class="form-group" style="margin-bottom: 0.3rem;">
     <input name="nombre" pattern="[A-Za-z ]*" title="Campo inválido" onkeypress="return validar(event)" type="text" class="form-control" placeholder="Nombre" required>
               </div>
                    
               <div class="form-group" style="margin-bottom: 0.3rem;">
               <input name="apellido" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" placeholder="Apellido" required>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
               <select name="tipdocumento" class="form-control" onchange="choice_doc(this)"  placeholder="PRODUCTO - PRECIO" required>
                    <option value="-">Tipo documento</option>
                    <option value="CC">CC</option>
                    <option value="CE">CE</option>
                    <option value="PPN">PAS</option>
               </select>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
                    <input name="documento" id="ident" pattern="[A-Za-z ]*[0-9]{5,12}" title="Campo inválido. Minimo 5 dígitos máximo 10 dígitos" maxlength="10"  type="text" class="form-control" placeholder="Numero de documento" required>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
               <input name="email" type="email" class="form-control" id="formGroupExampleInput2" placeholder="Correo electrónico" required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
                    <input name="direccion" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu dirección de residencia" required>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
               <select name="ciudad" class="form-control" placeholder="Ciudad" required>
                    <option value="">Ciudad</option>
                    <option value="1">Bogotá-Cundinamarca</option>
                    <option value="2">Medellín-Antioquia</option>
                    <option value="3">Cali-Valle</option>
                    <option value="4">Cartagena-Bolivar</option>
               </select>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
                    <input name="celular" pattern="^[0-9]{7,10}$" title="Campo inválido. Minimo 7 dígitos máximo 10 dígitos" maxlength="10" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Teléfono" required>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
                    <input name="placa" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Placa change" required>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
               <select name="marca" class="form-control" placeholder="Marca" required>
                    <option value="">Yamaha</option>
                    <option value="1">Auteco</option>
                    <option value="2">AKT</option>

               </select>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
                    <input name="color" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" placeholder="Color" required>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
               <select name="xx" class="form-control" placeholder="Referencia" required>
                    <option value="">MOTO1</option>
                    <option value="1">MOTO2</option>
                    <option value="2">MOTO3</option>

               </select>
               </div>

               </div>
          </form>
               </div>


     <div class="cart-compra-2">
          <div class="form-group" style="margin-bottom: 0.3rem;">
               <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>

               <div>
                    <p class="accept-tyc">Deseo que mi cuota mensual se debite automáticamente de la tarjeta de crédito utilizada en este proceso de pago</p>

                    </div>
               </div>
               
               <div class="cart-compra">
               <p>Selecciona tu método de pago</p>
               </div>

          <div class="contentpay">
        
               <div class="left">
            
            <center><img class="tarjeta-credito" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icontarjeta.png"></img>
               
                    <form action="{{route('cart.add')}}" method="post">@csrf
                         <input type="hidden" name="id_producto" value="1">
                         <button type="submit" class="btn btn-primary-pay">
                         <a href="{{route('cart.add')}}" class="a-white"><b>TARJETA CRÉDITO</b></a>
                         </button>
                    </form>
                    </center>

        </div>
        <div class="right">
            
            <center><img class="pse-method" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-pse.png"></img>
               
                    <form action="{{route('cart.add')}}" method="post">@csrf
                         <input type="hidden" name="id_producto" value="1">
                         <button type="submit" class="btn btn-primary-pay">
                         <a href="{{route('cart.add')}}" class="a-white"><b><b style="visibility: hidden">_______</b>PSE<b style="visibility: hidden">_______</b></b></a>
                         </button>
                    </form>
                    </center>

               </div>
               </div>
          </div>


     <div class="cart-compra-pay">
     <div class="card-pay">

     <div class="cart-tittle">

          <p style="color: white;">RESUMEN</p>
          </div>

     <div class="btn-group" style="margin-top: 0.6rem;">      
          <div>
               <p class="p-producto">Producto:</p>
          </div>&nbsp

          <div>
               <p>{{ $producto }}</p>
          </div>
          
     </div>&nbsp<br>

     <div class="btn-group">
          <div>
          <p class="p-precio">Precio:</p>
          </div>&nbsp
          <div>
          @if (Session::has('precio'))
          <p>{{ session('precio') }}</p>
           
          @else
          <p>{{ $precio }}</p>
          @endif


          
          </div>
     </div>&nbsp<br>
     <hr>

     <div class="btn-group">

          <div>
          <p class="p-precio">Total:</p>
          </div>&nbsp
          <div>

          @if (Session::has('precio'))
          <p>{{ session('precio') }}</p>
           
          @else
          <p>{{ $precio }}</p>
          @endif
          

          </div>
     </div>&nbsp

     <div class="btn-precio">
          <center>
               <button type="submit" form="form-pay" class="btn btn-primary2">
               <b>Pagar</b>
               </button>
          
          </center>
          </div>
          <div class="btn-group">

          <div class="btn-group">

<div>
<center>
     <input form="form-pay" class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
          </center>
</div>&nbsp
<div>
<p class="accept-tyc">Acepto y autorizo el uso de mi información de acuerdo a los términos y condiciones y política de cookies.</p>

</div>
</div>&nbsp
               </div>

     
     </div>
     </div>

     </section>

     <section>
     <div class="cart-method-pay">
     <div class="card-method">
     
     <!-- <p><img class="secure-icon" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icon-seguro.png"></img>Pago 100% seguro<p> -->
     <p>Compra fácil y seguro con cualquier medio de pago.<p></div>

     <div class="img-icon-pay">
     <main class="main-img">
     <center>
     <img class="icons-pay" id="icons-pay1" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-placetopay.png"></img>
     <img class="icons-pay" id="icons-pay3" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-pse.png"></img>
     <img class="icons-pay" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-visa.png"></img>
     <img class="icons-pay" id="icons-pay4" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-mastercard.png"></img>
</center>
     </main>
     </div>



     </section>





<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/value.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/validate2.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/choice_doc.js"></script>


@endsection