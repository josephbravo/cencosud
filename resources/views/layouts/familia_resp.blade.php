@extends('base.init')
@section('content')
<main>
     <section id="banner">

     <img class="bannermovile" src="https://connect-static-files.s3.amazonaws.com/pagos/banner_familia-mobile.jpg">
          <img class="bannerbig" src="https://connect-static-files.s3.amazonaws.com/pagos/Iframe+_familia_1220.jpg">
          
          <div class="contenedor">
          <img class="price" src="https://connect-static-files.s3.amazonaws.com/pagos/banner_familia-precio-mobile.png">
          
          </div>
          </div>
     </section>

     <section id="adquiere">
     <div class="formulario-mascota-resp">

     <h2><b>ADQUIERE TU SEGURO AQUÍ:</b></h2>
     <br>
     <div>
          <center>
          <button type="submit" class="btn btn-primary">
          <a href="#openModal" class="a-white"><b>COMPRAR 100% ONLINE</b></a>
          </button>
          </center>
     </div>

     <div>
          <center>
          <button type="submit" class="btn btn-primary">
          <a href="#openModalTwo" class="a-white"><b>QUIERO QUE ME LLAMEN</b></a>
          </button>
          </center>
     </div>
     
     <br>
     <div>
          <center>
          <p class="p-vig">La póliza inicia vigencia una vez se efectué el pago de la primera cuota y las Asistencias Medicas quedan activas a las 72 horas de iniciado su Seguro.
          <br>
          En el transcurso de 8 días hábiles estaremos enviando a tu correo electrónico la póliza y las condiciones particulares incluyendo las exclusiones y condiciones particulares del producto.</p>
          </center>
     </div>

     <center>
     <br>
     <a href="https://www.placetopay.com/web/home"> <img class="place" img src="https://connect-static-files.s3.amazonaws.com/pagos/Placetopay.png"> </a>
     </center>
     <center>
     <br>
     <a href="https://pagoscencosud.connect-sos.com/question">Preguntas Frecuentes</a>
     </center>
     </div>


     <h2 class="h2-container-fml">La mayor alegría es tener una familia, y la mayor satisfacción viene de asegurar que todos están bien.<br>
               Nuestro seguro de Familia Protegida se encarga de proteger a los tuyos para que disfruten del camino sin preocuparse por el mañana.</h2>

     <h2 class="h2-container-up" id="slide1">QUE CUBRE TU SEGURO FAMILIA PROTEGIDA</h2>

     <div class="container-fml">

          <ul class="slider">

          <li id="slide1">
          <div class="opt-adu-resp">
               <center><img class="icon-adu1" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_asistencia_medica.svg" alt="" />
               
               <p class="p-asist-opt">ASISTENCIA MÉDICA<br>(solo asegurado principal)</p>

               <p>Disposición de un médico para que adelante la consulta en su domicilio.</p>
     
          </div> 
          </li>
          <li id="slide2">
          <div class="opt-adu-resp">
               <center><img class="icon-adu1" src="https://connect-static-files.s3.amazonaws.com/pagos/iconos_domicilio.svg" alt="" />
               <p class="p-asist-opt">MÉDICO A DOMICILIO<br>(accidente o enfermedad)</p>
               <p>8 eventos al año, copago de $30.000 por evento. Luego de los 8 eventos enviaremos el servicio con un costo preferencial.</p>
               
               </div> 
          </li>
          <li id="slide3">
          <div class="opt-adu-resp">
               <center><img class="icon-adu1" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_asistencia_traslados.svg" alt="" />
               <p class="p-asist-opt">TRASLADOS MÉDICOS<br>TERRESTRE</p>

               <p>En caso de accidente y/o riesgo vital.</p><br><br><br>

               </div>
          </li>
          <li id="slide4">
          <div class="opt-adu-resp">
               <center><img class="icon-adu1" src="https://connect-static-files.s3.amazonaws.com/pagos/iconos_clinica.svg" alt="" />
               <p class="p-asist-opt">VALORACIÓN MÉDICA<br>EN CLÍNICA</p>

               <p>Por accidente y/o riesgo vital.</p><br><br>

               </div>
          </li>
          </ul>

     </div>

     <div class="menu-slide">

          <ul class="menu">
          <li>
               <a href="#slide1">1</a>
          </li>
          <li>
               <a href="#slide2">2</a>
          </li>
          <li>
               <a href="#slide3">3</a>
          </li>
          <li>
               <a href="#slide4">4</a>
          </li>

          </ul>
          <div>

     </section>


     <section id="section-imgs-adu"> <!-- class: funnel-container-plans -->

     <h2 class="h2-container-up-2" id="slide5">PLANES Y TARIFAS</h2>
     <h2 class="h2-container">Desde <b>$20.800</b> mensuales</h2><br>


     <div class="container-fml-2">

          <ul class="slider">

          <li id="slide5">
          <div class="opt-mst-resp">
               <center>              
                    
               <p class="p-asist-opt" id="plana">PLAN A</p><hr class="hr-plan">
               <p>Muerte Accidental</p>
               <p class="value"><b class="text-tariff">$10'547.000</b></p><hr class="hr-plan">
               <p>Desmembración por accidente</p>
               <p class="value"><b class="text-tariff">$10'547.000</b></p><hr class="hr-plan">
               <p>ITP por accidente</p>
               <p class="value"><b class="text-tariff">$10'547.000</b></p><hr class="hr-plan">
               <p>Muerte accidental como pasajero o peatón<br>en automóvil particular</p>
               <p class="value"><b class="text-tariff">$15'820.600</b></p><hr class="hr-plan">
               <p>Bono canasta para gastos del hogar por<br>muerte accidental</p>
               <p class="value"><b class="text-tariff">$3'164.100</b></p>
     
          </div> 
          </li>
          <li id="slide6">
          <div class="opt-mst-resp">
               <center>               
               <p class="p-asist-opt" id="planb">PLAN B</p><hr class="hr-plan">
               <p>Muerte Accidental</p>
               <p class="value"><b class="text-tariff">$15'820.600</b></p><hr class="hr-plan">
               <p>Desmembración por accidente</p>
               <p class="value"><b class="text-tariff">$15'820.600</b></p><hr class="hr-plan">
               <p>ITP por accidente</b></p>
               <p class="value"><b class="text-tariff">$15'820.600</b></p><hr class="hr-plan">
               <p>Muerte accidental como pasajero o peatón<br>en automóvil particular</p>
               <p class="value"><b class="text-tariff">$21'094.200</b></p><hr class="hr-plan">
               <p>Bono canasta para gastos del hogar por<br>muerte accidental</p>
               <p class="value"><b class="text-tariff">$5'273.500</b></p>
               
               </div> 
          </li>
          <li id="slide7">
          <div class="opt-mst-resp">
               <center>
               <p class="p-asist-opt" id="planc">PLAN C</p><hr class="hr-plan">
               <p>Muerte Accidental</p>
               <p class="value"><b class="text-tariff">$21'094.200</b></p><hr class="hr-plan">
               <p>Desmembración por accidente</p>
               <p class="value"><b class="text-tariff">$21'094.200</b></p><hr class="hr-plan">
               <p>ITP por accidente</p>
               <p class="value"><b class="text-tariff">$21'094.200</b></p><hr class="hr-plan">
               <p>Muerte accidental como pasajero o peatón<br>en automóvil particular</p>
               <p class="value"><b class="text-tariff">$26'367.700</b></p><hr class="hr-plan">
               <p>Bono canasta para gastos del hogar por<br>muerte accidental</p>
               <p class="value"><b class="text-tariff">$7'382.900</b></p>
               </div>
               
          </li>

          </ul>
          

     </div>

     <div class="menu-slide">

          <ul class="menu">
          <li>
               <a href="#slide5">A</a>
          </li>
          <li>
               <a href="#slide6">B</a>
          </li>
          <li>
               <a href="#slide7">C</a>
          </li>


          </ul>
     </div>

          <h2 class="h2-container-up-2">NUESTRO SEGURO DE FAMILIA PROTEGIDA</h2>
          <h2 class="h2-container">Una vida tan emocionante merece cuidarse.</h2><br>
          </section>



     <section> <!-- funnel-container-last -->

     <h2 class="h2-container-up-2">ASISTENCIA</h2>
     <h2 class="h2-container">A continuación encontrará las asistencias disponibles para este seguro</h2><br>
     <div class="opt-mst-last">
                <center><a href="https://api.whatsapp.com/send?phone=573057342036&text=&source=&data=&app_absent="><img class="asist-opt-fml" img src="https://connect-static-files.s3.amazonaws.com/pagos/redes_whatsapp.svg"></a>
               <p class="p-asist-opt-tel">ORIENTACIÓN MÉDICA TELEFÓNICA</p></center>
               </div> <br><br>
          

     <h2 class="h2-container-up-2">CON EL RESPALDO DE</h2>

     <center><img class="liberty" img src="https://connect-static-files.s3.amazonaws.com/pagos/logos_chubb.svg"></center><br>


          
             
     
          <center><div class="opt-plan">
          <p>Aplican condiciones y restricciones en caso de tener alguna inquietud comunicarse a línea de atención de Seguros Cencosud.</p></center>
          </div> 

          <article>
          <img class="gallager" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
          </article> 
     </section>

     <div id="openModal" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
          <br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">

          <div class="formulariotwo">
		<form method="post" action="{{route('familia.store')}}" onsubmit="return value()">
                    @csrf
          <h1>ADQUIERE TU SEGURO AQUÍ</h1>

               <div class="btn-group">
               &nbsp&nbsp<div>
                         <input name="nombre" pattern="[A-Za-z ]*" title="Campo inválido" onkeypress="return validar(event)" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre" required>
                         
                    </div>&nbsp
                    <div>
                         
                              <input name="apellido" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Apellido" required>
                         </center>
                    </div>
               </div>

               <div class="form-group">     
               </div>

               <div class="btn-group">
               <p class="date-born">En qué fecha naciste?</p>
                    <div>
                    <input name="date" type="date" id="start" class="form-control" name="trip-start" max="{{ $max }}" min="{{ $min }}" >
                    </div>
               </div>

               <div class="form-group">     
               </div>

               <div class="form-group">
                    <select name="tipdocumento" class="form-control" onchange="choice_doc(this)" placeholder="PRODUCTO - PRECIO" required>
                         <option value="">Tipo documento</option>
                         <option value="CC">CC</option>
                         <option value="CE">CE</option>
                         <option value="PPN">PAS</option>
                    </select>
               </div>

               <div class="form-group">
                    <input id="ident" pattern="[A-Za-z ]*[0-9]{5,12}" title="Campo inválido. Minimo 5 dígitos máximo 12 dígitos" maxlength="12" name="documento" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Número documento" required>
               </div>

               <div class="form-group">
                    <select name="genero" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="">Genero</option>
                         <option value="Masculino">Masculino</option>
                         <option value="Femenino">Femenino</option>
                    </select>    
               </div>

               <div class="form-group">
                    <input pattern="^[0-9]{7,10}$" title="Campo inválido. Minimo 7 dígitos máximo 10 dígitos" maxlength="10" name="celular" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Teléfono" required>
               </div>

               <div class="form-group">
                    <input name="email" type="email" class="form-control" id="formGroupExampleInput2" placeholder="Correo electrónico" required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>

               <div class="form-group">
                    <select name="ciudad" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="">Ciudad</option>
                         <option value="1">Bogotá-Cundinamarca</option>
                         <option value="2">Medellín-Antioquia</option>
                         <option value="3">Cali-Valle</option>
                         <option value="4">Cartagena-Bolivar</option>
                    </select>
               </div>

               <div class="form-group">
                    <input name="direccion" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu dirección de residencia" required>
               </div>

               <div class="form-group">
                    <select name="profesion" class="form-control" placeholder="Cuál es tu ocupación" required>
                              <option value="0">Profesión</option>
                         @foreach ($ocupacions as $ocupacion)
                              <option value="{{ $ocupacion->id }}">{{ $ocupacion->ocupacion }}</option>
                         @endforeach
                    </select>
               </div>

               <div class="form-group">
               <select id="select" name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                    <option value="0">Seleccione</option>
                    <option value="20449">Familia Protegida - A $20.800</option>
                    <option value="22006">Familia Protegida - B $22.400</option>
                    <option value="23563">Familia Protegida - C $24.900</option>
               </select>
                                   
          </div>
               
               <div class="btn-group">
                    
                    <div>
                         <center>
                              <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
                         </center>
                    </div>&nbsp
               <div>
                         <center>
                              <p class="p-terms2"> Al hacer click en el botón "COMPRAR 100% ONLINE" aceptas los <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Autorizaciones+Solicitud+Familia+protegida+Chubb.pdf">terminos, condiciones</a>, , <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Poli%CC%81tica+de+cookies+Cencosud.pdf">politica de cookies</a>  y <a target="_blank" href="https://cencosud.zonic3.com/habeas-data">políticas de tratamiento de datos</a> de Seguros Cencosud </p>
                         <center>
               </div>
               </div>
               <center><div class="btn-group">
               <div>
                         
                              <button type="submit" class="btn btn-primary">
                                   {{ __('Enviar solicitud') }}
                              </button>
                         
                    </div>&nbsp
               </div></center>

               <div class="form-group">
               <img class="gallager-form" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
               </div>
                    
                         </div>
               </form>
               </div>
               </div>
	</div>
</div>

<!-- OpenModal 2 -->

<div id="openModalTwo" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
          <br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">

          <div class="formulariotwo">
		<form method="post" action="{{route('digital.store')}}" onsubmit="return validate2()">
                    @csrf
                    
          <h1>SOLICITA TU ASESORÍA</h1>

          <div class="btn-group">
               &nbsp&nbsp<div>
                         <input name="nombre" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre" required>
                         
                    </div>&nbsp
                    <div>
                         
                              <input name="apellido" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Apellido" required>
                         </center>
                    </div>
          </div>&nbsp

               <div class="form-group">
                    <input oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" name="telefono" type="number" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu número celular" required>
               </div>
               <div class="form-group">
                    <input name="email" type="email" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu correo electrónico" required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>
               <div class="form-group">
                    <input oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" name="documento" type="number" class="form-control" id="formGroupExampleInput2" placeholder="Número de identificación" required>
               </div>
               <div class="form-group">
               <select id="select2" name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                    <option value="0">Seleccione</option>
                    <option value="20449">Familia Protegida - A $20.800</option>
                    <option value="22006">Familia Protegida - B $22.400</option>
                    <option value="23563">Familia Protegida - C $24.900</option>
               </select>&nbsp

               <div class="btn-group">
               <input type="hidden" name="producto" value="FML">
                    <div>
                         <center>
                              <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
                         </center>
                    </div>&nbsp
               <div>
                         <center>
                              <p class="p-terms2"> Al hacer click en el botón "Enviar solicitud" aceptas los <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Autorizaciones+Solicitud+Familia+protegida+Chubb.pdf">terminos, condiciones</a>, <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Poli%CC%81tica+de+cookies+Cencosud.pdf">politica de cookies</a> y <a target="_blank" href="https://cencosud.zonic3.com/habeas-data">políticas de tratamiento de datos</a> de Seguros Cencosud </p>
                         <center>
               </div>
               </div>
               <center><div class="btn-group">
               <div>
                         
                              <button type="submit" class="btn btn-primary">
                                   <a class="a-white">Enviar solicitud</a>
                              </button>
                         
                    </div>&nbsp
               </div></center>

               <div class="form-group">
               <img class="gallager-form" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
               </div>
                                   
               </div>

          </form>
     </div>
     </div>
     </div>
     </div>

     <div id="openModalThree" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
          <br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">

          <div class="formulariotwo">
                    
          <h1>¡GRACIAS POR TU SOLICITUD!</h1>
          
               &nbsp&nbsp<div>
                         <h2 class="h2-orange">TE ESTAREMOS LLAMANDO EN EL TRANSCURSO DEL DÍA</h2>
                         
                    </div>&nbsp
                    <div>
                         <h2 class="h2-orange">EN EL SIGUIENTE HORARIO</h2>
                         </center>
                    </div>
                    <div class="opt">
                         <h2>Lunes a viernes<br>8:00 a.m a 5:00 p.m</h2>
                    </div>&nbsp
                    <div class="opt">
                         <h2>Sábados<br>8:00 a.m a 12:00 p.m</h2>
                    </div>&nbsp


                         <div class="content-opt">
                         <div class="row">
                         <div class="col-md-1">
                              <div class="opt-h">
                              </div>     
                         </div>

                         <div class="col-md-9">
                              <div class="opt">
                              <center><a href="https://api.whatsapp.com/send?phone=573057342036&text=&source=&data=&app_absent="><img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/redes_whatsapp.svg">
                              <p class="p-asist-opt-tel">Si quieres asesoría  puedes comunicarte a nuestro WhatsApp</p></a></center>
                              </div> 
                         </div>

                         <div class="col-md-1">
                              <div class="opt-h">
                              </div> 
                         </div>
                         </div>
                         </div>
     </div>
     </div>
     </div>
     </div>

</div>
</div>
</div>


<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/value.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/validate2.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/choice_doc.js"></script>
</main>
@endsection