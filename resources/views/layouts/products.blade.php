@extends('layouts.init-prod')

@section('titulo')

<div class="title-prod"><h1>BIENVENIDO A TUS PRODUCTOS SEGUROS CENCOSUD</h1></div>
<div class="title-prod"><h2>Selecciona el producto que deseas consultar</h2></div>

 @endsection

@section('contenido')

    <div class="table-prod">
    <table class="table table-striped">
        <thead class="thead-prod">
            <tr>
            <th scope="col">Producto</th>
            <th scope="col">No Póliza</th>
            <th scope="col">Identificación</th>
            <th scope="col">Nombre</th>
            <th scope="col">Más información</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">1</th>
            <td>Estado</td>
            <td>1234567</td>
            <td>John Retavisca</td>
            <td>Detalle</td>
            </tr>
        </tbody>
        </table>
        </div>

        <div class="date-prod"><p>Fecha de ingreso: DD/MM/AA</p></div>

</body>
</html>


@endsection