@extends('base.init2')
@section('content')
<main>
     <section id="banner">
          <img class="bannermovile" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-banner-home-resp.jpg">
          <img style="max-height: 400px;" class="bannerbig" src="https://connect-static-files.s3.amazonaws.com/asistencia/img-banner-home.jpg">
          <div class="contenedor">
          </div>
     </section>
     <section id="adquiere">
          <div class="formulario-asistencia-resp">
               <h2 class="h2-24-7">24/7</h2>
               <center><h2 class="h2-home">TE RESPALDAMOS</h2>
               <h2 class="h2-home">EN LO QUE NECESITES</h2></center>
               </br></br></br>
          </div>
     </section>
     <section id="section-imgs-asist"> <!-- class: funnel-container-plans -->
          <h2 class="h2-container-up-2">TENEMOS PARA TI</h2><br>
        <div class="container-asist-2">
          <ul class="slider">
          <li id="slide5">
          <div class="opt-asist-autos"><center>
               <div class="home-prod"> 
               <p class="p-asist-opt" style="margin-bottom: 1rem;">TU VIDA RESUELTA</p>
               <p style="font-size: 0.8rem; margin-bottom: 0.5rem;">Las asistencias de Autos de Seguros Cencosud te ofrecen las coberturas necesarias para cualquier ocación y novedad que se pueda presentar en el camino.</p>
               <a href="{{route('auto.index')}}" class="a-white"><button type="submit" class="btn btn-primary2">
               <b>CONOCE MÁS</b></button></a>
               </center>               
          </div> 
          </li>
          <li id="slide6">
          <div class="opt-asist-motos">
               <center>   
               <div class="home-prod"> 
               <p class="p-asist-opt" style="margin-bottom: 1rem;">SIEMPRE CONTIGO</p>
               <p style="font-size: 0.8rem; margin-bottom: 0.5rem;"> &nbsp;&nbsp;&nbsp;&nbsp;Sabemos que lo importante es tu tranquilidad, es por esto que en Seguros Cencosud encontraras las mejores asistencias para tu moto.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
               <a href="{{route('moto.index')}}" class="a-white"><button type="submit" class="btn btn-primary2">
               <b>CONOCE MÁS</b></button></a>    
               </center>
          </div> 
          </li>
          <li id="slide7">
          <div class="opt-asist-multi">
               <center>
               <div class="home-prod"> 
               <p class="p-asist-opt" style="margin-bottom: 1rem;">VIVE SIN PREOCUPARTE</p>
               <p style="font-size: 0.8rem; margin-bottom: 0.5rem;">En seguros Cencosud te acompañamos en los momentos más importantes, por esto tenemos las mejores asistencias para cualquier novedad que se pueda presentar.</p>
               <a href="{{route('multi.index')}}" class="a-white"><button type="submit" class="btn btn-primary2">
               <b>CONOCE MÁS</b></button></a>
               </center>
          </div>
          </li>
          </ul>
        </div>
     <div class="menu-slide">
          <ul class="menu">
          <li>
               <a href="#slide5">A</a>
          </li>
          <li>
               <a href="#slide6">B</a>
          </li>
          <li>
               <a href="#slide7">C</a>
          </li>
          </ul>
     </div>
     </section>
     <section id="section-last-adu"> <!-- funnel-contact -->
          <article class="article-people">
          <center><img class="contactenos-asist" img src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-contacto-home.png"></center>
          </article> 
          <article class="article-contact">
          <h2 class="h2-contactenos">CONTÁCTANOS</h2>
          <div class="formulario-contactenos">
          <form method="post" action="{{route('digital.cos_andi_home')}}" onsubmit="return validate2()">@csrf
               <center><div class="btn-group">&nbsp&nbsp
               <div class="form-group">
                    <input name="nombre" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre" required>
               </div>&nbsp
               <div class="form-group">
                    <input name="apellido" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Apellidos" required>
               </div>
               </div>&nbsp
               <div class="form-group">
                    <!-- <input name="email" type="email" class="form-control"  placeholder="Correo electrónico" required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com"> -->
                    <input name="celular" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" type="number" class="form-control" placeholder="Ingresa tu número celular" required>
               </div>
               <select id="select2" name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                    <option disabled selected value="0">Productos</option>
                    <option value="33631">AUTO - $17.200</option>
                    <option value="36226">MOTO - $12.200</option>
                    <option value="38821">MULTIASISTENCIA - $26.750</option> 
               </select></center><br>
               <center>
               <div>
                    <button type="submit" class="btn btn-primary2">
                         <a class="a-white">ENVIAR</a>
                    </button>
                    &nbsp
               </div>
               </center>
               </div>
               </form>
               </div>
          </article> 
     </section>
<!-- OpenModal 2 -->
     <div id="openModalTwo" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a><br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">
          <div class="formulariotwo">
                    
          <h1>¡GRACIAS POR TU SOLICITUD!</h1>&nbsp&nbsp
               <div>
                    <h2 class="h2-orange">TE ESTAREMOS LLAMANDO EN EL TRANSCURSO DEL DÍA</h2>    
               </div>&nbsp
               <div>
                    <h2 class="h2-orange">EN EL SIGUIENTE HORARIO</h2>
                    </center>
               </div>
               <div class="opt">
                    <h2>Lunes a viernes<br>8:00 a.m a 5:00 p.m</h2>
               </div>&nbsp
               <div class="opt">
                    <h2>Sábados<br>8:00 a.m a 12:00 p.m</h2>
               </div>&nbsp

               <div class="content-opt">
               <div class="row">
               <div class="col-md-1">
                    <div class="opt-h">
                         </div>     
                    </div>
                    <div class="col-md-9">
                    <div class="opt" style="min-width: 11rem;">
                    <center><a href="https://api.whatsapp.com/send?phone=573057342036&text=&source=&data=&app_absent="><img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/redes_whatsapp.svg">
                    <p class="p-asist-opt-tel">Si quieres asesoría  puedes comunicarte a nuestro WhatsApp</p></a></center>
                    </div> 
               </div>

               <div class="col-md-1">
                    <div class="opt-h">
                    </div> 
               </div>
               </div>
          </div>
     </div>
     </div>
</div> 
     <script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/value.js"></script>
     <script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/validate2.js"></script>
     <script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/choice_doc.js"></script>
     <script>window.addEventListener( "pageshow", function ( event ) { var historyTraversal = event.persisted || ( typeof window.performance != "undefined" && window.performance.navigation.type === 2 ); if ( historyTraversal ) {window.location.reload(); } });</script>
</main>
@endsection