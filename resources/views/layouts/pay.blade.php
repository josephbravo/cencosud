<?php
$valor = $_GET["tot"]; 
?>

@extends('layouts.init-prod')

@section('contenido')

<div class="table-pay">

<div class="title-table">

<form method="GET" action="http://127.0.0.1:8081/confirm">

          <div class="h1-t"><center><h1 class="detail-pay">Detalles del pago seleccionado</h1></center></div>

          </div>
</div>


<div class="table-table"><table class="table table-hover">
 



    <tr>
      
      <td>Identificación</td>
      <td class="td-td">1032546782</td>

    </tr>
    <tr>
      
      <td>Nombres</td>
      <td class="td-td">Marta Lucia</td>
      
    </tr>
    <tr>
      
    <td>Apellidos</td>
      <td class="td-td">Torres Gomez</td>
     
    </tr>

    <tr>
      
    <td>Email registrado</td>
      <td class="td-td">maria@hotmail.com</td>
     
    </tr>

    <tr>
      
    <td>Número de póliza</td>
      <td class="td-td">Autos Estado</td>
     
    </tr>

    <tr>
      
    <td>Celular</td>
      <td class="td-td">320 987 5643</td>
     
    </tr>

    <tr>
      
    <td>Valor factura</td>
      <td class="td-td">$86.000</td>
     
    </tr>

    <tr>
      
    <td>Total a pagar</td>
      <td class="td-td">$<?php echo $valor;?></td>
     
    </tr>


</table></div>

<div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
            <center><button type="submit" class="btn btn-primary">
            <a class="a2" href="/confirm" >{{ __('Realizar pago') }}</a>
            </button></center>
            </div>

            
    </div></form>

<div class="data-policy">
    <table class="table table-striped" style="border-radius: 1em; overflow: hidden; border-collapse: collapse;">
        <thead class="thead-prod">
            <tr>
            <th scope="col">Número de factura</th>
            <th scope="col">Fecha de factura</th>
            <th scope="col">Número de orden</th>

            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">1345543</th>
            <td>27/02/2020</td>
            <td>9007126655</td>

            </tr>

     </div>
     
     
</table></div>

          <div class="text-foot"><p class="text-orange">Estimado cliente, si tienes más productos de Seguros Cencosud deberás realizar el pago de cada uno de manera independiente</p></div>
          <div class="text-foot"><p>
Cualquier persona que realice el pago en el sitio Pasarela de pagos, actuando libre y voluntariamente, autoriza a Cencosud Colombia S. A a través del proveedor del servicio EGM Ingeniería Sin Fronteras S. A. S y/o Place to Pay para que consulte y solicite información del comportamiento crediticio, financiero, comercial y de servicios a terceros.</p></div>

@endsection