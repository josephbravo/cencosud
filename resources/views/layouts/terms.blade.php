@extends('layouts.init-prod')
@section('contenido')


<div class="prince-item">

<form method="GET" name="form1"><center>
       <table>
          <h1>Términos y condiciones </h1>

          <p class="p-terms">Si no deseas recibir más correos electrónicos de SEGUROS CENCOSUD has click en la palabra “desuscribirse” al final de nuestros correos electrónicos y sigue las instrucciones.<br/><br>


          FINALIDADES DE LA CAPTURA DE DATOS: <br/><br>

          CENCOSUD COLOMBIA S.A. podrá usar los datos capturados de los Titulares para las siguientes actividades: I) Ejercer su derecho de conocer de manera suficiente al cliente/afiliado/usuario con quien se propone entablar relaciones, prestar servicios, y valorar el riesgo presente o futuro de las mismas relaciones y servicios;<br/><br>

          I) Ofrecer individual o conjuntamente a nombre propio, con terceros o a nombre de terceros, servicios financieros, comerciales y conexos, así como realizar campañas de promoción, marketing, publicidad, beneficencia o servicio social o en conjunto con terceros. Lo anterior en consideración a sus sinergias mutuas y su capacidad conjunta de proporcionar condiciones de servicio más favorables a sus clientes. En consecuencia, para las finalidades descritas, CENCOSUD COLOMBIA S.A. podrá:<br/><br>

          II) Conocer, almacenar y procesar toda la información suministrada por el Titular en una o varias bases de datos, en el formato que estime más conveniente.

          III) Ordenar, catalogar, clasificar, dividir o separar la información suministrada por el Titular. III) Verificar, corroborar, comprobar, validar, investigar o comparar la información suministrada por el Titular, con cualquier información de que disponga legítimamente, incluyendo aquella conocida por sus matrices, subordinadas, afiliadas o cualquier compañía del Grupo CENCOSUD.<br/><br>

          IV) Acceder, consultar, comparar y evaluar toda la información que sobre el Titular se encuentre almacenada en las bases de datos de cualquier central de riesgo crediticio, financiero, de antecedentes judiciales o de seguridad legítimamente constituida, de naturaleza estatal o privada, nacional o extranjera, o cualquier base de datos comercial o de servicios que permita establecer de manera integral e históricamente completa el comportamiento que como deudor, usuario, cliente, garante, endosante, afiliado, beneficiario, suscriptor, contribuyente y/o como titular de servicios financieros, comerciales o de cualquier otra índole.<br/><br>

          V) Analizar, procesar, evaluar, tratar o comparar la información suministrada por el Titular. A los datos resultantes de análisis, procesamientos, evaluaciones, tratamientos y comparaciones, les serán aplicables las mismas autorizaciones que otorgó en el Formulario de Autorización para la información suministrada por el Titular.<br/><br>

          VI) Estudiar, analizar, personalizar y utilizar la información suministrada por el Titular para el seguimiento, desarrollo y/o mejoramiento, tanto individual como general, de condiciones de servicio, administración, seguridad o atención, así como para la implementación de planes de mercadeo, campañas, beneficios especiales y promociones. CENCOSUD COLOMBIA S.A. podrá compartir con sus accionistas y con compañías controlantes, controladas, vinculadas, afiliadas o pertenecientes al mismo grupo empresarial, o con los aliados de negocios que se sometan a las condiciones de la presente autorización los resultados de los mencionados estudios, análisis, personalizaciones y usos, así como toda la información y datos personales suministrados por el Titular.<br/><br>

          VII) Reportar, comunicar o permitir el acceso a la información suministrada por el Titular o aquella de que disponga sobre el Titular a las centrales de riesgo crediticio, financiero, comercial o de servicios legítimamente constituidas, a entidades financieras, de acuerdo con las normas aplicables.<br/><br></p>
           </center>
        </table>
    <center>
    <button type="submit" class="btn btn-primary">
            <a class="a2" href="http://127.0.0.1:8000/" >{{ __('VOLVER') }}</a>
            </button>

       </form></div>
     </center>

@endsection