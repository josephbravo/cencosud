@extends('layouts.init-prod')

@section('contenido')

<div class="table-pay">
 
<div class="title-table">

<form method="post" action="{{route('peticion.store')}}">
@csrf
    <div class="h1-t">
      <center>
        <h1 class="detail-pay">Detalles del pago seleccionado</h1>  
      </center>
    </div>
    </div>
    </div>

    <div class="table-table">
        <table class="table table-hover">
          <tr>
            <td>Identificación</td>
            <td class="td-td">{{$doc}}</td>
          </tr>
          <tr>
            <td>Nombres</td>
            <td class="td-td">{{$nombreDef}}</td>
          </tr>
          <tr>
          <td>Apellidos</td>
            <td class="td-td">{{$apellidoDef}}</td>
          </tr>
          <tr>
          <td>Email registrado</td>
            <td class="td-td">{{$emailDef}}</td>
          </tr>
          <tr>
          <td>Número de póliza</td>
            <td class="td-td">{{$pol}}</td>
          </tr>
          <tr>
           <td>Celular</td>
            <td class="td-td">{{$celularDef}}</td>
          </tr>
          <td>Valor prima</td>
            <td class="td-td">${{$prima}}</td>
          </tr>
          <td>Valor IVA</td>
            <td class="td-td">${{$iva}}</td>
          </tr>
          <tr>
          <td>Valor factura</td>
            <td class="td-td">${{$tot}}</td>
          </tr>
          <tr>
          <td>Total a pagar</td>
            <td class="td-td">${{$tot}}</td>
          </tr>
      </table>


      <input type="hidden" name="documento" value="{{$doc}}">
      <input type="hidden" name="nombres" value="{{$nombreDef}}">
      <input type="hidden" name="apellidos" value="{{$apellidoDef}}">
      <input type="hidden" name="email" value="{{$emailDef}}">
      <input type="hidden" name="poliza" value="{{$pol}}">
      <input type="hidden" name="celular" value="{{$celularDef}}">
      <input type="hidden" name="factura" value="{{$total}}">
      <input type="hidden" name="prima" value="{{$prima}}">
      <input type="hidden" name="iva" value="{{$iva}}">
      <input type="hidden" name="pagar" value="{{$total}}">
      <input type="hidden" name="referencia" value="{{$referencia}}">
      <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                  <center>
                    <button type="submit" class="btn btn-primary">
                      <a class="a2">Realizar pago</a>
                    </button>
                </center>
            </div>   
      </div>
    </div>
</form>

<div class="data-policy">
    <table class="table table-striped" style="border-radius: 1em; overflow: hidden; border-collapse: collapse;">
        <thead class="thead-prod">
            <tr>
            <th scope="col">Producto</th>
            <th scope="col">Fecha de factura</th>
            <th scope="col">Referencia de pago</th>

            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">{{$prod}}</th>
            <td>
              <?php
                echo date("d-m-Y");
              ?>
            </td>
            <td>{{$referencia}}</td>

            </tr>

     </div>
     
     
</table></div>

          <div class="text-foot"><p class="text-orange">Estimado cliente, si tienes más productos de Seguros Cencosud deberás realizar el pago de cada uno de manera independiente</p></div>
          <div class="text-foot"><p>
Cualquier persona que realice el pago en el sitio Pasarela de pagos, actuando libre y voluntariamente, autoriza a Cencosud Colombia S. A a través del proveedor del servicio EGM Ingeniería Sin Fronteras S. A. S y/o Place to Pay para que consulte y solicite información del comportamiento crediticio, financiero, comercial y de servicios a terceros.</p></div>
<br><br>
<p>{{$json}}</p>

@endsection