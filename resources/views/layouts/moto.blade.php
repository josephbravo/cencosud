@extends('base.init2')
@section('content')
<main>
     <section id="banner">
          <img class="bannermovile" style="min-height: 10rem;" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-banner-motos-mobile.jpg">
          <img style="max-height: 370px;min-height: 19.5rem;" class="bannerbig" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-banner-motos.jpg">
          <div class="contenedor">
          </div>
     </section>
     <section id="adquiere">
     <div class="formulario-autos-resp">
          <h1 class="h2-24-7">MOTOS</h1>
          <h2 class="h2-home-orange" style="margin-bottom: -0.5rem;">ASISTENCIAS</h2>
          <h2 class="h2-home-orange">DESDE $12.200</h2>

          <center>
          <input type="hidden" name="id_producto" value="2">
          <button type="submit" class="btn btn-primary2">
          <a href="#openModal" class="a-white"><b>COMPRA ONLINE</b></a>
          </button><br>
          </form>
          </center>

          <center>
          <button type="submit" class="btn btn-primary2" style="max-width: 11rem;">
          <a href="#openModalTwo" class="a-white"><b>COMPRA POR TELÉFONO</b></a>
          </button>
          </center>
     
               </div>
          </div>
     </section>

     <section id="section-imgs-asist-moto"> <!-- class: funnel-container-plans -->
          <h2 class="h2-autos">Asistencias de moto es un producto que te brinda tranquilidad a la hora de movilizarte. Está creado para dar mayor cobertura a la hora de usar tu moto y lo podrás adquirir para complemetar tu seguro todo riesgo o tomarlo de forma independiente y disfrutar todas las asistencias exclusivas</h2><br>
          <h2 class="h2-container-up">TENEMOS MUCHAS MANERAS DE ACOMPAÑARTE EN EL CAMINO</h2><br>
     <div class="container-autos">
          <ul class="slider">
          <li id="slide5">
          <div class="opt-moto-resp""><center>
          <img class="icon-autos1" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icon1-motos.png" alt="">
               <div class="home-autos"> 
               <p class="p-asist-opt" id="planmotoa" style="margin-bottom: 0rem;">TRAMITACIÓN EN ENTIDADES DE TRÁNSITO</p>
               <p style="font-size: 1rem; margin-bottom: 0rem;">Hasta $250.000</p>
          </center>               

          </div> 
          </li>
          <li id="slide6">
          <div class="opt-moto-resp""><center>
          <img class="icon-autos1" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icon2-motos.png" alt="">
                
               <div class="home-autos"> 
               <p class="p-asist-opt" id="planmoto" style="margin-bottom: 0rem;">TRAMITACIÓN DE INFRACCIONES</p>
               <p style="font-size: 1rem; margin-bottom: 0rem;">Hasta $250.000</p>          
          </center>
               </div> 
          </li>
          <li id="slide7">
          <div class="opt-moto-resp""><center>
          <img class="icon-autos1" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icon3-motos.png" alt="">  
               <div class="home-autos"> 
               <p class="p-asist-opt" id="planmoto" style="margin-bottom: 0rem;">TAXI SOS</p>
               <p style="font-size: 1rem; margin-bottom: 0rem;">Hasta $600.000</p>          
          </button>
               </center>
               </div>
          </li>
          </ul>
     </div>
     <div class="menu-slide">
          <ul class="menu">
          <li>
               <a href="#slide5">A</a>
          </li>
          <li>
               <a href="#slide6">B</a>
          </li>
          <li>
               <a href="#slide7">C</a>
          </li>
          </ul>
          </div>
     <div class="container-autos">
          <ul class="slider">
          <li id="slide8">
          <div class="opt-moto-resp""><center>
          <img class="icon-autos1" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icon4-motos.png" alt="">
               <div class="home-autos"> 
               <p class="p-asist-opt" id="planmoto" style="margin-bottom: 0rem;">PÉRDIDA DE LLAVES</p>
               <p style="font-size: 1rem; margin-bottom: 0rem;">Hasta $600.000</p>
          </center>               
          </div> 
          </li>
          <li id="slide9">
          <div class="opt-moto-resp""><center>
          <img class="icon-autos1" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icon5-autos.png" alt="">
               
               <div class="home-autos"> 
               <p class="p-asist-opt" id="planmoto" style="margin-bottom: 0rem;">GRÚA</p>
               <p style="font-size: 1rem; margin-bottom: 0rem;">Hasta $600.000</p>          
          </center>   
          </div> 
          </li>
          <li id="slide10">
          <div class="opt-moto-resp""><center>
          <img class="icon-autos1" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icon6-autos.png" alt="">   
               <div class="home-autos"> 
               <p class="p-asist-opt" id="planmoto" style="margin-bottom: 0rem;">CARRO TALLER</p>
               <p style="font-size: 1rem; margin-bottom: 0rem;">Hasta $600.000</p>          
          </button>
               </center>
               </div>
          </li>
          </ul>

          </div>

          <div class="menu-slide">
               <ul class="menu">
               <li>
                    <a href="#slide8">A</a>
               </li>
               <li>
                    <a href="#slide9">B</a>
               </li>
               <li>
                    <a href="#slide10">C</a>
               </li>
               </ul>
               </div>


          <div class="container-autos">

          <ul class="slider">

          <li id="slide11">
          <div class="opt-moto-resp""><center>
          <img class="icon-autos1" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icon7-motos.png" alt="">
          
               <div class="home-autos"> 
               <p class="p-asist-opt" id="planmoto" style="margin-bottom: 0rem;">ASISTENCIA DE COMBUSTIBLE</p>
               <p style="font-size: 1rem; margin-bottom: 0rem;">Hasta $600.000</p>
          </center>               

          </div> 
          </li>
          <li id="slide12">
          <div class="opt-moto-resp""><center>
          <img class="icon-autos1" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icon5-motos.png" alt="">
                
               <div class="home-autos"> 
               <p class="p-asist-opt" id="planmotoa" style="margin-bottom: 0rem;">TRASLADO TALLER A MANTENIMIENTO</p>
               <p style="font-size: 1rem; margin-bottom: 0rem;">Hasta $600.000</p>          
          </center>
               
               </div> 
          </li>
          <li id="slide13">
          <div class="opt-moto-resp""><center>
          <img class="icon-autos1" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icon9-autos.png" alt="">
               
               <div class="home-autos"> 
               <p class="p-asist-opt" id="planmoto" style="margin-bottom: 0rem;">DESINFECCIÓN DEL MOTO</p>
               <p style="font-size: 1rem; margin-bottom: 0rem;">Hasta $300.000</p>          
          </button>
               </center>
               </div>
          </li>
          </ul>

     </div>



     <div class="menu-slide">

     <ul class="menu">
     <li>
          <a href="#slide11">A</a>
     </li>
     <li>
          <a href="#slide12">B</a>
     </li>
     <li>
          <a href="#slide13">C</a>
     </li>


     </ul>
     </div>
     </section>

     <h2 class="h2-autos">Conoce todas las coberturas <a href="https://connect-static-files.s3.amazonaws.com/pagos/Motosv1RV.pdf" target="_blank">aquí</a></h2><br>

     <section id="section-last-adu"> <!-- funnel-contact -->

     <div class="autos-compra">

     <h1 class="h2-autos-w">MOTOS</h1>
     <h1 class="h2-autos-w2">ASISTENCIAS DESDE $12.200</h1>

          <center>
          <a href="#openModal" class="a-white"><input type="hidden" name="id_producto" value="2">
          <button type="submit" class="btn btn-primary2">
          <b>COMPRA ONLINE</b>
          </button></a><br>
          </form>
          </center>

          <center>
          <a href="#openModalTwo" class="a-white"><button type="submit" class="btn btn-primary2" style="max-width: 11rem;">
          <b>COMPRA POR TELÉFONO</b>
          </button></a>
          </center>
     </div>

     </section>
<!-- OpenModal 1 -->
     <div id="openModal" class="modalDialogAsist">
	<div>
		<a href="#close" title="Close" class="close">x</a><br>
     <div class="scroll-andi">

     <div class="formularioasistencias">
     <form method="post" action="{{route('cart.data_pay')}}" onsubmit="return validate()" id="form-pay">@csrf
     <!-- BEFORE route('cart.check') -->
     <h1 class="h1-frm">PARA SEGUIR CON LA COMPRA POR FAVOR, COMPÁRTENOS ESTA INFORMACIÓN</h1>
          <input type="hidden" name="id_producto" value="2">

     <div class="form-group" style="margin-bottom: 0.3rem;">
     <input name="nombre" pattern="[A-Za-z ]*" title="Campo inválido" onkeypress="return validar(event)" type="text" class="form-control" placeholder="Nombre" required>
               </div>
                    
               <div class="form-group" style="margin-bottom: 0.3rem;">
               <input name="apellido" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" placeholder="Apellidos" required>
               </div>

               <center><div class="btn-group" style="margin-bottom: 0.3rem;">
               <p class="date-born" style="color:white">En qué fecha naciste?</p>
                    <div>
                    <input name="date" type="date" id="start" class="form-control" name="trip-start" min="1910-01-01" max="2002-12-31">
                    </div>
               </div></center>

               <div class="form-group" style="margin-bottom: 0.3rem;">
               <select name="tipdocumento" class="form-control" onchange="choice_doc(this)" placeholder="PRODUCTO - PRECIO" required>
                    <option value="-">Tipo documento</option>
                    <option value="CC">CC</option>
                    <option value="CE">CE</option>
                    <option value="PPN">PAS</option>
               </select>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
                    <input name="documento" id="ident" pattern="[A-Za-z ]*[0-9]{5,12}" title="Campo inválido. Minimo 5 dígitos máximo 10 dígitos" maxlength="10"  type="text" class="form-control" placeholder="Numero de documento" required>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
               <input name="email" type="email" class="form-control" placeholder="Correo electrónico" required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>

               <center><div class="btn-group" style="margin-bottom: 0.3rem;">
               <p class="date-born" style="color:white">Periodicidad de pago</p>
                    <div>
                    <select onchange="choice_doc(this)" id="periodicidad" name="totalselect" class="form-control" onchange="choice_doc(this)" placeholder="PRODUCTO - PRECIO" required>
                    <option value="12200">Mensual</option>
                    <option value="2">Trimetral</option>
                    <option value="3">Semestral</option>
                    <option value="4">Anual</option>
               </select>
                    </div>
               </div></center>

               <div class="form-group" style="margin-bottom: 0.3rem;">
                    <input name="direccion" type="text" class="form-control" placeholder="Dirección dimicilio" required>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
               <select name="ciudad" class="form-control" placeholder="Ciudad" required>
               <option value="" disabled selected>Ciudad domicilio</option>
               @foreach ($ciudades as $item)
               <option value="{{$item->id}}">{{$item->nombre}}</option>
               @endforeach
               </select>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
                    <input name="celular" pattern="^[0-9]{7,10}$" title="Campo inválido. Minimo 7 dígitos máximo 10 dígitos" maxlength="10" type="text" class="form-control"  placeholder="Celular" required>
               </div>

               <div class="form-group" style="margin-bottom: 0.3rem;">
                    <input name="placa" title="Campo inválido" type="text" minlength="5" maxlength="6" class="form-control"  placeholder="Placa moto" required>
                    <input type="hidden" name="producto" value="MOTO">
               </div>   

               <div class="cart-compra-divauto">
                    <div class="form-group" style="margin-bottom: 0.3rem;">
               <input name="checkadd" class="form-check-auto" type="checkbox" id="inlineCheckbox1" value="approv">
                    <div>
                    <p class="accept-tycauto">Deseo que mi cuota se debite automáticamente haciendo suscripción de la tarjeta de crédito utilizada en este proceso de pago</p>
                    </div>

                    <div class="form-group" style="margin-bottom: 0.3rem;">
          
                    <input class="form-check-auto" type="checkbox" id="inlineCheckbox1" value="option1" required>
               
                    <div>
                    <p class="accept-tycauto">Acepto y autorizo el uso de mi información de acuerdo a los términos y condiciones y política de cookies.</p>

                    </div>
               </div>
               </div>

               <div class="value-total">
                    <div class="textvalue"><p>Valor Asistencia</p>
                         <input id="valorpagar" name="total" class="totalapagar" type="text" placeholder="$12.200" />
                    </div>
               </div>         

               <center><div class="form.group">
               <div> 
               <input type="hidden" name="redirect" value="1">
               <button type="submit" form="form-pay" class="btn btn-primary2" id="btn-form-auto" name="btn1" value="1">
               <b>Pagar</b>
               </button>
               </div>&nbsp
               </div></center>
               <center><div class="form.group">
               <div>  
               <input type="hidden" name="redirect" value="2">
               <button type="submit" form="form-pay" class="btn btn-primary2" id="btn-form-auto2" name="btn1" value="2">
               <b>Pagar con efectivo</b>
               </button>
               </div>&nbsp
               </div></center>

               

               </div>
          </form>
               </div>
          </div>
	</div>
</div>

<!-- OpenModal 2 -->

<div id="openModalTwo" class="modalDialogAsist">
	<div style="height: 32rem;padding: 1rem; width: 25rem;" id="telform">
		<a href="#close" title="Close" class="close">x</a>
          <div>

          <div class="formularioasistenciastel">
		<form method="post" action="{{route('digital.cos_andi')}}" onsubmit="return validate2()">@csrf

               <h1 class="h1-frm">COMPRA POR TELÉFONO</h1>
               <center><div class="btn-group"><br><br>  
                    
               <div>
                    <input name="nombre" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre" required>
               </div>&nbsp
               <div>
                    <input name="apellido" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Apellidos" required>
               
               </div>
               </div></center>

               <div class="form-group">
                    <input oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" name="celular" type="number" class="form-control"  placeholder="Ingresa tu número celular" required>
               </div>
               <div class="form-group">
                    <input name="email" type="email" class="form-control"  placeholder="Ingresa tu correo electrónico" required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>
               <div class="form-group">
                    <input oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" name="documento" type="number" class="form-control"  placeholder="Número de identificación" required>
               </div>
               <div class="form-group">
               <select id="select2"  name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                    <option value="0">Seleccione</option>
                    <option value="36226">MOTO - $12.200</option>
               </select>&nbsp

               <div class="btn-group">
                    <input type="hidden" name="producto" value="MOTO">
                    <div>
                    <center>
                         <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
                    </center>
                    </div>&nbsp
               <div>
                    <center>
                         <p class="accept-tycauto"> Al hacer click en el botón "Enviar solicitud" aceptas los <a target="_blank" style="color: white;" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Autorizaciones+Solicitud+Mascota+Plus++Mejorado+Chubb.pdf">terminos, condiciones</a>, <a target="_blank"style="color: white;"  href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Poli%CC%81tica+de+cookies+Cencosud.pdf">politica de cookies</a>  y <a target="_blank" style="color: white;" href="https://cencosud.zonic3.com/habeas-data">políticas de tratamiento de datos</a> de Seguros Cencosud </p>
                    <center>
                    </div>
               </div>
               <center>
               <div class="btn-group">
               <div>
                    <button type="submit" class="btn btn-primary2" id="btn-form-auto" >
                         <b>Enviar solicitud</b>
                    </button>
               </div>&nbsp
               </div>
               </center>
               </div>
          </form>
     </div>
     </div>
     </div>
     </div>

<!-- OpenModal 3 -->
     <div id="openModalThree" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a><br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">
          <div class="formulariotwo">
                    
          <h1>¡GRACIAS POR TU SOLICITUD!</h1>&nbsp&nbsp
               <div>
                    <h2 class="h2-orange">TE ESTAREMOS LLAMANDO EN EL TRANSCURSO DEL DÍA</h2>    
               </div>&nbsp
               <div>
                    <h2 class="h2-orange">EN EL SIGUIENTE HORARIO</h2>
                    </center>
               </div>
               <div class="opt">
                    <h2>Lunes a viernes<br>8:00 a.m a 5:00 p.m</h2>
               </div>&nbsp
               <div class="opt">
                    <h2>Sábados<br>8:00 a.m a 12:00 p.m</h2>
               </div>&nbsp

               <div class="content-opt">
               <div class="row">
               <div class="col-md-1">
                    <div class="opt-h">
                         </div>     
                    </div>
                    <div class="col-md-9">
                    <div class="opt" style="min-width: 11rem;">
                    <center><a href="https://api.whatsapp.com/send?phone=573057342036&text=&source=&data=&app_absent="><img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/redes_whatsapp.svg">
                    <p class="p-asist-opt-tel">Si quieres asesoría  puedes comunicarte a nuestro WhatsApp</p></a></center>
                    </div> 
               </div>
               <div class="col-md-1">
                    <div class="opt-h">
                    </div> 
               </div>
               </div>
          </div>
     </div>
     </div>
     </div>
</div>
</div>
</div>
@section('script')
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/value.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/validate2.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/choice_doc.js"></script>
<script>window.addEventListener( "pageshow", function ( event ) { var historyTraversal = event.persisted || ( typeof window.performance != "undefined" && window.performance.navigation.type === 2 ); if ( historyTraversal ) {window.location.reload(); } });</script>
</main>

<!--<script> 
function choice_doc(select) 
{ 
var elNombre = document.getElementById('valorpagar');
console.log(elNombre.value);
var mOption = "111";
if (select.value==1) 
{ alert("Escogiste Mensual");
document.getElementById("autocomplete").options[0].selected = true;;
 } if (select.value==2) { 
      alert("Escogiste Timestral");
     document.getElementById("autocomplete").options[4].selected = true;; 
} if (select.value==3) { 
      alert("Escogiste Semestral");
     document.getElementById("autocomplete").options[4].selected = true;;
     elNombre.value = mOption.text;
} if (select.value==4) { 
      alert("Escogiste Anual");
     document.getElementById("autocomplete").options[4].selected = true;; }
  else{ } } </script>
-->
<script>
document.getElementById('periodicidad').onchange = function() {
  /* Referencia al option seleccionado */
  var mOption = this.options[this.selectedIndex];
  /* Referencia a los atributos data de la opción seleccionada */
  var mData = mOption.dataset;

  /* Referencia a los input */
  var elNombre = document.getElementById('valorpagar');

  /* Asignamos cada dato a su input*/
  if (mOption.value==12200) { 
  elNombre.value = "$12.200";
  };
  if (mOption.value==2) { 
     elNombre.value = "$36.600";
     };
  if (mOption.value==3) { 
     elNombre.value = "$73.200";

     };
  if (mOption.value==4) { 
     elNombre.value = "$146.400";

     }
};</script>

@endsection
@endsection