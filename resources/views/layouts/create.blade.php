@extends('layouts.init-prod')

@section('titulo')
<div class="table-prod">
    <br>
    <h1 style="float: left;">Mis seguros / Todo riesgo Chubb</h1>
    <br>
    <br>
    <br>
    <br>
</div>


 @endsection

@section('contenido')

<div class="table-prod">
<form method="post" action="{{route('peticion.index')}}">
                        @csrf

    <table class="table table-striped" style="border-radius: 1em; overflow: hidden; border-collapse: collapse;">
        <thead class="thead-prod">
            <tr>
                <th class="tr-c" scope="col">Producto</th>
                <th scope="col">No Póliza</th>
                <th scope="col">Identificación</th>
                <th scope="col">Nombre</th>
                <th scope="col">Valor Prima</th>
                <th scope="col">Valor IVA</th>
                <th scope="col">Valor Cuota</th>
                <th scope="col">Saldo</th>
                <th scope="col">Vigencia</th>
                <th scope="col">Couta</th>
                <th scope="col">Estado de póliza</th>
                <th scope="col">Motivo de Rechazo</th>
                <th scope="col">Pagar producto</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($carteras as $cartera)
            <tr>
                <th scope="row">{{ $product }}</th>
                <td>{{ $cartera->NUMPOL }}</td>
                <td>{{ number_format($cartera->CODCLI, 0) }}</td>
                <td>{{ $nombreDef }}</td>
                <td id="td-value">${{ number_format($cartera->VLRPRI, 2) }}</td>
                <td id="td-value">${{ number_format($cartera->VLRIVA, 2) }}</td>
                <td id="td-value">${{ number_format($cartera->VLRFAC, 2) }}</td>
                <td>${{ number_format($cartera->VLRFAC, 2) }}</td>
                <td>{{ $cartera->MESDEFACTURACION }}</td>
                <td>{{ $cartera->CUOTA }}</td>
                <td>{{ $cartera->ESTADO }}</td>
                <td>{{ $cartera->CAUSAL }}</td>
            <td>
            <input type="{{ $cartera->checking }}" id="cbox1" value="{{ $cartera->VLRFAC }}" onclick="sume(this)"></td>
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>

    <script>
        var total = 0;
        var td = document.getElementById('td-value');

        function sume(pr){
        var data = Number(pr.value)
        if(!pr.checked) {data *= -1}
        total+= data
        value_total = Intl.NumberFormat().format(total)

        document.getElementById("tot").value = value_total
        document.getElementById("sum").value = value_total
        }
    </script>
    @foreach ($carteras as $cartera)
    <input type="hidden" name="productoHidden" value="{{ $product }}">
    <input type="hidden" name="polizaHidden" value="{{ $cartera->NUMPOL }}">
    <input type="hidden" name="documentoHidden" value="{{ $cartera->CODCLI }}">
    <input type="hidden" name="prima" value="{{ $cartera->VLRPRI }}">
    <input type="hidden" name="iva" value="{{ $cartera->VLRIVA }}">
    @endforeach
    <form action="pay.blade.php" method='get'>
        <div class="total">
            <h2>Total a pagar:</h2>
            
            <h2>$<input type="text" readonly="readonly" name="tot" id="tot"></h2>
        </div>
        <br>
        <center>
            <p>Una vez su pago sea aprobado en el transcuros de 24h estara aplicado en el sistema</p> 
        </center>
        <br>
                                                                                                 
        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <center>
                <button type="submit" class="btn btn-primary" onclick="myFunction()">Pagar</button>
                </center>
                <a class="a2"></a>
            </div>

            <script>
                function myFunction() {
                var formulario = document.getElementById("myform");

                if (document.getElementById("tot").value == 0) {
                alert("El valor a pagar no puede ser 0");
                event.preventDefault();}
                }
            </script>
        </div>
    </form>
    <br>
    <div class="date-prod">
        <p>Fecha de ingreso: 
            <?php
            echo date("d-m-Y");
            ?>
        </p>
    </div>


@endsection
