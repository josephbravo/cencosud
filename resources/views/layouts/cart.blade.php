@extends('base.init2')

@section('content')
<link href="{{ asset('css/footable.min.css') }}" rel="stylesheet">
<script href="{{ asset('js/footable.min.js') }}" rel="stylesheet"></script>
<main>
<section id="section-last-adu"> <!-- funnel-contact -->
     <div class="autos-compra">
               @if (Cart::isEmpty())
               <h1 class="h2-autos-w2">SIGAMOS CON EL PROCESO DE COMPRA</h1>
               @else
               <h1 class="h2-autos-w2">YA CASI TERMINAMOS</h1>
               @endif
     </div>
</section>
<section class="section-dataproduct">
<div class="cart-compra">
@if (count(Cart::getContent()))
<h2 class="h2-container-up" id="h2-cart">Tienes {{count(Cart::getContent())}} productos en tu carrito de compras:</h2>
     @else
     <h2 class="h2-container-up" id="h2-cart">No tienes productos en tu carrito de compras</h2>@endif
     @if (count(Cart::getContent()))
     <div class="col-sm-8 bg-light">
     <div class="hscroll">
     <table class="table">
  <thead>
    <tr>
     <td style="font-size: 1rem;">PRODUCTO</td>
     <td style="font-size: 1rem;">PRECIO</td>
     <td style="font-size: 1rem;">CUOTAS</td>
     <td style="font-size: 1rem;">TOTAL</td>
    </tr>
  </thead>
  <tbody>
  @foreach (Cart::getContent() as $item)
    <tr>
    @if ($item->name=="AUTOS")
    <td><img class="product-add" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-auto-resumen.png"></img></td>
     @else
     @if ($item->name=="MOTOS")
     <td><img class="product-add" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-motos-resumen.png"></img></td>
     @else
     @if ($item->name=="MULTIASISTENCIAS")
     <td><img class="product-add" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-multi-resumen.png"></img></td>
     @else
      <td>{{$item->name}}</td>
      
      @endif
      @endif
      @endif
     <td>COP ${{ number_format($item->price, 2) }}</td>
     <td>
     <select id="select-cuotas" name="profesion" class="form-control" placeholder="Cuál es tu ocupación" required>
          <option value="1">1</option>
     </select>
     </td>      
     <td>COP ${{ $total }}</td>
     <td>
     <form action="{{route('cart.remove')}}" method="post">@csrf
          <input type="hidden" name="id_producto" value="{{$item->id}}">
          <button  id="btn-x" type="submit" class="btn btn-primary2">
          <a href="{{route('cart.remove')}}" class="a-white"><b>Eliminar</b></a>
     </button>
     </form>
     </td>
    </tr>
    @endforeach
  </tbody>
</table>
          </div>    
     </div>
     @elseif (\Session::has('success'))
     @else
     <h1>Carrito vacío</h1>
     <center><div style="max-width: 12rem;">
      <a href="{{route('asistencia.index')}}" class="a-white">
          <button type="button" class="btn btn-primary2">
          <b>VOLVER</b>
          </button></a></div>
          </center>
     @endif
     </div>
     <div class="cart-compra-auto">
     @if (count(Cart::getContent()))         
     @else
     <br>
     @endif
     <div class="card-pay">
     <div class="cart-tittle">
          <p class="p-resumen">RESUMEN</p>
     </div>
     <form action="{{route('cart.data_pay')}}" class="form-pagar" method="post">@csrf
          <div class="btn-group" style="margin-top: 0.6rem;">      
          <div>
          @if (Cart::isEmpty())
               <p class="p-producto">Producto:</p>
          @else
               <p class="p-producto2">Producto:</p>
               @endif
          </div>&nbsp
          <div>
          @foreach (Cart::getContent() as $item)
               @if (count(Cart::getContent()))
                    @if ($item->name == "AUTOS")
                         <p style="position: relative;right: -2rem;font-size: 1.3rem;">AUTO</p>
                    @endif
                    @if ($item->name == "MOTOS")
                         <p style="position: relative;right: -2rem;font-size: 1.3rem;">MOTO</p>
                    @endif
                    @if ($item->name == "MULTIASISTENCIAS")
                         <p style="position: relative;right: -2rem;font-size: 1.3rem;">MULTI</p>
                         <p style="position: relative;right: -2rem;font-size: 1.3rem;">ASISTENCIAS</p>
                    @endif
               @else
                    <p>----</p>
               @endif
          @endforeach

          @if (Cart::isEmpty())
               <p style="visibility: hidden;" style="font-size: 1.3rem;">-------------------</p>
               @else
               @endif
          </div>
     </div>&nbsp<br>
     <div class="btn-group">
          <div>
          <p class="p-precio">Precio:</p>
          </div>&nbsp
          <div>
          @if (count(Cart::getContent()))
          <p style="font-size: 1.3rem;">COP ${{ $precio }}</p>           
          @else
          <p style="font-size: 1.3rem;">COP $0.0</p>
          @endif
          </div>
     </div>&nbsp<br>
     <hr>
     <div class="btn-group">
          <div>
          <p class="p-precio">Total:</p>
          </div>&nbsp
          <div>
          @if (count(Cart::getContent()))
          <p style="font-size: 1.3rem;">COP ${{ $total }}</p>
          @else
          <p style="font-size: 1.3rem;">COP $0.0</p>
          @endif
          </div>
     </div>&nbsp
     <div class="btn-precio">
          <center>
          @if (count(Cart::getContent()))
               <button type="submit" class="btn btn-primary2">
          @else
               <button type="submit" class="btn btn-primary2" disabled="disabled" style="min-width: 12rem;">
          @endif
               <b>PAGAR</b>
               </button>
          </center>
          </div>
          <div class="btn-group">
          <div class="btn-group">
          <div>
          <center>
               <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
          </center>
          </div>&nbsp
          <div>
          <p class="accept-tyc">Acepto y autorizo el uso de mi información de acuerdo a los términos y condiciones y política de cookies.</p>
          </div>
          </div>&nbsp
          </div>
          </form>
          </div>
     </section>
     <section class="section-icons-pay">
          <div class="cart-method-pay">
          <div class="card-method">
               <p class="payonline"><img class="secure-icon" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icon-seguro.png"></img>Pago 100% seguro<p>
               <p style="font-size: 1.3rem;">Compra fácil y seguro con cualquier medio de pago.<p></div>
     <div class="img-icon-pay">
     <main class="main-img">
     <center>
     <img class="icons-pay" id="icons-pay1" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-placetopay.png"></img>
     <img class="icons-pay" id="icons-pay3" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-pse.png"></img>
     <img class="icons-pay" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-visa.png"></img>
     <img class="icons-pay" id="icons-pay4" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-mastercard.png"></img>
     </center>
     </main>
     </div>
     <div class="card-what">
     <a href="https://api.whatsapp.com/send?phone=573057342036&text=&source=&data=&app_absent=" target="_blank"><p class="payonline"     ><img class="what-icon" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-icon-whatsapp.png"></img></a>
     Si necesitas asesoría</p> 
     <p style="font-size: 1.3rem;">puedes escribirnos a nuestro canal de whatsapp <a href="https://api.whatsapp.com/send?phone=573057342036&text=&source=&data=&app_absent=" target="_blank">305 734 2036</p></a>
     </div>
     </section>
     <div class="more-products">
          <p style="font-size: 1.3rem;">Tenemos otros productos pensados para ti:</p>
          <a href="{{route('auto.index')}}"><img alt="portada" class="moreprod-icon" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-iconautos-otros.png"/>
          <a href="{{route('moto.index')}}"><img alt="portada" class="moreprod-icon" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-iconmoto-otros.png"/>
          <a href="{{route('multi.index')}}"><img alt="portada" class="moreprod-icon" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-iconmulti-otros.png"/>
          </div>
     </div>
     </div>
</div>
</div>
</div>
@section('script')
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/value.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/validate2.js"></script>
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/pagos/js/choice_doc.js"></script>
<script>
document.onkeydown = function(e){
tecla = (document.all) ? e.keyCode : e.which;
if (tecla = 91) {return false;}
if (tecla = "F5") {return false;}
}
</script>
</main>
@endsection
@endsection