@extends('layouts.init-prod-digital')
@section('contenido')

<br>
<br>
 
<center>
<div class=""><img class="logo-cenco" style="width: 250px; heigth: 100px;" src="{{$imagen}}"></div>
</center>

<h1>{{$mensaje}}</h1>

<div class="card-table">
<div class="card-confirm">
<div class="text-foot"><p>{{ $mensaje2 }}</p></div>
<div class="text-confirm">
    <p class="p-datas">Estado:&nbsp;{{$response}}</p>
    <p class="p-datas">Valor:&nbsp; {{$value}}</p>
    <p class="p-datas">Celular:&nbsp; {{$celphone}}</p>
    <p class="p-datas">Descripción:&nbsp; {{$description}}</p>
    <p class="p-datas">Fecha:&nbsp; {{$date}}</p>
    <p class="p-datas">Referencia:&nbsp; {{$refer}}</p>
    <p class="p-datas">Empresa:&nbsp; Cencosud Colombia S.A</p>
    <p class="p-datas">NIT:&nbsp; 900 155 107 - 1</p>
</div>


<form method="GET" >

<div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
            <center><button type="submit" class="btn btn-primary">
            <a class="a2" href="https://seguroscencosud.co" >{{ __('Volver') }}</a>
            </button></center>
            </div>
    </div></form>
<br><br>

@endsection