@extends('layouts.init-prod')
@section('contenido')

<h1>TIENES UN PAGO EN ESTADO PENDIENTE</h1>

<div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
        <p>En este momento su pago con referencia {{ $refer }} presenta
        un proceso de pago cuya transacción se encuentra PENDIENTE de recibir
        confirmación por parte de su entidad financiera, por favor espere unos minutos y vuelva
        a consultar más tarde para verificar si su pago fue confirmado de forma exitosa. Si
        desea mayor información sobre el estado actual de su operación puede comunicarse a
        nuestras líneas de atención al cliente 6511102 en Bogota o 3057342036 a nivel nacional. </p>
        <br>
            <center><button type="submit" class="btn btn-primary">
            <a class="a2" href="javascript:history.back()">Volver</a>
            </button></center>
            </div>
    </div>

@endsection