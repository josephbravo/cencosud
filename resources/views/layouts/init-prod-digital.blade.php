<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, width=device-width">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://connect-static-files.s3.amazonaws.com/pagos/js/app.js" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src='https://s3.amazonaws.com/styles.soatdigital/staticfilescompensar/vue/vue_dev.js'></script>
    <script src='js/init.js'></script>
    <script src='https://s3.amazonaws.com/styles.soatdigital/staticfilescompensar/vue/init.js'></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" id="line-awesome-css"  href="//maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css?ver=1.0.4" type="text/css" media="all" />
    <script src="https://kit.fontawesome.com/f93929b876.js" crossorigin="anonymous"></script>
    
    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://connect-static-files.s3.amazonaws.com/pagos/css/app.css" rel="stylesheet">
</head>

<body style="height: 100%; background-size: 100% auto;background-size: cover; background-repeat: no-repeat; width: auto; height:auto;');">
    <div class="app2">
        <nav class="navbar navbar-expand-md navbar-light bg-white2 shadow-sm">
    <div><a href="/home"><img class="logo-cenco-prod" style="width: 250px; heigth: 100px;" src="https://connect-static-files.s3.amazonaws.com/pagos/cencosud-logo-negativo.png"></a></div>
            
        </nav>

        <main class="py-4">
            
        </main>
    </div>

    @yield('titulo')

    <div class="table-prod">
    @yield('contenido')
    </div>

</body>
</html>


