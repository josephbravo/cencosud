@extends('base.init')

@section('content')

<div class="bgsizefam" id="contain">

     <div class="container">

          <div class="row2">
               <div class="col-lg-2">
               </div>

               <div class="col-lg-2">
               </div>

               <div class="col-lg">
                    <div class="img-adu">
                         <img class="familia-titulo" img src="https://connect-static-files.s3.amazonaws.com/pagos/familia_precio_1220.png">
                    </div>
               </div>

               <div class="col-lg">
                    <div class="img-adu">
                         <img class="familia-precio-res" img src="https://connect-static-files.s3.amazonaws.com/pagos/familia_precio_1220-white.png">
                    </div>
               </div>

          <div class="col-lg-1">
          <div  class="table-prod">
               <div class="formulario-familia">

                    <h2><b>ADQUIERE TU SEGURO AQUÍ:</b></h2>

                    <br>
               
                    <div>
                    <center>
                         <button type="submit" class="btn btn-primary">
                         <a href="#openModal" class="a-white">COMPRAR 100% ONLINE</a>
                         </button>
                    </center>
                    </div>&nbsp
                    <div>
                    <center><button type="button" class="btn btn-primary">
                         <a href="#openModalTwo" class="a-white">QUIERO QUE ME LLAMEN</a>
                         </button></center>
                    </div>
                    <br>
                    <div>
                    <center>
                    <p class="p-vig">La póliza inicia vigencia una vez se efectué el pago de la primera cuota y las Asistencias Medicas quedan activas a las 72 horas de iniciado se Seguro.
                    <br>
                    En el transcurso de 8 días hábiles estaremos enviando a tu correo electrónico la póliza y las condiciones particulares incluyendo las exclusiones y condiciones particulares del producto</p>
                    </center>
                    </div>
                    <center>
                    <br>
                    <a href="https://www.placetopay.com/web/home"> <img class="place" img src="https://connect-static-files.s3.amazonaws.com/pagos/Placetopay.png"> </a>
                    </center>
                    <center>
                    <br>
                    <a href="https://pagoscencosud.connect-sos.com/question">Preguntas Frecuentes</a>
                    </center>
                         </div>
          </div>
          </div>
          </div>
     </div>
</div>


<div class="funnel-container">
<div class="funnel-fluid">
<div class="funnel-content-medical">

          <h2 class="h2-container-fml">La mayor alegría es tener una familia, y la mayor satisfacción viene de asegurar que todos están bien.<br>
               Nuestro seguro de Familia Protegida se encarga de proteger a los tuyos para que disfruten del camino sin preocuparse por el mañana.</h2>

          <h2 class="h2-container-up">QUE CUBRE TU SEGURO FAMILIA PROTEGIDA</h2>
          <div class="content-opt">
     <div class="row">
          <div class="col-md-3">
               <div class="opt">
               <img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/icono_asistencia_medica.svg">
               <p class="p-asist-opt">ASISTENCIA MÉDICA<br>(solo asegurado principal)</p>

               <p>Disposición de un médico para que adelante la consulta en su domicilio.</p>
               </div>     
          </div>

          <div class="col-md-3">
                <div class="opt">
               <img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/iconos_domicilio.svg">
               <p class="p-asist-opt">MÉDICO A DOMICILIO<br>en caso de accidente o enfermedad</p>

               <p>8 eventos al año, con copago de $30.000 por evento. Luego de los 8 eventos enviaremos el servicio con un costo preferencial.</p>
               </div> 
          </div>

          <div class="col-md-3">
               <div class="opt">
               <img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/icono_asistencia_traslados.svg">
               <p class="p-asist-opt">TRASLADOS MÉDICOS<br>TERRESTRE</p>

               <p>En caso de accidente y/o riesgo vital.</p>
          </div>
          </div>

          <div class="col-md-3">
               <div class="opt">
               <img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/iconos_clinica.svg">
               <p class="p-asist-opt">VALORACIÓN MÉDICA<br>EN CLÍNICA</p>

               <p>Por accidente y/o riesgo vital.</p>
          </div> 


          </div>
     </div>
</div>

</div>
</div>
</div>


<div class="funnel-container-plans-fml">
<div class="funnel-fluid-plans">
<div class="funnel-content-fml-plans">

<h2 class="h2-container-up-2">PLANES Y TARIFAS</h2>
<h2 class="h2-container">Desde <b>$20.449</b> mensuales</h2>

<div class="content-plans-fml">
<div class="row">
          <div class="col-md-4">
               <div class="opt-fml">
               <p class="p-asist-opt">PLAN A</p><hr class="hr-plan">
               <p>Muerte Accidental</p>
               <p class="value"><b>$10'380.000</b></p><hr class="hr-plan">
               <p>Desmembración por accidente</p>
               <p class="value"><b>$10'380.000</b></p><hr class="hr-plan">
               <p>ITP por accidente</p>
               <p class="value"><b>$10'380.000</b></p><hr class="hr-plan">
               <p>Muerte accidental como pasajero o peatón<br>en automóvil particular</p>
               <p class="value"><b>$15'570.000</b></p><hr class="hr-plan">
               <p>Bono canasta para gastos del hogar por<br>muerte accidental</p>
               <p class="value"><b>$3'114.000</b></p>
               </div>     
          </div>

          <div class="col-md-4">
          <div class="opt-fml">
               <p class="p-asist-opt">PLAN B</p><hr class="hr-plan">
               <p>Muerte Accidental</p>
               <p class="value"><b>$15'570.000</b></p><hr class="hr-plan">
               <p>Desmembración por accidente</p>
               <p class="value"><b>$15'570.000</b></p><hr class="hr-plan">
               <p>ITP por accidente</b></p>
               <p class="value"><b>$15'570.000</b></p><hr class="hr-plan">
               <p>Muerte accidental como pasajero o peatón<br>en automóvil particular</p>
               <p class="value"><b>$20'760.000</b></p><hr class="hr-plan">
               <p>Bono canasta para gastos del hogar por<br>muerte accidental</p>
               <p class="value"><b>$5'190.000</b></p>
               </div> 
          </div>

          <div class="col-md-4">
          <div class="opt-fml">
               <p class="p-asist-opt">PLAN C</p><hr class="hr-plan">
               <p>Muerte Accidental</p>
               <p class="value"><b>$20'760.000</b></p><hr class="hr-plan">
               <p>Desmembración por accidente</p>
               <p class="value"><b>$20'760.000</b></p><hr class="hr-plan">
               <p>ITP por accidente</p>
               <p class="value"><b>$20'760.000</b></p><hr class="hr-plan">
               <p>Muerte accidental como pasajero o peatón<br>en automóvil particular</p>
               <p class="value"><b>$25'950.000</b></p><hr class="hr-plan">
               <p>Bono canasta para gastos del hogar por<br>muerte accidental</p>
               <p class="value"><b>$7'266.000</b></p>
               </div> 
          </div>
     </div>
</div>

<h2 class="h2-container-up-2">NUESTRO SEGURO DE FAMILIA PROTEGIDA</h2>
<h2 class="h2-container">Una vida tan emocionante merece cuidarse.</h2>
     </div>
     </div>
</div>
</div>

<div class="funnel-container-last">
<div class="funnel-fluid">
<div class="funnel-content-medical-last">
     
     <h2 class="h2-container-up-2">ASISTENCIA</h2>
     <h2 class="h2-container">A continuación encontrará las asistencias disponibles para este seguro</h2><br>

     <div class="content-opt">
     <div class="row">
          <div class="col-md-4">
               <div class="opt-h">
               </div>     
          </div>

          <div class="col-md-4">
                <div class="opt">
                <a href="https://api.whatsapp.com/send?phone=573208899730&text=&source=&data=&app_absent="><img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/icono_asistencia_telefonica.svg"></a>
               <p class="p-asist-opt-tel">ORIENTACIÓN MÉDICA TELEFÓNICA</p>
               </div> 
          </div>

          <div class="col-md-4">
               <div class="opt-h">
               </div> 
          </div>
     </div>
</div>


     <h2 class="h2-container-up-2">CON EL RESPALDO DE</h2>

     <img class="liberty" img src="https://connect-static-files.s3.amazonaws.com/pagos/logos_chubb.svg">


<div class="container-boton">
     <div class="row">

          <div class="col-md-2">
               <div class="opt-plan">
               <img class="gallager" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
               </div>     
          </div>

          <div class="col-md-8">
               <div class="opt-plan">
               <p class="cont-last-text">Aplican condiciones y restricciones en caso de tener alguna inquietud comunicarse a línea de atención de Seguros Cencosud.</p>
               </div>     
          </div>

          <div class="col-md-2">
               <div class="opt-plan">
               </div>     
          </div>

     </div>
</div>

<div id="openModal" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
          <br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">

          <div class="formulariotwo">
		<form method="post" action="{{route('familia.store')}}" onsubmit="return Validate()">
                    @csrf
          <h1>ADQUIERE TU SEGURO AQUÍ</h1>

               <div class="btn-group">
               &nbsp&nbsp<div>
                         <input name="nombre" pattern="[A-Za-z ]*" title="Campo inválido" onkeypress="return validar(event)" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre" required>
                         
                    </div>&nbsp
                    <div>
                         
                              <input name="apellido" pattern="[A-Za-z ]*" title="Campo inválido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Apellido" required>
                         </center>
                    </div>
               </div>

               <div class="form-group">     
               </div>

               <div class="btn-group">
               <p class="date-born">En qué fecha naciste?</p>
                    <div>
                    <input name="date" type="date" id="start" class="form-control" name="trip-start" min="1954-01-01" max="2002-12-31">
                    </div>
               </div>

               <div class="form-group">     
               </div>

               <div class="form-group">
                    <select name="tipdocumento" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="">Tipo documento</option>
                         <option value="CC">CC</option>
                         <option value="CE">CE</option>
                         <option value="PPN">PAS</option>
                    </select>
               </div>

               <div class="form-group">
                    <input pattern="[A-Za-z ]*[0-9]{5,12}" title="Campo inválido. Minimo 5 dígitos máximo 12 dígitos" maxlength="12" name="documento" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Número documento" required>
               </div>

               <div class="form-group">
                    <select name="genero" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="">Genero</option>
                         <option value="Masculino">Masculino</option>
                         <option value="Femenino">Femenino</option>
                    </select>    
               </div>

               <div class="form-group">
                    <input pattern="^[0-9]{7,10}$" title="Campo inválido. Minimo 7 dígitos máximo 10 dígitos" maxlength="10" name="celular" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Teléfono" required>
               </div>

               <div class="form-group">
                    <input name="email" type="email" class="form-control" id="formGroupExampleInput2" placeholder="Correo electrónico" required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>

               <div class="form-group">
                    <select name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                         <option value="">Ciudad</option>
                         <option value="1">Bogotá-Cundinamarca</option>
                         <option value="2">Medellín-Antioquia</option>
                         <option value="3">Cali-Valle</option>
                         <option value="4">Cartagena-Bolivar</option>
                    </select>
               </div>

               <div class="form-group">
                    <input name="direccion" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu dirección de residencia" required>
               </div>

               <div class="form-group">
                    <select name="ocupacion" class="form-control" placeholder="Cuál es tu ocupación" required>
                         @foreach ($ocupacions as $ocupacion)
                              <option value="{{ $ocupacion->id }}">{{ $ocupacion->ocupacion }}</option>
                         @endforeach
                    </select>
               </div>

               <div class="form-group">
               <select id="select" name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                    <option value="0">Seleccione</option>
                    <option value="20449">Familia Protegida - A $20.449</option>
                    <option value="22006">Familia Protegida - B $22.006</option>
                    <option value="23563">Familia Protegida - C $23.563</option>
               </select>
                                   
          </div>
               
               <div class="btn-group">
                    
                    <div>
                         <center>
                              <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
                         </center>
                    </div>&nbsp
               <div>
                         <center>
                              <p class="p-terms2"> Al hacer click en el botón "COMPRAR 100% ONLINE" aceptas los <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Autorizaciones+Solicitud+Familia+protegida+Chubb.pdf">terminos, condiciones</a>, , <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Poli%CC%81tica+de+cookies+Cencosud.pdf">politica de cookies</a>  y <a target="_blank" href="https://cencosud.zonic3.com/habeas-data">políticas de tratamiento de datos</a> de Seguros Cencosud </p>
                         <center>
               </div>
               </div>
               <center><div class="btn-group">
               <div>
                         
                              <button type="submit" class="btn btn-primary">
                                   {{ __('Enviar solicitud') }}
                              </button>
                         
                    </div>&nbsp
               </div></center>

               <div class="form-group">
               <img class="gallager-form" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
               </div>
                    
                         </div>
               </form>
               </div>
               </div>
	</div>
</div>

<div id="openModalTwo" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
          <br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">

          <div class="formulariotwo">
		<form method="post" action="{{route('digital.store')}}" onsubmit="return Validate()">
                    @csrf
                    
          <h1>SOLICITA TU ASESORÍA</h1>

          <div class="btn-group">
               &nbsp&nbsp<div>
                         <input name="nombre" onkeypress="return validar(event)" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre" required>
                         
                    </div>&nbsp
                    <div>
                         
                              <input name="apellido" type="text" class="form-control" id="formGroupExampleInput" placeholder="Apellido" required>
                         </center>
                    </div>
          </div>&nbsp

               <div class="form-group">
                    <input oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" name="telefono" type="number" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu número celular" required>
               </div>
               <div class="form-group">
                    <input name="email" type="email" class="form-control" id="formGroupExampleInput2" placeholder="Ingresa tu correo electrónico" required pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+[.com]*$" title="ejemplo@gmail.com">
               </div>
               <div class="form-group">
                    <input oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="10" name="documento" type="number" class="form-control" id="formGroupExampleInput2" placeholder="Número de identificación" required>
               </div>
               <div class="form-group">
               <select id="select" name="valor" class="form-control" placeholder="PRODUCTO - PRECIO" required>
                    <option value="0">Seleccione</option>
                    <option value="15717">Asistencia Medica Domiciliaria - A $15.717</option>
               </select>&nbsp

               <div class="btn-group">
                    
                    <div>
                         <center>
                              <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required>
                         </center>
                    </div>&nbsp
               <div>
                         <center>
                              <p class="p-terms2"> Al hacer click en el botón "Enviar solicitud" aceptas los <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Autorizaciones+Solicitud+Familia+protegida+Chubb.pdf">terminos, condiciones</a>, <a target="_blank" href="https://connect-static-files.s3.amazonaws.com/pagos/documentos/Poli%CC%81tica+de+cookies+Cencosud.pdf">politica de cookies</a> y <a target="_blank" href="https://cencosud.zonic3.com/habeas-data">políticas de tratamiento de datos</a> de Seguros Cencosud </p>
                         <center>
               </div>
               </div>
               <center><div class="btn-group">
               <div>
                         
                              <button type="button" class="btn btn-primary">
                                   <a href="#openModalThree" class="a-white">Enviar solicitud</a>
                              </button>
                         
                    </div>&nbsp
               </div></center>

               <div class="form-group">
               <img class="gallager-form" img src="https://connect-static-files.s3.amazonaws.com/pagos/gallaguer_vigilado.svg">
               </div>
                                   
               </div>

          </form>
     </div>
     </div>
     </div>
     </div>

     <div id="openModalThree" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
          <br>
          <div style="width: 410px; height: 550px; overflow-y: scroll; color:white;">

          <div class="formulariotwo">
                    
          <h1>¡GRACIAS POR TU SOLICITUD!</h1>
          
               &nbsp&nbsp<div>
                         <h2 class="h2-orange">TE ESTAREMOS LLAMANDO EN EL TRANSCURSO DEL DÍA</h2>
                         
                    </div>&nbsp
                    <div>
                         <h2 class="h2-orange">EN EL SIGUIENTE HORARIO</h2>
                         </center>
                    </div>
                    <div class="opt">
                         <h2>Lunes a viernes<br>8:00 a.m a 5:00 p.m</h2>
                    </div>&nbsp
                    <div class="opt">
                         <h2>Sábados<br>8:00 a.m a 12:00 p.m</h2>
                    </div>&nbsp


                         <div class="content-opt">
                         <div class="row">
                         <div class="col-md-1">
                              <div class="opt-h">
                              </div>     
                         </div>

                         <div class="col-md-9">
                              <div class="opt">
                              <center><a href="https://api.whatsapp.com/send?phone=573208899730&text=&source=&data=&app_absent="><img class="asist-opt1" img src="https://connect-static-files.s3.amazonaws.com/pagos/redes_whatsapp.svg">
                              <p class="p-asist-opt-tel">Si quieres asesoría  puedes comunicarte a nuestro WhatsApp</p></a></center>
                              </div> 
                         </div>

                         <div class="col-md-1">
                              <div class="opt-h">
                              </div> 
                         </div>
                         </div>
                         </div>



     </div>
     </div>
     </div>
     </div>

</div>
</div>
</div>


<script type="text/javascript">
     function Validate()
     {
          var e = document.getElementById("select");
          var strUser = e.options[e.selectedIndex].value;

          var strUser1 = e.options[e.selectedIndex].text;
          if(strUser==0)
          {
               alert("Porfavor selecciona un valor");
               return false;
          }else{
               return true;
          }
     }
</script>

@endsection