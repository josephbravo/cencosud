@extends('base.init2')
@section('content')

<section id="section-last-adu"> <!-- funnel-contact -->
     <div class="autos-compra">
          <h1 class="h2-autos-w2">{{ $tittle }}</h1>
     </div>
</section>
 
<center>
<div class=""><img class="logo-cenco" style="width: 200px;" src="{{$imagen}}"></div>
</center>

<h1>{{$mensaje}}</h1>
<div class="card-table">
<div class="card-confirm">
<div class="text-foot"><p>{{ $mensaje2 }}</p></div>
<div class="text-confirm">
    <p class="p-datas">Estado:&nbsp;{{$response}}</p>
    <p class="p-datas">Valor:&nbsp; $ {{$value}}</p>
    <p class="p-datas">Celular:&nbsp; {{$celphone}}</p>
    <p class="p-datas">Descripción:&nbsp; {{$description}}</p>
    <p class="p-datas">Fecha:&nbsp; {{$date}}</p>
    <p class="p-datas">Referencia:&nbsp; {{$refer}}</p>
    <p class="p-datas">Empresa:&nbsp; Cencosud Colombia S.A</p>
    <p class="p-datas">NIT:&nbsp; 900 155 107 - 1</p>
</div>
<div class="text-foot"><p>Para mayor información comunícate a nuestra línea de atención al cliente:<a href="tel:3057342036"> 305 734 2036</a></p></div>
<div class="text-foot"><p>{{ $cert }}</p></div>


@if (session()->has('la'))
<div class="text-foot"><a target="_blank" href="{{ Session::get('link_auto')}}"><img class="product-add-confirm" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-auto-resumen.png"></a></div>
@else


@endif

@if (session()->has('lm'))
<div class="text-foot"><a target="_blank" href="{{ Session::get('link_moto')}}"><img class="product-add-confirm" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-motos-resumen.png"></a></div>
@else

@endif

@if (session()->has('lms'))
<div class="text-foot"><a target="_blank" href="{{ Session::get('link_multi')}}"><img class="product-add-confirm" src="https://connect-static-files.s3.amazonaws.com/pagos/asistencias/img-multi-resumen.png"></a></div>

@else

@endif

<br>
@endsection