<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Subject</title>
  <style type="text/css">
    body {
      margin: 0;
      padding: 0;
 ´     min-width: 100% !important;
    }
    .nopadmar{
      padding:0;
      margin:0;
    }
    img {
      height: auto;
    }

    .content {
      width: 100%;
      max-width: 600px;
    }

    .center {
      text-align: center;
    }

    /*
     * Medidas
     */
    .col230 {
      width: 100%;
      max-width: 290px;
    }

    .col310 {
      width: 100%;
      max-width: 360px;
    }
    ul{
      list-style-type:none;
    }

    .col600 img {
      width: 100%;
      max-width: 650px;
    }

    .blue {
      color: #005CB2;
    }
      /*table-area*/
    .pdBlue {
      border: 1px solid #76AADB;
      border-radius: 10px;
    }
    
    .pdgrayLeft{
      border-left: 7px solid #d7d8dc;
    }
      .pdgrayRight{
      border-right: 7px solid #d7d8dc;
    }
      .pdgrayTop{
      border-top: 7px solid #d7d8dc;
    }
      .pdgrayBottom{
      border-bottom: 7px solid #d7d8dc;
    }
    .pdGrayL{
      border-left: 7px solid #d7d8dc;
      border-top:7px solid #d7d8dc;
      border-radius: 15px;
    }
    .pdGrayR{
      border-right: 7px solid #d7d8dc;
      border-bottom: 7px solid #d7d8dc;
     
    }
    table.qtable,table.qtable th, table.qtable td {
      margin: 0;
      padding: 0;
      font-size: 100%;
      font: inherit;
      border-spacing:0;
    }

    .header {
      padding: 0px;
    }

    .headAlt {
      height: 70px;
    }

    .logo {
      text-align: left;
    }

    .logo img {
      width: 230px;
    }

    .bg-image {
      width: 100%;
    }

    .subhead {
      font-family: 'Open Sans', sans-serif;
      text-align: right;
    }

    .innerpadding {
      padding: 10px 20px 20px 20px;
    }

    .h1, .h2, .bodycopy, .info, .blue, .bodycopyInc {
      font-family: 'Open Sans', sans-serif;
    }
    
    .h1 {
      font-size: 24pt;
      text-align: left;
      font-weight: 500;
    }

    .h2 {
      font-size: 13.7pt;
      line-height: 28px;
      font-weight: bold;
      text-align: center;
      margin: 0 0 10px 0;
    }

    .h3 {
      font-size: 18pt;
      line-height: 28px;
      font-weight: bold;
      text-align: center;
      margin: 0 0 10px 0;
    }

    .info {
      color: #5c5b5f;
      font-size: 15px;
      padding-top: 10px;
      font-weight: 100;
      line-height: 26px;
      text-align: justify;
    }

    .bodycopy {
      font-size: 16px;
      line-height: 22px;
    }

    .bodycopyInc {
      font-size: 16px;
      line-height: 22px;
      color: #87868A;
    }
    .f16{
      font-size:16px;
    }
    .button {
      text-align: center;
      font-size: 18px;
      font-family: 'Open Sans', sans-serif;
      font-weight: bold;
      padding: 0 30px 0 30px;
    }

    .button a {
      color: #ffffff;
      text-decoration: none;
    }

    .btn-call {
      text-align: center;
    }

    .btn-call a {
      font-family: 'Open Sans', sans-serif;
      background: #42B715;
      border-radius: 5px;
      color: white;
      font-size: 14pt;
      padding: 10px 10px 10px 20px;
      text-decoration: none;
    }

    .icon-i {
      display: inline-block;
      vertical-align: middle;
    }

    .footer {
      padding: 20px 30px 15px 30px;
    }
    .info p{
      font-size:14px;
    }

    .footercopy {
      font-family: 'Open Sans', sans-serif;
      font-size: 13px;
      color: #87868A;
      padding-top: 10px;
      line-height: 18px;
    }

    .footercopy a {
      color: #000;
      text-decoration: underline;
    }

    .img {
      width: 120px !important;
      vertical-align: middle;
    }
    
    /*Character misc*/
    .fgray{
      color:#87868A;
    }
    .f100{
      font-weight:100;
    }
    .f400{
      font-weight:400;
    }
    .f600{
      font-weight:600;
    }
    .fbold{
      font-weight:bold;
    }
    .center {
      text-align: center;
    }
    .sans-serif{
      font-family: 'Open Sans', sans-serif;
    }
    .tjustify{
      text-align:justify;
    }
   
   {% snippet "header_styles" %}

    @media only screen and (max-width: 501px), screen and (max-device-width: 501px) {
      body[yahoo] .button {
        padding: 0px !important;
      }

      body[yahoo] .button a {
        background-color: #e05443;
        padding: 15px 15px 13px !important;
      }

      body[yahoo] .headAlt:last-child {
        padding-bottom: 10px;
        height: 0px !important;
      }

      body[yahoo] .subhead {
        text-align: center !important;
      }

      body[yahoo] .logo {
        text-align: center !important;
      }

      body[yahoo] .h1 {
        font-size: 18pt !important
      }

      body[yahoo] .info {
        font-size: 12pt !important
      }

      /*
       * Medidas
       */
      body[yahoo] .col230 {
        max-width: 100% !important;
      }

      body[yahoo] .col310 {
        max-width: 100% !important;
      }
    }
  </style>
  <style>
    /*Table area*/
    .bdblue{
       border: 2px solid #76a9da;
    }
    .ctab{
      border-spacing: 5px;
      padding:0;
      margin:0;
      display:inline-block;
      width:275px;
    }


  </style>
</head>

<body yahoo>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <table bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td class="header" style="padding: 30px 0 30px;" align="center">
                <img 
                  src="https://connect-static-files.s3.amazonaws.com/mail/HeaderMailing.png" 
                  alt="" 
                  style="width: auto;height: auto !important;max-width: 100% !important;"
                />
            </td>
          </tr>
        <tr>
        </tr>
        <tr>
          <td class="innerpadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="h1 blue" style="font-family: Abadi MT Condensed, Sans-serif; font-size: 30pt;">
                  <center><b>Hola</b></center>
                </td>
              </tr>
              <tr>
                <td class="center">
                  
                  <p style="font-family: Abadi MT Condensed, Sans-serif; font-size: 20pt; color: #5c5b5f;">John Sebastian Retavisca Murcia</p>
                  <p style="font-family: Abadi MT Condensed, Sans-serif;" class="">
                    Para conocer y administrar las pólizas de tus productos ingresa a <a href="https://pagoscencosud.connect-sos.com">https://pagoscencosud.connect-sos.com</a> con el siguiente usuario y contraseña.
                    <br>
                    <center>
                    
                    <p style="font-family: Abadi MT Condensed, Sans-serif; font-size: 15pt; color: #5c5b5f;">USUARIO: john.retavisca@connect-sos.com</p>
                    <p style="font-family: Abadi MT Condensed, Sans-serif; font-size: 15pt; color: #5c5b5f;">CONTRASEÑA: Connect2020*</p>
                </td>
              </tr>
              <tr>
                <td class="h1 blue" style="font-family: Abadi MT Condensed, Sans-serif; font-size: 16pt;">
                  <center>Recuerda que</center>
                  <p style="font-family: Abadi MT Condensed, Sans-serif; font-size: 8pt; color: #C5C5C5;">El usuario y la contraseña son personales e instranferibles. La informacion de nuestros afiliados es es confidencual y goza del privilegio y derecho a la intimidad en el Articulo 15 de la Constitucion Politica de Colombia de 1991. Este mensaje y sus anexos, enviado mediante correo electronico, contiene informacion confidencial. Si usted no es el destinatario autorizado, y recibio este mensaje por error, abdtengase de revisarlo, copiarlo, utilizarlo o revelarlo de cualquier forma. Por facor informe al remitente y posteriormente borrelo de su sistema sin conserver copia del mismo. La utilizacion o difusion no autorizada de este mensaje esta prohibida por la ley. Por favor consulte nuestra Politica de Tratamiento de Datos Personales en: <a href="https://seguroscencosud.co/">https://seguroscencosud.co/</a></p>
                </td>
              </tr>
              <tr>
            <td class="header" style="padding: 30px 0 30px;" align="center">
                <img 
                  src="https://connect-static-files.s3.amazonaws.com/mail/footer.png" 
                  alt="" 
                  style="width: auto;height: auto !important;max-width: 100% !important;"
                />
            </td>
          </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<footer>

</footer>
</body>
</html>