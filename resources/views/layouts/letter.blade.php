<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src='https://s3.amazonaws.com/styles.soatdigital/staticfilescompensar/vue/vue_dev.js'></script>
    <script src='js/init.js'></script>
    <script src='https://s3.amazonaws.com/styles.soatdigital/staticfilescompensar/vue/init.js'></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" id="line-awesome-css"  href="//maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css?ver=1.0.4" type="text/css" media="all" />
    <script src="https://kit.fontawesome.com/f93929b876.js" crossorigin="anonymous"></script>
    


    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

     <title>Letter</title>
</head>
<body>

<div class="funnel-container">
     <div class="prince">

          <div class="prince-item">

               <img style="heigth: 300px; width: 750px; padding-top: 1px;" src="https://connect-static-files.s3.amazonaws.com/pagos/headerCartaBienvenida.jpg">

               <br>
               <p>Bogotá D. C., 13 de Marzo de 2019</p><br/>
               Señor(a):</p>
               WILLIAM DIAZ PENALOZA</p>
               VILLA ROSARIO-NTE SANTANDER</p><br/>
               Estimado Cliente:</p><br/>
               <p class="text-content-blue">Es grato para Nosotros darte la bienvenida al <span class="text-content-orange">Programa de Seguros Cencosud,</span> intermediado por Arthur J. Gallagher, con el fin de brindarte protección y tranquilidad en todo momento.</p><br/>
               Adjunto encontrarás la póliza de Producto Cert. 994000072525 respaldada por la compañía Sura de Colombia así como el clausulado en el cual se detallan los términos, condiciones y exclusiones por los cuales se rige tu póliza, documentos que te recordamos revisar y conservar en un lugar seguro.</p><br/>
               La cobertura de tus pólizas depende del pago mensual y oportuno de las primas, por lo tanto, queremos recordarte que, si por alguna circunstancia no es posible hacer el cobro correspondiente según el acuerdo de pago establecido, tu seguro será cancelado automáticamente; por favor revisa los cobros en sus extractos bancarios y ante cualquier anomalía que identifiques, dudas o explicación que requieras comunícate con nosotros.</p><br>
               Cualquier inquietud que tengas o modificación que desees hacer a tus pólizas, por favor comunícala a nuestro departamento de Servicio al Cliente:</p><br/>
               </p>
               <div class="text-foot">
               <p class="text-orange"><i class="fas fa-headset"></i> Línea de atención a nivel nacional: 320-8899730</p>

               <p class="text-orange"><i class="fas fa-envelope-square"></i> E-Mail: seguroscencosud@connect-sos.com</p><br>

               </div>

          <footer class='footer-letter'>
               <div class="text-foot2">
               <img class="logo-cenco2" src="https://connect-static-files.s3.amazonaws.com/pagos/cencosud-logo.png">
                    <p class="text-content-blue">Cordialmente,</p><br/>
                    <p class="text-content-blue">Servicio al cliente</p>
                    <p class="text-content-blue">Programa de Seguros Cencosud.</p><br/>
                    
                    Conoce nuestro portafolio de productos en los stands de tiendas Jumbo y Metro a nivel nacional, 38 puntos autorizados*. El programa de Seguros Cencosud es un programa intermediadio por Arthur J. Gallager. Para cualquier solicitud con tu producto de seguros llama a la lína de atención a nivel nacional: 320 889 9550</p>
          </footer>

          <footer class='footer-letter2'>
               <div class="text-foot2">
               <div class="last-foot">
                    <p class="text-social"><i class="fab fa-facebook-square"></i> Programa de seguros de cencosud colombia<span class="text-social2"><i class="fab fa-instagram"></i> programaseguroscencosudcol</span></p>
          
               </div>

          </footer>
          </div>
          </div>

     </div>
</div>
     
</body>
</html>

