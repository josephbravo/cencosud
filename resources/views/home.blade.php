@extends('layouts.init-prod')

@section('titulo')

<br>
<div class="title-prod"><h1>BIENVENIDO A TUS PRODUCTOS SEGUROS CENCOSUD</h1></div>
<div class="title-prod"><h2>Selecciona el producto que deseas consultar</h2></div>
<br>
<br>

 @endsection

@section('contenido')
<form method="post" action="{{route('create.store')}}">
@csrf
    <div class="table-prod">
    <table class="table table-striped" style="border-radius: 1em; overflow: hidden; border-collapse: collapse;">
        <thead class="thead-prod">
            <tr>
            <th scope="col">Producto</th>
            <th scope="col">No Póliza</th>
            <th scope="col">Identificación</th>
            <th scope="col">Nombre</th>
            <th scope="col">Estado</th>
            <th scope="col">Más información</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($clientess as $cliente)
            <tr>
            <th scope="row">{{ $cliente->BRAMO }}</th>
            <td>{{ $cliente->NROPOL }}</td>
            <td>{{ $cliente->NITCLIENTE }}</td>
            <td>{{ $cliente->NOMBRE }}</td>
            <td>{{ $cliente->ESTADOSICS }}</td>
            <td>
            <input type="hidden" name="documentoHidden" value="{{ $cliente->NITCLIENTE }}">
                <button name="poliza" value="{{ $cliente->NROPOL }}" type="submit" class="btn btn-link" {{$cliente->ver}}>
                    <a class="a2">Mas Detalles</a>
                </button>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
        </div>
        <br>
        <br>
        <div class="date-prod"><p>Fecha de ingreso: <?php
            echo date("d-m-Y");
            ?>
            </p></div>
           
</form>
</body>
</html>


@endsection