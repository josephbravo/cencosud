<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Tag Manager -->
    <script src="https://connect-static-files.s3.amazonaws.com/pagos/js/tag_manager.js" defer></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Pagos Cencosud</title>

    <!-- Scripts -->
    <script src="https://connect-static-files.s3.amazonaws.com/pagos/js/app.js" defer></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script> -->
    
    <!-- favicon -->
    <link rel="icon" type="image/png" href="https://www.seguroscencosud.co/favicon.ico">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <!--<link href="https://connect-static-files.s3.amazonaws.com/pagos/css/app.css" rel="stylesheet"> --> 
    <!--<link href="https://connect-static-files.s3.amazonaws.com/pagos/css/appv2.css" rel="stylesheet">--> 
    <link href="https://pagoscencosud.connect-sos.com/css/app.css" rel="stylesheet">
    <!-- Slide.css <link rel="stylesheet" href="https://connect-static-files.s3.amazonaws.com/pagos/css/flexslider.css" type="text/css">-->
    
</head>
<body>
    <!-- Tag Manager -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NGPPGXZ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<header>
<div class="header-prod">
    <div class="cenco-header">
    <div class="row">
      <div class="col-md-6">
      <a href="https://seguroscencosud.co/"><img alt="portada" class="header-cenco" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/imagenes/Logo-Cencosud-Gallager.svg"/></a>
      </div>
        <div class="col-md-1">
        <div class="text-header">
            <center><a href="tel:3057342036"><img alt="portada" class="icon-header-atent" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_atencion_cliente.svg"/></a></center>
        </div>
        </div> 

        <div class="col-md-2">
            <div class="text-aten">
                <p class="attention">Atención al Cliente:</p>
                <p class="cel">305 734 2036</p>
            </div>   
        </div> 
         
        <div class="col-md-3">
        <a href="https://www.facebook.com/ProgramaSegurosCencosudColombia/"><img alt="portada" class="icon-header-facebook" src="https://connect-static-files.s3.amazonaws.com/pagos/redes_facebook.svg"/></a>
        <a href="https://www.instagram.com/seguroscencosudcol/?hl=es-la"><img alt="portada" class="icon-header-instagram" src="https://connect-static-files.s3.amazonaws.com/pagos/redes_instagram.svg"/></a>
        <a href="https://api.whatsapp.com/send?phone=573057342036&text=&source=&data=&app_absent="><img alt="portada" class="icon-header-what" src="https://connect-static-files.s3.amazonaws.com/pagos/redes_whatsapp.svg"/></a>
        </div> 
    </div>
    </div>
    </div>
</div>
</header>

@yield('content')

<hr>
<footer class="footer">

     <article class="cenco-foot-img">
     <!-- <center><img class="cenco-foot-last" src="https://seguroscencosud.co/wp-content/uploads/2020/08/LogoFooterSeguros.svg"></img></center> -->
     <center><img class="cenco-foot-last" src="https://connect-static-files.s3.amazonaws.com/pagos/img-logoheader.png"></img></center>
    </article>

    <article>   
    <div class="text-aten-foot">
    <a href="tel:3057342036"><img alt="portada" class="icon-header-foot" src="https://connect-static-files.s3.amazonaws.com/pagos/icono_atencion_cliente.svg"/></a>
        <p class="p-foot">Atención al Cliente:</p>
        <p class="cel2">305 734 2036</p>
    </div>
    </article>

    <center><div class="img-aten-foot">
        <a href="https://www.facebook.com/ProgramaSegurosCencosudColombia/"><img alt="portada" class="icon-header-facebook-foot" src="https://connect-static-files.s3.amazonaws.com/pagos/redes_facebook.svg"/></a>
        <a href="https://www.instagram.com/seguroscencosudcol/?hl=es-la"><img alt="portada" class="icon-header-instagram-foot" src="https://connect-static-files.s3.amazonaws.com/pagos/redes_instagram.svg"/></a>
        <a href="https://api.whatsapp.com/send?phone=573057342036&text=&source=&data=&app_absent="><img alt="portada" class="icon-header-what-foot" src="https://connect-static-files.s3.amazonaws.com/pagos/redes_whatsapp.svg"/></a>
    </div></center>

    <div class="text-foot-inform">
        <p class="text-last">Este es un resumen informativo. Consulta las coberturas, asistencias y condiciones completas en los términos y
        condiciones de este producto. En la página web de la aseguradora encontrarás información relevante en materia de
        protección al consumidor financiero. La vigencia de este producto es mensual con renovación automática.</p>

    <a href="https://seguroscencosud.co/"><p class="www-cenco">www.seguroscencosud.co</p></a>
    <p class="www-cenco2">Todos los derechos reservados Seguros Cencosud 2020</p>
    </div>     
    </div>
    </div>
</footer>

</body>
</html>