<?php

use Illuminate\Support\Facades\Route;
use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
}); 

Route::get('/index', function () {
    return view('auth/index');
});   

Route::get('/create', function() {
    return view('layouts/create');
});

Route::get('/conexion', function() {
    try{
        DB::connection()->getPdo();
    } catch(\Exception $e){
        die("NO HAY CONEXION CON BASE DE DATOS".$e);
    }
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/mail', function() {
    return view('layouts/mail');
});

Route::get('/envio', function() {

    $users = App\User::all();

    foreach ($users as $user) {
        Mail::send('layouts/mail', ['user'=>$user], function($message) use ($user) {
            $message->from('seguroscencosud@connect-sos.com', 'Programa de seguros cencosud');
            $message->to($user->email,$user->name);
            $message->subject('Bienvenido al Programa de seguros cencosud');
        });
    }
    return "Se ha enviado el correo";
});

Route::get('/create', 'CreateController@index');
Route::post('/create', 'CreateController@store')
    ->name('create.store');

Route::post('/peticionpay', 'PeticionController@index')->name('peticion.index');
Route::post('/peticion', 'PeticionController@store')
    ->name('peticion.store');

Route::get('/digital', 'DigitalController@index');
Route::post('/digital', 'DigitalController@store')->name('digital.store');
Route::post('/get_refer', 'DigitalController@get_refer')->name('digital.test');
Route::post('/ajax', 'DigitalController@ajax')->name('digital.ajax');
Route::post('/cos_andi', 'DigitalController@cos_andi')->name('digital.cos_andi');
Route::post('/cos_andi_home', 'DigitalController@cos_andi_home')->name('digital.cos_andi_home');

Route::get('/confirm', 'ConfirmController@index');
Route::post('/confirm', 'ConfirmController@store')->name('confirm.store');

Route::get('/confirmtwo', 'ConfirmtwoController@index');
Route::post('/confirmtwo', 'ConfirmtwoController@store')->name('confirm.store');

Route::get('/notification', 'NotificationController@index');
Route::post('/notification', 'NotificationController@index')
    ->name('notification');

Route::get('/notificationtwo', 'NotificationtwoController@index');
Route::post('/notificationtwo', 'NotificationtwoController@index')
    ->name('notificationtwo');

Route::get('/terms', 'TermsController@index');
Route::post('/terms.store', 'TermsController@store')
    ->name('terms.store');

Route::get('/question', 'QuestionController@index');
Route::post('/question.store', 'QuestionController@store')
    ->name('question.store');

Route::get('/questiontwo', 'QuestiontwoController@index');
Route::post('/questiontwo.store', 'QuestiontwoController@store')
    ->name('questiontwo.store');

Route::get('/adu', 'AduController@index');
Route::post('/adu', 'AduController@store')->name('adu.store');

Route::get('/mascota', 'MascotaController@index');
Route::post('/mascota', 'MascotaController@store')->name('mascota.store');
Route::post('/mascota#openModalThree', 'MascotaController@show')->name('mascota.show');

//Nuevas

Route::get('/asistencia', 'AsistenciaController@index')->name('asistencia.index');
Route::post('/asistencia', 'AsistenciaController@store')->name('asistencia.store');
Route::post('/asistencia#openModalThree', 'AsistenciaController@show')->name('asistencia.show');

Route::get('/moto', 'MotoController@index')->name('moto.index');
Route::post('/moto', 'MotoController@store')->name('moto.store');
Route::post('/moto#openModalThree', 'MotoController@show')->name('moto.show');

Route::get('/auto', 'AutoController@index')->name('auto.index');
Route::post('/auto_pay', 'AutoController@store')->name('auto.store');
//Route::post('/auto#openModalThree', 'AutoController@show')->name('auto.show');

Route::get('/multiasistencia', 'MultiasistenciaController@index')->name('multi.index');
Route::post('/multiasistencia', 'MultiasistenciaController@store')->name('multi.store');
Route::post('/multiasistencia#openModalThree', 'MultiasistenciaController@show')->name('multiasistencia.show');

Route::get('/confirmasist', 'CartController@confirm');

//Nueva Shopping Cart

Route::post('/cart-add', 'CartController@add')->name('cart.add');

//Route::get('/cart', 'CartController@index')->name('cart.index');

Route::post('/cart', 'CartController@check')->name('cart.check');
Route::get('/cart', 'CartController@checking')->name('cart.checking');
Route::get('/cart_check', 'CartController@cart_check')->name('cart.cart_check');
Route::get('/cart_in', 'CartController@check_multi')->name('cart.check_multi');

//Route::get('/cart-checkout', 'CartController@index')->name('cart.checkout');
Route::get('/cart-clear', 'CartController@clear')->name('cart.clear');

Route::post('/cart-remove', 'CartController@remove')->name('cart.remove');

Route::post('/data_pay', 'CartController@data_pay')->name('cart.data_pay');
Route::post('/data_pay', 'CartController@data_pay')->name('cart.data_pay');

//fin nuevas

Route::get('/familia', 'FamiliaController@index');
Route::post('/familia', 'FamiliaController@store')
    ->name('familia.store');

Route::get('/control_pay', 'Control_PayController@index');
Route::post('/control_pay', 'Control_PayController@store')
    ->name('control_pay.store');    

Route::get('/test', 'TestController@index');
Route::post('/test', 'TestController@store')->name('test.store');
Route::get('/mascota_resp', 'TestController@create');
Route::get('/adu_resp', 'TestController@store');
Route::get('/familia_resp', 'TestController@show');

Route::get('/products', function() {
    return view('layouts/products');
});

Route::get('/letter', function() {
    return view('layouts/letter');
});

Route::get('/web_service', function() {
    return view('layouts/web_service');
});

/*
Route::get('/', 'WelcomeController@index');
Route::resource('images', 'WelcomeController', ['only' => ['store', 'destroy']]);
*/

Route::get('/service','TaskController@service');

Route::get('enviar', function() { 

    $email = "jorbravoa@gmail.com";
    $nombre = "Prueba";
    $link = "https://sendgrid.com/docs/ui/sending-email/how-to-send-an-email-with-dynamic-transactional-templates/";

    $send = '
    {"personalizations": [
        {
            "to": [
                {
                    "email": "'.$email.'"
                }]
        }],
        "from": {
            "email": "seguroscencosud@connect-sos.com"
        },
        "subject": "Gracias por tu compra!",
        "content": [
            {
                "type": "text/plain",
                "value": "Gracias por el pago de tu seguro. A continuación puedes descargar tu poliza: '.$link.'"
            }]
    }
    ';

    $send2 = '{
        "from":{
           "email":"seguroscencosud@connect-sos.com",
        },
        "personalizations":[
           {
              "to":[{
                    "email":"'.$email.'"
                 }],
              "dynamic_template_data":{
     
                 "name":"'.$nombre.'",
                 "producto":"AUTO",
                 "link":"'.$link.'",
               }}],
        "template_id":"d-bac3948551b74e188e147681d12e8979"
     }';

    $client_grid = new Client([
        'body'=>$send,
        'verify' => false,
        'Connection: keep-alive',
    ]);
    $headers = [
        'Authorization' => 'Bearer '. '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',        
        'Content-type'  => 'application/json',
    ];

    $response = $client_grid->request('POST', 'https://api.sendgrid.com/v3/mail/send', ['headers' => $headers,]);
    $resp = json_decode($response->getBody()->getContents(), true);
    

    return "Se ha enviado email:";

});