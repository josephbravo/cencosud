<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'https://test.placetopay.com',
        'https://dnetix.co/p2p/client',
        'https://test.placetopay.com/redirection/api/session/',
        'https://secure.placetopay.com/redirection/api/session/',
        'https://pagoscencosud.connect-sos.com/notification',
        'https://pagoscencosud.connect-sos.com/notificationtwo'
    ];
}
