<?php

namespace App\Http\Controllers;

use App\ModelsTest;
use Illuminate\Http\Request;

use Carbon\Carbon;
use App\ModelsAdu;
use App\Models\Peticion;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requestId = '308515';
        $documento = '1017217135';

        /**$peticions = Peticion::where('user_id','1')->get('nombres')->last();
        
        $peticions2 = Peticion::where('nombres','Juan')->update(['nombres' => "Joseph B"]);

        $peticions = Peticion::where('requestId',$requestId)->get('nombres')->last(); */

        $status_pay = Peticion::where('documento',$documento)->get('estado_transaccion')->last();
        $status= json_encode($status_pay["estado_transaccion"], true);
        $status_pay = str_replace('"',"", $status);

        if ($status_pay == "PENDING_PAY") {
            return view('layouts/control_pay');
        }
        

        $cambio = json_encode($peticions["nombres"], true);


        $peticions["nombres"] = "Joseph";
        $peticions->save();

        $requestId = str_replace('"',"", $cambio);

        $requestId = "Joseph";

        $json = $status_pay;
        $json2 = "segundo";

        
        return view('layouts/test', compact('json', 'json2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        $date = Carbon::now();
        $max = $date->subYears(18)->format('Y-m-d'); // pasar tambien en ADU y Familia
        $min = $date->subYears(47)->format('Y-m-d'); // pasar tambien Familia
        $ocupacions = DB::table('ocupacions')->where('Estado','A')->get();
        $mensaje="";
        $url = "";

        return view('layouts/mascota_resp', compact('url','mensaje','max','min','ocupacions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = Carbon::now();
        $max = $date->subYears(18)->format('Y-m-d'); // pasar tambien en ADU y Familia
        $min = $date->subYears(47)->format('Y-m-d'); // pasar tambien Familia
        $ocupacions = DB::table('ocupacions')->where('Estado','A')->get();
        $mensaje="";
        $url = "";
  
        //return view('layouts/adu', compact('url','mensaje','max','min','ocupacions'));
        return view('layouts/adu_resp', compact('url','mensaje','max','min','ocupacions'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsTest  $modelsTest
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsTest $modelsTest)
    {
        $date = Carbon::now();
        $max = $date->subYears(18)->format('Y-m-d'); // pasar tambien en ADU y Familia
        $min = $date->subYears(47)->format('Y-m-d'); // pasar tambien Familia
        $ocupacions = DB::table('ocupacions')->where('Estado','A')->get();
        $mensaje="";
        $url = "";

        return view('layouts/familia_resp', compact('url','mensaje','max','min','ocupacions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsTest  $modelsTest
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsTest $modelsTest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsTest  $modelsTest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsTest $modelsTest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsTest  $modelsTest
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsTest $modelsTest)
    {
        //
    }
}
