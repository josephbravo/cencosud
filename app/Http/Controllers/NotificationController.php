<?php

namespace App\Http\Controllers;

use App\Models\Peticion;
use App\ModelsNotification;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)

    {
        /**if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        } */

        $bodyContent = $request->all();

        $status = $bodyContent['status']['status'];

        $requestId = $bodyContent["requestId"];
        $reference = $bodyContent["reference"];

        $peticions = Peticion::where('referencia', $reference)
                                ->update(['estado_transaccion' => $status]);   //=> $status_resp

        $peticions = Peticion::where('user_id',auth()->id())->where('referencia', $reference)
                                ->get('requestID')->last();
        
        $cambio = json_encode($peticions["requestID"], true);
        $requestId = str_replace('"',"", $cambio);

        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
          } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
          } else {
            $nonce = mt_rand();
          }
          $nonceBase64 = base64_encode($nonce);

        $seed = date('c');
        $secretKey = 'aIMRMh8tpKiU5UGN'; // trankey  
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $auth = '
        {
            "auth": {
                "login": "825e1b4e038b3c380aefc33181bed268",
                "tranKey": "'.$tranKey.'",
                "nonce": "'.$nonceBase64.'",
                "seed": "'.$seed.'"
            }
        }
        ';

        $req = json_decode($auth,true);

        $req["auth"]["tranKey"] = $tranKey;
        $req["auth"]["nonce"] = $nonceBase64;
        $req["auth"]["seed"] = $seed;

        $client = new Client([
            // You can set any number of default request options.
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req,
            'timeout' => 600,
        ]);
        
        $response2 = $client->request('post', 'https://secure.placetopay.com/redirection/api/session/'.$requestId, [ 'body' => json_encode($req) ]);

        $body2 = json_decode($response2->getBody()->getContents(), true);
        $cambior = json_encode($body2['status']['status'], true);

        return view('layouts/notification');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsNotification  $modelsNotification
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsNotification $modelsNotification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsNotification  $modelsNotification
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsNotification $modelsNotification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsNotification  $modelsNotification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsNotification $modelsNotification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsNotification  $modelsNotification
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsNotification $modelsNotification)
    {
        //
    }
}
