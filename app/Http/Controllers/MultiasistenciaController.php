<?php

namespace App\Http\Controllers;

use App\ModelsMultiasistencia;
use App\ModelsAuto;
use Illuminate\Http\Request;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Models\Products;
use App\Models\Ciudades;
use Cart;

class MultiasistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date = Carbon::now();
        $max = $date->subYears(18)->format('Y-m-d'); // pasar tambien en ADU y Familia
        $min = $date->subYears(47)->format('Y-m-d'); // pasar tambien Familia
        $ocupacions = DB::table('ocupacions')->where('Estado','A')->get();
        $mensaje="";
        $url = "";
        $items = Cart::getContent();
        foreach(Cart::getContent() as $row) {
            if ($row->name == "MULTIASISTENCIAS"){
                session()->forget('data');
            }else {
                $url = "SI TIENE";
                session(['data' => $url]);
            }}

        $productos = Products::all();
        $ciudades = Ciudades::all();
        
        return view('layouts/multiasistencia', compact('url','mensaje','max','min','ocupacions', 'productos','items', 'ciudades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = "MULTI";
        $date = Carbon::now();
        $tomorrow = $date->add(1, 'day');
        $minutes = $date->subMinute(50);

        $nombre = $request->input('nombre');
        $apellido = $request->input('apellido');
        $email = $request->input('email');
        $documento = $request->input('documento');
        $celular = $request->input('celular');

        /**$date = Carbon::now();
        $tomorrow = $date->add(1, 'day');
        $minutes = $date->subMinute(50);
        $tipdocumento = $request->input('tipdocumento');
        $direccion = $request->input('direccion');
        $ciudad = $request->input('ciudad');
        $placa = $request->input('placa');
        $marca = $request->input('marca');
        $color = $request->input('color');
        $xx = $request->input('xx');
        session(['documentsession' => $documento]);
        session(['product' => "AUTO"]);

        $status_pay = Autotransactions::where('documento',$documento)->get('referencia')->last();
        $status= json_encode($status_pay["referencia"], true);
        $status_pay = str_replace('"',"", $status);
        
          
        if ($status_pay == "PENDING_PAY") {
            return view('layouts/control_pay');
        }

        //dd($nombre = $nombre);

        */
        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
            } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
            } else {
            $nonce = mt_rand();
            }
            $nonceBase64 = base64_encode($nonce);

        $seed = date('c');
        $secretKey = 'aIMRMh8tpKiU5UGN';
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $req["auth"]["seed"] = $seed;
        $req["auth"]["nonce"] = $nonceBase64;
        $req["auth"]["tranKey"] = $tranKey;
        $req["expiration"] = $tomorrow;

        //total remplazar por valor real
        $json = '{
            "buyer": {
              "name": "'.$nombre.'",
              "surname": "'.$apellido.'",
              "email": "'.$email.'",
              "document": "'.$documento.'",
              "documentType": "CC",
              "mobile": "'.$celular.'"
            },
            "payment": {
              "reference": "'.$product.$documento.$date->format('dmYHis').'",
              "description": "Pago a producto seguros Cencosud",
              "amount": {
                "currency": "COP",
                "total": "1.000"
              }
            },
            "expiration": "'.$minutes.'",
            "ipAddress": "201.184.3.213",
            "returnUrl": "https://pagoscencosud.connect-sos.com/confirmtwo",
            "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36",
            "paymentMethod": "",
            "skipResult": "True",
            
            "auth": {
              "login": "825e1b4e038b3c380aefc33181bed268",
              "tranKey": "'.$tranKey.'",
              "nonce": "'.$nonceBase64.'",
              "seed": "'.$seed.'"
             }
            }';

        $req = json_decode($json,true);
        //dd($req);

        $client = new Client([
            // You can set any number of default request options.
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req,
            
            'timeout' => 600,
        ]);
        
        
        $response = $client->request('post', 'https://secure.placetopay.com/redirection/api/session/', [ 'body' => json_encode($req) ]);

        $response = json_decode($response->getBody()->getContents(), true);

        $response_encode = json_encode($response["processUrl"], true);
        $requestId = json_encode($response["requestId"], true);
        $cambio1 = str_replace("\/\/","//", $response_encode);
        $cambio2 = str_replace("\/","/", $cambio1);
        $cambio3 = str_replace('"',"", $cambio2);

        $url = $cambio3;

        return redirect()->away($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsMultiasistencia  $modelsMultiasistencia
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsMultiasistencia $modelsMultiasistencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsMultiasistencia  $modelsMultiasistencia
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsMultiasistencia $modelsMultiasistencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsMultiasistencia  $modelsMultiasistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsMultiasistencia $modelsMultiasistencia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsMultiasistencia  $modelsMultiasistencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsMultiasistencia $modelsMultiasistencia)
    {
        //
    }
}
