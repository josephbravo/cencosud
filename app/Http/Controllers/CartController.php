<?php

namespace App\Http\Controllers;

use App\ModelsCart;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Cart;
use GuzzleHttp\Client;
use App\Models\Products;
use App\Http\Controllers\AutoController;
use App\Models\Payments;
use App\Models\Auto;
use App\Models\Moto;
use App\Models\Multiasistencia;
use Session;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $date = Carbon::now();
        $url = "";

        $product = Products::find($request->id_producto);
        Cart::add(
            $product->id,
            $product->producto,
            $product->precio,
            $product->meses,
            $product->total,
        );

        return redirect('layouts/auto', compact('url'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * Add product to Car Shoping
     */
    public function check(Request $request)
    {
        $date = Carbon::now();
        
        if ($request->input('producto') == "AUTO"){
            $auto_database = new Auto;
            $auto_database->fecha=$date->format('d-m-Y H:i:s');
            $auto_database->nombre=$request->input('nombre');
            $auto_database->apellido=$request->input('apellido');
            $auto_database->tipdocumento=$request->input('tipdocumento');
            $auto_database->documento=$request->input('documento');
            $auto_database->email=$request->input('email');
            $auto_database->direccion=$request->input('direccion');
            $auto_database->ciudad=$request->input('ciudad');
            $auto_database->celular=$request->input('celular');
            $auto_database->placa=$request->input('placa');
            $auto_database->marca=$request->input('marca');
            $auto_database->color=$request->input('color');
            $auto_database->referencia_marca=$request->input('referencia');
            $auto_database->updated_at=$date->format('d-m-Y H:i:s');
            $auto_database->created_at=$date->format('d-m-Y H:i:s');
            $auto_database->estado_transaccion="INGRESO DE DATOS";
            $auto_database->requestid=""; //Go it when link placetopay are ready
            $auto_database->referencia="";
            $auto_database->url="";
            $auto_database->save();
            session(['nombre_auto' => $request->input('nombre')]);
            session(['apellido_auto' => $request->input('apellido')]);
            session(['documento_auto' => $request->input('documento')]);
            session(['email_auto' => $request->input('email')]);
            session(['celular_auto' => $request->input('celular')]);
            session(['ciudad_auto' => $request->input('ciudad')]);
            session(['direccion_auto' => $request->input('direccion')]);
            session(['placa_auto' => $request->input('placa')]);
            session(['color_auto' => $request->input('color')]);
            session(['last_name' => $request->input('nombre')]);
            session(['last_sname' => $request->input('apellido')]);
            session(['last_email' => $request->input('email')]);
            session(['last_ident' => $request->input('documento')]);
            session(['last_tipdoc' => $request->input('tipdocumento')]);
            session(['last_cel' => $request->input('celular')]);
            session(['last_dir' => $request->input('direccion')]);
            session(['last_city' => $request->input('ciudad')]);
            session(['last_placa' => $request->input('placa')]);
            session(['last_marca' => $request->input('marca')]);
            session(['last_color' => $request->input('color')]);
            session(['last_refer-marca' => $request->input('referencia')]);
            session(['last_prod' => "AUTO"]);
            }
            elseif ($request->input('producto') == "MOTO"){
                $moto_database = new Moto;
                $moto_database->fecha=$date->format('d-m-Y H:i:s');
                $moto_database->nombre=$request->input('nombre');
                $moto_database->apellido=$request->input('apellido');
                $moto_database->tipdocumento=$request->input('tipdocumento');
                $moto_database->documento=$request->input('documento');
                $moto_database->email=$request->input('email');
                $moto_database->direccion=$request->input('direccion');
                $moto_database->ciudad=$request->input('ciudad');
                $moto_database->celular=$request->input('celular');
                $moto_database->placa=$request->input('placa');
                $moto_database->updated_at=$date->format('d-m-Y H:i:s');
                $moto_database->created_at=$date->format('d-m-Y H:i:s');
                $moto_database->estado_transaccion="INGRESO DE DATOS";
                $moto_database->requestid="";
                $moto_database->referencia="";
                $moto_database->url="";
                $moto_database->save();
                session(['nombre_moto' => $request->input('nombre')]);
                session(['apellido_moto' => $request->input('apellido')]);
                session(['email_moto' => $request->input('email')]);
                session(['celular_moto' => $request->input('celular')]);
                session(['documento_moto' => $request->input('documento')]);
                session(['ciudad_moto' => $request->input('ciudad')]);
                session(['direccion_moto' => $request->input('direccion')]);
                session(['placa_moto' => $request->input('placa')]);
                session(['last_name' => $request->input('nombre')]);
                session(['last_sname' => $request->input('apellido')]);
                session(['last_email' => $request->input('email')]);
                session(['last_ident' => $request->input('documento')]);
                session(['last_tipdoc' => $request->input('tipdocumento')]);
                session(['last_cel' => $request->input('celular')]);
                session(['last_dir' => $request->input('direccion')]);
                session(['last_city' => $request->input('ciudad')]);
                session(['last_placa' => $request->input('placa')]);
                session(['last_marca' => ""]);
                session(['last_color' => ""]);
                session(['last_refer-marca' => ""]);
                session(['last_prod' => "MOTO"]);
            } elseif ($request->input('producto') == "MULTI") {
                $multi_database = new Multiasistencia;
                $multi_database->fecha=$date->format('d-m-Y H:i:s');
                $multi_database->nombre=$request->input('nombre');
                $multi_database->apellido=$request->input('apellido');
                $multi_database->tipdocumento=$request->input('tipdocumento');
                $multi_database->documento=$request->input('documento');
                $multi_database->email=$request->input('email');
                $multi_database->direccion=$request->input('direccion');
                $multi_database->ciudad=$request->input('ciudad');
                $multi_database->celular=$request->input('celular');
                $multi_database->placa=$request->input('placa');
                $multi_database->updated_at=$date->format('d-m-Y H:i:s');
                $multi_database->created_at=$date->format('d-m-Y H:i:s');
                $multi_database->estado_transaccion="INGRESO DE DATOS";
                $multi_database->requestid="";
                $multi_database->referencia="";
                $multi_database->url="";
                $multi_database->save();
                session(['nombre_multi' => $request->input('nombre')]);
                session(['apellido_multi' => $request->input('apellido')]);
                session(['email_multi' => $request->input('email')]);
                session(['celular_multi' => $request->input('celular')]);
                session(['direccion_multi' => $request->input('direccion')]);
                session(['ciudad_multi' => $request->input('ciudad')]);
                session(['placa_multi' => $request->input('placa')]);
                session(['documento_multi' => $request->input('documento')]);
                session(['last_name' => $request->input('nombre')]);
                session(['last_sname' => $request->input('apellido')]);
                session(['last_email' => $request->input('email')]);
                session(['last_ident' => $request->input('documento')]);
                session(['last_tipdoc' => $request->input('tipdocumento')]);
                session(['last_dir' => $request->input('direccion')]);
                session(['last_cel' => $request->input('celular')]);
                session(['last_city' => $request->input('ciudad')]);
                session(['last_placa' => $request->input('placa')]);
                session(['last_marca' => ""]);
                session(['last_color' => ""]);
                session(['last_refer-marca' => ""]);
                session(['last_prod' => "MULTI"]);
            }

        $products = Products::find($request->input('id_producto'));
        Cart::add(
            $products->id,
            $products->producto,
            $products->precio,
            //$products->meses,
            1,
            //$products->total,
            array($products->total)
        );        

        $btn = $request->input('btn1');
        if ($btn == 2){
            return view('layouts/asistencia_resp');
            }
        $total = Cart::getSubTotal();
        $num = (int)$total;
        $total = number_format($num , 0);
        $total = str_replace(",",".", $total);
        $precio = $total;

        return view('layouts/cart', compact('precio', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * Checking car from header
     */
    public function checking(Request $request)
    {
        $date = Carbon::now();        
        $total = Cart::getSubTotal();
        $num = (int)$total;
        $total = number_format($num , 0);
        $total = str_replace(",",".", $total);
        $precio = $total;

        return view('layouts/cart', compact('precio', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     * Cheking car from product - contents
     */
   public function cart_check()
   {
       $date = Carbon::now();
       $total = Cart::getSubTotal();
       $num = (int)$total;
       $total = number_format($num , 0);
       $total = str_replace(",",".", $total);
       $precio = $total;

       return view('layouts/cart', compact('precio', 'total'));
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function check_multi()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        /**
        $products = Products::find($request->input('id_producto'));
        Cart::add(
            $products->id,
            $products->producto,
            $products->precio,
            //$products->meses,
            1,
            //$products->total,
            array($products->total)
        );
        $success = "Producto agregado con éxito";
        return back()->with('success','success');
        */
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * Remove product of car
     */
    public function remove(Request $request)
    {
        Cart::remove([
        'id' => $request->input('id_producto'),
        ]);
        $total = Cart::getSubTotal();
        $num = (int)$total;
        $total = number_format($num , 0);
        $precio = $total;
        //return back()->with('success',"Eliminado con éxito", 'url','$url');
        //return view('layouts/cart',   compact('success','url')->with('success',"Eliminado con éxito"));
        //return redirect()->back()->with(compact('success','url', 'precio','deactive'));
        return view('layouts/cart' , compact('precio', 'total',));

    }

    /**
     * Display the specified resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * Connect whit Placetopay Webservices
     */
    public function data_pay(Request $request)
    {
        $cartCollection = Cart::getContent();

        $date = Carbon::now();
        
        if ($request->input('producto') == "AUTO"){
            $auto_database = new Auto;
            $auto_database->fecha=$date->format('d-m-Y H:i:s');
            $auto_database->nombre=$request->input('nombre');
            $auto_database->apellido=$request->input('apellido');
            $auto_database->tipdocumento=$request->input('tipdocumento');
            $auto_database->documento=$request->input('documento');
            $auto_database->email=$request->input('email');
            $auto_database->direccion=$request->input('direccion');
            $auto_database->ciudad=$request->input('ciudad');
            $auto_database->celular=$request->input('celular');
            $auto_database->placa=$request->input('placa');
            $auto_database->marca=$request->input('marca');
            $auto_database->color=$request->input('color');
            $auto_database->referencia_marca=$request->input('referencia');
            $auto_database->updated_at=$date->format('d-m-Y H:i:s');
            $auto_database->created_at=$date->format('d-m-Y H:i:s');
            $auto_database->estado_transaccion="INGRESO DE DATOS";
            $auto_database->requestid=""; //Go it when link placetopay are ready
            $auto_database->referencia="";
            $auto_database->url="";
            $auto_database->save();
            session(['nombre_auto' => $request->input('nombre')]);
            session(['apellido_auto' => $request->input('apellido')]);
            session(['documento_auto' => $request->input('documento')]);
            session(['email_auto' => $request->input('email')]);
            session(['celular_auto' => $request->input('celular')]);
            session(['ciudad_auto' => $request->input('ciudad')]);
            session(['direccion_auto' => $request->input('direccion')]);
            session(['placa_auto' => $request->input('placa')]);
            session(['color_auto' => $request->input('color')]);
            session(['last_name' => $request->input('nombre')]);
            session(['last_sname' => $request->input('apellido')]);
            session(['last_email' => $request->input('email')]);
            session(['last_ident' => $request->input('documento')]);
            session(['last_tipdoc' => $request->input('tipdocumento')]);
            session(['last_cel' => $request->input('celular')]);
            session(['last_dir' => $request->input('direccion')]);
            session(['last_city' => $request->input('ciudad')]);
            session(['last_placa' => $request->input('placa')]);
            session(['last_marca' => $request->input('marca')]);
            session(['last_color' => $request->input('color')]);
            session(['last_refer-marca' => $request->input('referencia')]);
            session(['last_prod' => "AUTO"]);
            }
            elseif ($request->input('producto') == "MOTO"){
                $moto_database = new Moto;
                $moto_database->fecha=$date->format('d-m-Y H:i:s');
                $moto_database->nombre=$request->input('nombre');
                $moto_database->apellido=$request->input('apellido');
                $moto_database->tipdocumento=$request->input('tipdocumento');
                $moto_database->documento=$request->input('documento');
                $moto_database->email=$request->input('email');
                $moto_database->direccion=$request->input('direccion');
                $moto_database->ciudad=$request->input('ciudad');
                $moto_database->celular=$request->input('celular');
                $moto_database->placa=$request->input('placa');
                $moto_database->updated_at=$date->format('d-m-Y H:i:s');
                $moto_database->created_at=$date->format('d-m-Y H:i:s');
                $moto_database->estado_transaccion="INGRESO DE DATOS";
                $moto_database->requestid="";
                $moto_database->referencia="";
                $moto_database->url="";
                $moto_database->save();
                session(['nombre_moto' => $request->input('nombre')]);
                session(['apellido_moto' => $request->input('apellido')]);
                session(['email_moto' => $request->input('email')]);
                session(['celular_moto' => $request->input('celular')]);
                session(['documento_moto' => $request->input('documento')]);
                session(['ciudad_moto' => $request->input('ciudad')]);
                session(['direccion_moto' => $request->input('direccion')]);
                session(['placa_moto' => $request->input('placa')]);
                session(['last_name' => $request->input('nombre')]);
                session(['last_sname' => $request->input('apellido')]);
                session(['last_email' => $request->input('email')]);
                session(['last_ident' => $request->input('documento')]);
                session(['last_tipdoc' => $request->input('tipdocumento')]);
                session(['last_cel' => $request->input('celular')]);
                session(['last_dir' => $request->input('direccion')]);
                session(['last_city' => $request->input('ciudad')]);
                session(['last_placa' => $request->input('placa')]);
                session(['last_marca' => ""]);
                session(['last_color' => ""]);
                session(['last_refer-marca' => ""]);
                session(['last_prod' => "MOTO"]);
            } elseif ($request->input('producto') == "MULTI") {
                $multi_database = new Multiasistencia;
                $multi_database->fecha=$date->format('d-m-Y H:i:s');
                $multi_database->nombre=$request->input('nombre');
                $multi_database->apellido=$request->input('apellido');
                $multi_database->tipdocumento=$request->input('tipdocumento');
                $multi_database->documento=$request->input('documento');
                $multi_database->email=$request->input('email');
                $multi_database->direccion=$request->input('direccion');
                $multi_database->ciudad=$request->input('ciudad');
                $multi_database->celular=$request->input('celular');
                $multi_database->placa=$request->input('placa');
                $multi_database->updated_at=$date->format('d-m-Y H:i:s');
                $multi_database->created_at=$date->format('d-m-Y H:i:s');
                $multi_database->estado_transaccion="INGRESO DE DATOS";
                $multi_database->requestid="";
                $multi_database->referencia="";
                $multi_database->url="";
                $multi_database->save();
                session(['nombre_multi' => $request->input('nombre')]);
                session(['apellido_multi' => $request->input('apellido')]);
                session(['email_multi' => $request->input('email')]);
                session(['celular_multi' => $request->input('celular')]);
                session(['direccion_multi' => $request->input('direccion')]);
                session(['ciudad_multi' => $request->input('ciudad')]);
                session(['placa_multi' => $request->input('placa')]);
                session(['documento_multi' => $request->input('documento')]);
                session(['last_name' => $request->input('nombre')]);
                session(['last_sname' => $request->input('apellido')]);
                session(['last_email' => $request->input('email')]);
                session(['last_ident' => $request->input('documento')]);
                session(['last_tipdoc' => $request->input('tipdocumento')]);
                session(['last_dir' => $request->input('direccion')]);
                session(['last_cel' => $request->input('celular')]);
                session(['last_city' => $request->input('ciudad')]);
                session(['last_placa' => $request->input('placa')]);
                session(['last_marca' => ""]);
                session(['last_color' => ""]);
                session(['last_refer-marca' => ""]);
                session(['last_prod' => "MULTI"]);
            }

        $date = Carbon::now();
        $tomorrow = $date->add(1, 'day');
        $minutes = $date->subMinute(50);
        $monthmore = $date->addMonth()->format('Y-m-d');

        $nombre = session('last_name');
        $apellido = session('last_sname');
        $email = session('last_email');
        $documento = session('last_ident');
        $tipdoc = session('last_tipdoc');
        $celular = session('last_cel');
        $product = session('last_prod');
        $referencia = $product.$documento.$date->format('dmYHis');
        session(['referencia_transaccion' => $referencia]);

        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
            } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
            } else {
            $nonce = mt_rand();
            }
            $nonceBase64 = base64_encode($nonce);

        $seed = date('c');
        $secretKey = 'aIMRMh8tpKiU5UGN';
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $req["auth"]["seed"] = $seed;
        $req["auth"]["nonce"] = $nonceBase64;
        $req["auth"]["tranKey"] = $tranKey;
        $req["expiration"] = $tomorrow;

        //PROD: https://pagoscencosud.connect-sos.com/confirmasist
        //TEST: http://127.0.0.1:8000/confirmasist
        

        if ($request->input('checkadd') == "approv") {
            #SI ACEPTÓ SUSCRIPCION Y PAGO AUTOMATICO PARA GENERAR TOKEN ASOCIADO A TARJETA DE PAGO - AQUI SE GENERA LINK PARA QUE EL CLIENTE HAGA LA SUSCRIPCION DE LA TARJETA - SI LA SUSCRIPCION ES EXITOSA EL RECAUDO Y COBRO SE HACE CON METODO /COLLECT EN LA ULTIMA PAGINA https://pagoscencosud.connect-sos.com/confirmasist"

            $json = '{
                "auth": {
                    "login": "825e1b4e038b3c380aefc33181bed268",
                    "tranKey": "'.$tranKey.'",
                    "nonce": "'.$nonceBase64.'",
                    "seed": "'.$seed.'"
                   },
                "subscription": {
                "reference": "'.$referencia.'",
                "description": "Una suscripción de prueba"
                },
                "expiration": "'.$minutes.'",
                "ipAddress": "201.184.3.213",
                "returnUrl": "https://pagoscencosud.connect-sos.com/confirmasist",
                "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36",
                "paymentMethod": "",
                "skipResult": "True"
                }';
                $req = json_decode($json,true);

                $client = new Client([
                    // You can set any number of default request options.
                    'header'  => 'Content-type: application/json',
                    'method'=>'POST',
                    'json'=>$req,
                    'verify' => false,
                    'timeout' => 1000,
                ]);
                
                $response = $client->request('post', 'https://secure.placetopay.com/redirection/api/session/', [ 'body' => json_encode($req) ]);
                $response = json_decode($response->getBody()->getContents(), true);


                $response_encode = json_encode($response["processUrl"], true);
                $requestId = json_encode($response["requestId"], true);
                $cambio1 = str_replace("\/\/","//", $response_encode);
                $cambio2 = str_replace("\/","/", $cambio1);
                $cambio3 = str_replace('"',"", $cambio2);
                $url = $cambio3;

                session(['requestId' => $requestId]);

                if ($product == "AUTO"){
                    $data = Auto::where('documento',$documento)->update(['estado_transaccion' => "PENDING"]);
                    $data = Auto::where('documento',$documento)->update(['requestid' => $requestId]);
                    $data = Auto::where('documento',$documento)->update(['referencia' => $referencia]);
                    $data = Auto::where('documento',$documento)->update(['url' => $url]);
                }
                if ($product == "MOTO"){
                    $data = Moto::where('documento',$documento)->update(['estado_transaccion' => "PENDING"]);
                    $data = Moto::where('documento',$documento)->update(['requestid' => $requestId]);
                    $data = Moto::where('documento',$documento)->update(['referencia' => $referencia]);
                    $data = Moto::where('documento',$documento)->update(['url' => $url]);
                }
                if ($product == "MULTI"){
                    $data = Multiasistencia::where('documento',$documento)->update(['estado_transaccion' => "PENDING"]);
                    $data = Multiasistencia::where('documento',$documento)->update(['requestid' => $requestId]);
                    $data = Multiasistencia::where('documento',$documento)->update(['referencia' => $referencia]);
                    $data = Multiasistencia::where('documento',$documento)->update(['url' => $url]);
                }
                
                return redirect()->away($url);
    
        }

        $cambio1 = str_replace("$","", $request->input('total'));
        if ($cambio1 == ""){
            $cambio1 = str_replace("$","", $request->input('totalselect'));
        }

        $totalpagar = str_replace(".","", $cambio1);
        session(['totalpagar' => $totalpagar]);
        

        $json = '{
            "buyer": {
              "name": "'.$nombre.'",
              "surname": "'.$apellido.'",
              "email": "'.$email.'",
              "document": "'.$documento.'",
              "documentType": "'.$tipdoc.'",
              "mobile": "'.$celular.'"
            },
            "payment": {
              "reference": "'.$referencia.'",
              "description": "Pago a producto seguros Cencosud",
              "amount": {
                "currency": "COP",
                "total": "'.$totalpagar.'"
                }
            },
            "expiration": "'.$tomorrow.'",
            "ipAddress": "201.184.3.213",
            "returnUrl": "https://pagoscencosud.connect-sos.com/confirmasist",
            "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36",
            "paymentMethod": "",
            "skipResult": "True",
            
            "auth": {
              "login": "825e1b4e038b3c380aefc33181bed268",
              "tranKey": "'.$tranKey.'",
              "nonce": "'.$nonceBase64.'",
              "seed": "'.$seed.'"
             }
            }';
            
        $req = json_decode($json,true);

        $client = new Client([
            // You can set any number of default request options.
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req,
            'verify' => false,
            'timeout' => 1000,
        ]);
        
        $response = $client->request('post', 'https://secure.placetopay.com/redirection/api/session/', [ 'body' => json_encode($req) ]);
        $response = json_decode($response->getBody()->getContents(), true);

        $response_encode = json_encode($response["processUrl"], true);
        $requestId = json_encode($response["requestId"], true);
        $cambio1 = str_replace("\/\/","//", $response_encode);
        $cambio2 = str_replace("\/","/", $cambio1);
        $cambio3 = str_replace('"',"", $cambio2);
        $url = $cambio3;

        session(['requestId' => $requestId]);

        if ($product == "AUTO"){
            $data = Auto::where('documento',$documento)->update(['estado_transaccion' => "PENDING"]);
            $data = Auto::where('documento',$documento)->update(['requestid' => $requestId]);
            $data = Auto::where('documento',$documento)->update(['referencia' => $referencia]);
            $data = Auto::where('documento',$documento)->update(['url' => $url]);
        }
        if ($product == "MOTO"){
            $data = Moto::where('documento',$documento)->update(['estado_transaccion' => "PENDING"]);
            $data = Moto::where('documento',$documento)->update(['requestid' => $requestId]);
            $data = Moto::where('documento',$documento)->update(['referencia' => $referencia]);
            $data = Moto::where('documento',$documento)->update(['url' => $url]);
        }
        if ($product == "MULTI"){
            $data = Multiasistencia::where('documento',$documento)->update(['estado_transaccion' => "PENDING"]);
            $data = Multiasistencia::where('documento',$documento)->update(['requestid' => $requestId]);
            $data = Multiasistencia::where('documento',$documento)->update(['referencia' => $referencia]);
            $data = Multiasistencia::where('documento',$documento)->update(['url' => $url]);
        }
        // GUARDAR REQUEST ID PARA CONFIRM
        
        return redirect()->away($url);
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirm()
    {

        $date = Carbon::now();
        //$value = "1000"; GET THE REAL
        $description = "Pago de seguro Asistencias";
        $date = date("d-m-Y");

        $nombre = session('last_name');
        $apellido = session('last_sname');
        $email = session('last_email');
        $celphone = session('last_cel');
        $documento = session('last_ident');
        $tipdoc = session('last_tipdoc');
        $direccion = session('last_dir');
        $ciudad = session('last_city');
        $placa = session('last_placa');
        $requestId = session('requestId');
        $referencia_transaccion = session('referencia_transaccion');
        $marca = session('last_marca');
        $color = session('last_color');
        $referencia_marca = session('last_refer-marca');
        $totalpagar = session('totalpagar');

        

        $producto = session('last_prod');
    
        session()->forget('link_auto');
        session()->forget('link_moto');
        session()->forget('link_multi');

        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
          } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
          } else {
            $nonce = mt_rand();
          }
          $nonceBase64 = base64_encode($nonce);

        $seed = date('c');
        $secretKey = 'aIMRMh8tpKiU5UGN'; // trankey  PROD aIMRMh8tpKiU5UGN
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));
        //prod: login 825e1b4e038b3c380aefc33181bed268
        $auth = '
        {
            "auth": {
                "login": "825e1b4e038b3c380aefc33181bed268", 
                "tranKey": "'.$tranKey.'",
                "nonce": "'.$nonceBase64.'",
                "seed": "'.$seed.'"
            }
        }
        ';

        $req = json_decode($auth,true);

        $req["auth"]["tranKey"] = $tranKey;
        $req["auth"]["nonce"] = $nonceBase64;
        $req["auth"]["seed"] = $seed;

        $client = new Client([
            // You can set any number of default request options.
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req,
            'verify' => false,
            'timeout' => 1000,
        ]);
        
        $response2 = $client->request('post', 'https://secure.placetopay.com/redirection/api/session/'.$requestId, [ 'body' => json_encode($req) ]);

        $body2 = json_decode($response2->getBody()->getContents(), true);

        $status_resp = json_encode($body2['status']['status'], true);
        $status = str_replace('"',"", $status_resp);

        #$payment = json_encode($body2['payment'], true);

        #$value = json_encode($body2['request']['payment']['amount']['total'], true);

        try {
            $value = json_encode($body2['request']['payment']['amount']['total'], true);
            #dd("SI HAY REGISTRO DE PAGO UNO A UNO - PAGO NORMAL");

            $value = str_replace('"',"", $value);
            $num = (int)$value;
            $value = number_format($num , 0);
            
            $celphone_resp = json_encode($body2['request']['buyer']['mobile'], true);
            $celphone = str_replace('"',"", $celphone_resp);

            $description_resp = json_encode($body2['request']['payment']['description'], true);
            $description = str_replace('"',"", $description_resp);

            $date = date("d-m-Y");
            $refer = json_encode($body2['request']['payment']['reference'], true);
            $refer = str_replace('"',"", $refer);
        
        } catch (\Exception $e) {
                #dd("");    
                $value = "0";
                $refer = $referencia_transaccion;
                $error = $e;


                
        }

        #$status = "PENDING";
    
        if ($status == "APPROVED"){            
            $mensaje="Gracias por tu compra";
            $mensaje2="Te llegará un correo electrónico con el link de descarga de tu producto adquirido.";
            $response="Transacción Aprobada";
            $tittle = "ESTÁ HECHO";
            $imagen="https://connect-static-files.s3.amazonaws.com/pagos/Check-Icon.png";
            $cert = "Descarga aquí certificado de tu poliza:";

            $login = '
            {
                "login": {
                    "user": "CENCOSUD",
                    "pass": "F3MccICphHkk/vnXyzbeEOxZR09YMbvfKx/uCdyHr3LWudzCXFjzzna+/aVb8GZIUBNF8L2kFDaFKeBV+yyq9Q==",
                    "country": 7,
                    "remote_addr": ""
                    }
                }
            ';

            $req_login = json_decode($login,true);

            $client = new Client([
                'header'  => 'Content-type: application/json',
                'method'=>'POST',
                'json'=>$req_login,
                'timeout' => 600,
                'verify' => false,
            ]);

            $resp_content = $client->request('post', 'https://api.mapfre.com/srv/warranties/login', ['body' => json_encode($req_login)], ['verify' => false]);
            $resp_content = json_decode($resp_content->getBody()->getContents(), true);
            //print("RESPUESTA".json_encode($resp_content));

            $token = json_encode($resp_content["root"]["token"], true);
            $cambio1 = str_replace("\/\/","//", $token);
            $cambio2 = str_replace("\/","/", $cambio1);
            $token_id = str_replace('"',"", $cambio2);
    
            //dd($cartCollection->name); AQUI GUARDAR LOS APROBADOS
                
            $date = Carbon::now();
            $auto_database = new Payments;
            $auto_database->fecha=$date->format('d-m-Y H:i:s');
            $auto_database->nombre=$nombre;
            $auto_database->apellido=$apellido;
            $auto_database->tipdocumento=$tipdoc;
            $auto_database->documento=$documento;
            $auto_database->email=$email;
            $auto_database->direccion=$direccion;
            $auto_database->ciudad=$ciudad;
            $auto_database->celular=$celphone;
            $auto_database->placa=$placa;
            $auto_database->marca=$marca;
            $auto_database->color=$color;
            $auto_database->referencia_marca=$referencia_marca;
            $auto_database->created_at=$date->format('d-m-Y H:i:s');
            $auto_database->updated_at=$date->format('d-m-Y H:i:s');
            $auto_database->estado_transaccion="APPROVED";
            $auto_database->requestid=$requestId; //Go it when link placetopay are ready
            $auto_database->referencia=$referencia_transaccion;
            $auto_database->productos=Cart::getTotalQuantity();
            $auto_database->entidad_recaudadora="";
            $auto_database->metodo_pago="";
            $auto_database->total=Cart::getTotal();
            $auto_database->save();


            if ($producto == "AUTO"){
                $peticions = Auto::where('requestId',$requestId)->update(['estado_transaccion' => "APPROVED"]);
                
                $date = Carbon::now();
                $nombre = session('nombre_auto');
                $apellido = session('apellido_auto');
                $documento = session('documento_auto');
                $email = session('email_auto');
                $celular = session('celular_auto');
                $ciudad = session('ciudad_auto');
                $direccion = session('direccion_auto');
                $placa = session('placa_auto');
                $color = session('color_auto');
                $idmarca = session('idmarca_auto');
                $inicio = $date->format('d/m/Y');
                $fin_add = $date->addYear();
                $fin = $fin_add->format('d/m/Y');
            
                $issuingPlus = '
                {
                    "issuingPlus":
                    {
                        "token":"'.$token_id.'",
                        "xml":"<root><coberData><CoberturaLimites/><chk_AA0_11_10>1</chk_AA0_11_10></coberData><ecasData/><insuredData><cmbTipoCIF>1</cmbTipoCIF><txtApeAsegurado>'.$apellido.'</txtApeAsegurado><txtCiudadPaisAsegurado>bogota</txtCiudadPaisAsegurado><txtDirAsegurado>'.$direccion.'</txtDirAsegurado><txtEmail>'.$email.'</txtEmail><txtIdFiscal>'.$documento.'</txtIdFiscal><txtNmAsegurado>'.$nombre.'</txtNmAsegurado><txtTelefono>'.$celular.'</txtTelefono></insuredData><policyData><contratoReplicadoV2/><idRegDivisaProducto/><idRegFranquicia>-1</idRegFranquicia><idRegProducto>749</idRegProducto><idRegProductoComisionVariable>-1</idRegProductoComisionVariable><idRegRegion>-1</idRegRegion><tipoPagador>-1</tipoPagador><txtCodDealer>1057331</txtCodDealer><txtCodPromocion/><txtComentario/><txtDivisaProducto/><txtDuracion>12</txtDuracion><txtFHExpiracion/><txtFhCambioDivisa/><txtFhFin>'.$fin.'</txtFhFin><txtFhInicio>'.$inicio.'</txtFhInicio><txtNpoliza>AUTO</txtNpoliza><txtPrecioBrutoTotal/><txtProducto>ASISTENCIA CENCOSUD AUTOS LIVIANOS</txtProducto><txtSufijo>CSAUTO</txtSufijo></policyData><riskData><cmbMarca>1</cmbMarca><cmbModelo>3</cmbModelo><txtColor>'.$color.'</txtColor><txtMatricula>'.$placa.'</txtMatricula></riskData><tomadorData><cmbTipoCIF_policyHolder>1</cmbTipoCIF_policyHolder><txtApeAsegurado_policyHolder>'.$apellido.'</txtApeAsegurado_policyHolder><txtCiudadPaisAsegurado_policyHolder>'.$ciudad.'</txtCiudadPaisAsegurado_policyHolder><txtDirAsegurado_policyHolder>'.$direccion.'</txtDirAsegurado_policyHolder><txtEmail_policyHolder>'.$email.'</txtEmail_policyHolder><txtIdFiscal_policyHolder>'.$documento.'</txtIdFiscal_policyHolder><txtNmAsegurado_policyHolder>'.$nombre.'</txtNmAsegurado_policyHolder><txtTelefono_policyHolder>'.$celular.'</txtTelefono_policyHolder><payData><chkMensualizar>1</chkMensualizar><cmbMesesMensualizar>1</cmbMesesMensualizar><cmbRangoMensualizar>1</cmbRangoMensualizar></payData></tomadorData><parameters><action>A</action><origenRecepcion>2</origenRecepcion><posicionTomador>1</posicionTomador><noAceptaOfertasCom>0</noAceptaOfertasCom><permitirEnvioPublicidad>0</permitirEnvioPublicidad><permitirVentaDistancia>0</permitirVentaDistancia><permitirEstudioMercado>0</permitirEstudioMercado></parameters></root>",
                        "printCertificate":"0"
                    }
                }
                
                ';

                $req_issuingP = json_decode($issuingPlus,true);

                $client2 = new Client([
                    'header'  => 'Content-type: application/json',
                    'Connection: keep-alive',
                    'method'=>'POST',
                    'json'=>$req_issuingP,
                    'timeout' => 900,
                    'verify' => false,
                ]);

                $resp_content = $client2->request('post', 'https://apisb.mapfre.com/srv/warranties/issuingPlus', ['body' => json_encode($req_issuingP)], ['verify' => false]);
                $resp_content = json_decode($resp_content->getBody()->getContents(), true);

                if (array_key_exists('ERROR', $resp_content)) {
                    //dd("OCURRIO UN ERROR NO SE PUDO EMITIR CERTIFICADO AQUI SE ESTA TRABAJANDO PAGINA A MOSTRAR",$resp_content);
                    Cart::clear();
                    $date = Carbon::now();
                    $date = $date->format('d/m/Y');
                    $mensaje2="Hubo un error al emitir la póliza con los datos ingresados. Comunicate a nuestras lineas de atención para más información.";
                    $cert = "";
                    return view('layouts/confirmasist', compact('mensaje2','mensaje','imagen', 'value', 'celphone', 'description', 'date', 'refer', 'response', 'tittle', 'cert'));
                }

                //dd("RESPUESTA".json_encode($resp_content));
                $idRegContrato = json_encode($resp_content["root"]["idRegContrato"], true); //try sesion terminada
                $idRegContrato1 = str_replace("\/\/","//", $idRegContrato);
                $idRegContrato2 = str_replace("\/","/", $idRegContrato1);
                $idcertificado = str_replace('"',"", $idRegContrato2);

                $printCertificate = '
                {
                    "printCertificate": {
                      "token": "'.$token_id.'",
                      "certificateID": "'.$idcertificado.'"
                    }
                  }
                
                ';

                $printCert = json_decode($printCertificate,true);

                $client3 = new Client([
                    'header'  => 'Content-type: application/json',
                    'Connection: keep-alive',
                    'method'=>'POST',
                    'json'=>$printCert,
                    'timeout' => 900,
                    'verify' => false,
                ]);

                $resp_cert = $client3->request('post', 'https://apisb.mapfre.com/srv/warranties/printCertificate', ['body' => json_encode($printCert)], ['verify' => false]);
                $resp_cert = json_decode($resp_cert->getBody()->getContents(), true);
                $respuesta = json_encode($resp_cert["respuesta"], true);
                $respuesta1 = str_replace("\/\/","//", $respuesta);
                $respuesta2 = str_replace("\/","/", $respuesta1);
                $url = str_replace('"',"", $respuesta2);

                session(['link_auto' => $url]);
                } 

            if ($producto == "MOTO"){
                $peticions = Moto::where('requestId',$requestId)->update(['estado_transaccion' => "APPROVED"]);
                $date = Carbon::now();
                $nombre = session('nombre_moto');
                $apellido = session('apellido_moto');
                $documento = session('documento_moto');
                $email = session('email_moto');
                $celular = session('celular_moto');
                $ciudad = session('ciudad_moto');
                $direccion = session('direccion_moto');
                $placa = session('placa_moto');
                $inicio = $date->format('d/m/Y');
                $fin_add = $date->addYear();
                $fin = $fin_add->format('d/m/Y');

                $issuingPlus = '
                {
                    "issuingPlus":
                    {
                        "token": "'.$token_id.'",
                        "xml":"<root><coberData><CoberturaLimites/><chk_AA0_11_10>1</chk_AA0_11_10></coberData><ecasData/><insuredData><cmbTipoCIF>1</cmbTipoCIF><txtApeAsegurado>'.$apellido.'</txtApeAsegurado><txtCiudadPaisAsegurado>'.$ciudad.'</txtCiudadPaisAsegurado><txtDirAsegurado>'.$direccion.'</txtDirAsegurado><txtEmail>'.$email.'</txtEmail><txtIdFiscal>'.$documento.'</txtIdFiscal><txtNmAsegurado>'.$nombre.'</txtNmAsegurado><txtTelefono>'.$celular.'</txtTelefono></insuredData><policyData><contratoReplicadoV2/><idRegDivisaProducto/><idRegFranquicia>-1</idRegFranquicia><idRegProducto>796</idRegProducto><idRegProductoComisionVariable>-1</idRegProductoComisionVariable><idRegRegion>-1</idRegRegion><tipoPagador>-1</tipoPagador><txtCodDealer>1057331</txtCodDealer><txtCodPromocion/><txtComentario/><txtDivisaProducto/><txtDuracion>12</txtDuracion><txtFHExpiracion/><txtFhCambioDivisa/><txtFhFin>'.$fin.'</txtFhFin><txtFhInicio>'.$inicio.'</txtFhInicio><txtNpoliza>AUTO</txtNpoliza><txtPrecioBrutoTotal/><txtProducto>ASISTENCIA CENCOSUD MOTOS</txtProducto><txtSufijo>CSMOTO</txtSufijo></policyData><riskData><txtColor>negro</txtColor><txtMatricula>'.$placa.'</txtMatricula></riskData><tomadorData><cmbTipoCIF_policyHolder>1</cmbTipoCIF_policyHolder><txtApeAsegurado_policyHolder>'.$apellido.'</txtApeAsegurado_policyHolder><txtCiudadPaisAsegurado_policyHolder>'.$ciudad.'</txtCiudadPaisAsegurado_policyHolder><txtDirAsegurado_policyHolder>'.$direccion.'</txtDirAsegurado_policyHolder><txtEmail_policyHolder>'.$email.'</txtEmail_policyHolder><txtIdFiscal_policyHolder>'.$documento.'</txtIdFiscal_policyHolder><txtNmAsegurado_policyHolder>'.$nombre.'</txtNmAsegurado_policyHolder><txtTelefono_policyHolder>'.$celular.'</txtTelefono_policyHolder><payData><chkMensualizar>12</chkMensualizar><cmbMesesMensualizar>1</cmbMesesMensualizar><cmbRangoMensualizar>1</cmbRangoMensualizar></payData></tomadorData><parameters><action>A</action><origenRecepcion>2</origenRecepcion><posicionTomador>1</posicionTomador><noAceptaOfertasCom>0</noAceptaOfertasCom><permitirEnvioPublicidad>0</permitirEnvioPublicidad><permitirVentaDistancia>0</permitirVentaDistancia><permitirEstudioMercado>0</permitirEstudioMercado></parameters></root>",
                        "printCertificate":"0"
                    }
                }
                ';

                $req_issuingP = json_decode($issuingPlus,true);

                $client2 = new Client([
                    'header'  => 'Content-type: application/json',
                    'Connection: keep-alive',
                    'method'=>'POST',
                    'json'=>$req_issuingP,
                    'timeout' => 900,
                    'verify' => false,
                ]);

                $resp_content = $client2->request('post', 'https://apisb.mapfre.com/srv/warranties/issuingPlus', ['body' => json_encode($req_issuingP)], ['verify' => false]);
                $resp_content = json_decode($resp_content->getBody()->getContents(), true);
                //dd($resp_content);
                
                if (array_key_exists('ERROR', $resp_content)) {
                    //dd("OCURRIO UN ERROR NO SE PUDO EMITIR CERTIFICADO AQUI SE ESTA TRABAJANDO PAGINA A MOSTRAR",$resp_content);
                    Cart::clear();
                    $date = Carbon::now();
                    $date = $date->format('d/m/Y');
                    $mensaje2="Hubo un error al emitir la póliza con los datos ingresados. Comunicate a nuestras lineas de atención para más información.";
                    $cert = "";
                    return view('layouts/confirmasist', compact('mensaje2','mensaje','imagen', 'value', 'celphone', 'description', 'date', 'refer', 'response', 'tittle', 'cert'));
                }
                
                $idRegContrato = json_encode($resp_content["root"]["idRegContrato"], true); //try sesion terminada
                $idRegContrato1 = str_replace("\/\/","//", $idRegContrato);
                $idRegContrato2 = str_replace("\/","/", $idRegContrato1);
                $idcertificado = str_replace('"',"", $idRegContrato2);

                $printCertificate = '
                {
                    "printCertificate": {
                      "token": "'.$token_id.'",
                      "certificateID": "'.$idcertificado.'"
                    }
                  }
                
                ';

                $printCert = json_decode($printCertificate,true);

                $client3 = new Client([
                    'header'  => 'Content-type: application/json',
                    'Connection: keep-alive',
                    'method'=>'POST',
                    'json'=>$printCert,
                    'timeout' => 900,
                    'verify' => false,
                ]);

                $resp_cert = $client3->request('post', 'https://apisb.mapfre.com/srv/warranties/printCertificate', ['body' => json_encode($printCert)], ['verify' => false]);
                $resp_cert = json_decode($resp_cert->getBody()->getContents(), true);
                $respuesta = json_encode($resp_cert["respuesta"], true);
                $respuesta1 = str_replace("\/\/","//", $respuesta);
                $respuesta2 = str_replace("\/","/", $respuesta1);
                $url = str_replace('"',"", $respuesta2);

                session(['link_moto' => $url]);

            } 

            if ($producto == "MULTI"){
                $peticions = Multiasistencia::where('requestId',$requestId)->update(['estado_transaccion' => "APPROVED"]);
                $date = Carbon::now();
                $nombre = session('nombre_multi');
                $apellido = session('apellido_multi');
                $documento = session('documento_multi');
                $email = session('email_multi');
                $celular = session('celular_multi');
                $ciudad = session('ciudad_multi');
                $direccion = session('direccion_multi');
                $placa = session('placa_multi');
                $inicio = $date->format('d/m/Y');
                $fin_add = $date->addYear();
                $fin = $fin_add->format('d/m/Y');

                $issuingPlus = '
                {
                    "issuingPlus":
                    {
                        "token":"'.$token_id.'",
                        "xml":"<root><coberData><CoberturaLimites/><chk_MULASIS_01>1</chk_MULASIS_01></coberData><ecasData/><insuredData><cmbTipoCIF>1</cmbTipoCIF><txtApeAsegurado>'.$apellido.'</txtApeAsegurado><txtCiudadPaisAsegurado>'.$ciudad.'</txtCiudadPaisAsegurado><txtDirAsegurado>'.$direccion.'</txtDirAsegurado><txtEmail>'.$email.'</txtEmail><txtIdFiscal>'.$documento.'</txtIdFiscal><txtNmAsegurado>'.$nombre.'</txtNmAsegurado><txtTelefono>'.$celular.'</txtTelefono></insuredData><policyData><contratoReplicadoV2/><idRegDivisaProducto/><idRegFranquicia>-1</idRegFranquicia><idRegProducto>797</idRegProducto><idRegProductoComisionVariable>-1</idRegProductoComisionVariable><idRegRegion>-1</idRegRegion><tipoPagador>-1</tipoPagador><txtCodDealer>1057331</txtCodDealer><txtCodPromocion/><txtComentario/><txtDivisaProducto/><txtDuracion>12</txtDuracion><txtFHExpiracion/><txtFhCambioDivisa/><txtFhFin>'.$fin.'</txtFhFin><txtFhInicio>'.$inicio.'</txtFhInicio><txtNpoliza>AUTO</txtNpoliza><txtPrecioBrutoTotal/><txtProducto>ASISTENCIA CENCOSUD MULTIASISTENCIA</txtProducto><txtSufijo>CSMULT</txtSufijo></policyData><riskData><cmbMarca></cmbMarca><cmbModelo></cmbModelo><txtColor>negro</txtColor><txtMatricula>'.$placa.'</txtMatricula><txtDetalleVehiculo>BICICLETA TODOTERRENO TREK AZUL RIN 29</txtDetalleVehiculo></riskData><tomadorData><cmbTipoCIF_policyHolder>1</cmbTipoCIF_policyHolder><txtApeAsegurado_policyHolder>'.$apellido.'</txtApeAsegurado_policyHolder><txtCiudadPaisAsegurado_policyHolder>'.$ciudad.'</txtCiudadPaisAsegurado_policyHolder><txtDirAsegurado_policyHolder>'.$direccion.'</txtDirAsegurado_policyHolder><txtEmail_policyHolder>'.$email.'</txtEmail_policyHolder><txtIdFiscal_policyHolder>'.$documento.'</txtIdFiscal_policyHolder><txtNmAsegurado_policyHolder>'.$nombre.'</txtNmAsegurado_policyHolder><txtTelefono_policyHolder>'.$celular.'</txtTelefono_policyHolder><payData><chkMensualizar>12</chkMensualizar><cmbMesesMensualizar>1</cmbMesesMensualizar><cmbRangoMensualizar>1</cmbRangoMensualizar></payData></tomadorData><parameters><action>A</action><origenRecepcion>2</origenRecepcion><posicionTomador>1</posicionTomador><noAceptaOfertasCom>0</noAceptaOfertasCom><permitirEnvioPublicidad>0</permitirEnvioPublicidad><permitirVentaDistancia>0</permitirVentaDistancia><permitirEstudioMercado>0</permitirEstudioMercado></parameters></root>",
                        "printCertificate":"0"
                    }
                }
                ';

                $req_issuingP = json_decode($issuingPlus,true);

                $client2 = new Client([
                    'header'  => 'Content-type: application/json',
                    'Connection: keep-alive',
                    'method'=>'POST',
                    'json'=>$req_issuingP,
                    'timeout' => 900,
                    'verify' => false,
                ]);

                $resp_content = $client2->request('post', 'https://apisb.mapfre.com/srv/warranties/issuingPlus', ['body' => json_encode($req_issuingP)], ['verify' => false]);
                $resp_content = json_decode($resp_content->getBody()->getContents(), true);
                //dd("RESPUESTA".json_encode($resp_content));

                if (array_key_exists('ERROR', $resp_content)) {
                    //dd("OCURRIO UN ERROR NO SE PUDO EMITIR CERTIFICADO AQUI SE ESTA TRABAJANDO PAGINA A MOSTRAR",$resp_content);
                    Cart::clear();
                    $date = Carbon::now();
                    $date = $date->format('d/m/Y');
                    $mensaje2="Hubo un error al emitir la póliza con los datos ingresados. Comunicate a nuestras lineas de atención para más información.";
                    $cert = "";
                    return view('layouts/confirmasist', compact('mensaje2','mensaje','imagen', 'value', 'celphone', 'description', 'date', 'refer', 'response', 'tittle', 'cert'));
                }

                $idRegContrato = json_encode($resp_content["root"]["idRegContrato"], true); //try sesion terminada
                $idRegContrato1 = str_replace("\/\/","//", $idRegContrato);
                $idRegContrato2 = str_replace("\/","/", $idRegContrato1);
                $idcertificado = str_replace('"',"", $idRegContrato2);

                $printCertificate = '
                {
                    "printCertificate": {
                      "token": "'.$token_id.'",
                      "certificateID": "'.$idcertificado.'"
                    }
                  }
                ';

                $printCert = json_decode($printCertificate,true);

                $client3 = new Client([
                    'header'  => 'Content-type: application/json',
                    'Connection: keep-alive',
                    'method'=>'POST',
                    'json'=>$printCert,
                    'timeout' => 900,
                    'verify' => false,
                ]);

                $resp_cert = $client3->request('post', 'https://apisb.mapfre.com/srv/warranties/printCertificate', ['body' => json_encode($printCert)], ['verify' => false]);
                $resp_cert = json_decode($resp_cert->getBody()->getContents(), true);
                $respuesta = json_encode($resp_cert["respuesta"], true);
                $respuesta1 = str_replace("\/\/","//", $respuesta);
                $respuesta2 = str_replace("\/","/", $respuesta1);
                $url = str_replace('"',"", $respuesta2);

                session(['link_multi' => $url]);

            }

            //if($request->session()->has('link_auto'))
            if(Session::has('link_auto'))
            {
                $link_auto = session('link_auto');
                $send = '{
                    "from":{
                    "email":"seguroscencosud@connect-sos.com",
                    },
                    "personalizations":[
                    {
                        "to":[{
                                "email":"'.$email.'"
                            }],
                        "dynamic_template_data":{
                
                            "name":"'.$nombre.'",
                            "producto":"AUTO",
                            "link":"'.$link_auto.'",
                        }}],
                    "template_id":"d-bac3948551b74e188e147681d12e8979"
                }';
                $client_grid1 = new Client([
                    'body'=>$send,
                    'verify' => false,
                    'Connection: keep-alive',
                ]);
                $headers = [
                    'Authorization' => 'Bearer '. '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',        
                    'Content-type'  => 'application/json',
                ];
                $response_grid = $client_grid1->request('POST', 'https://api.sendgrid.com/v3/mail/send', ['headers' => $headers,]);

                session(['la' => $link_auto]);
            }
            if(Session::has('link_moto'))
            {
                $link_moto = session('link_moto');
                $send = '{
                    "from":{
                    "email":"seguroscencosud@connect-sos.com",
                    },
                    "personalizations":[
                    {
                        "to":[{
                                "email":"'.$email.'"
                            }],
                        "dynamic_template_data":{
                
                            "name":"'.$nombre.'",
                            "producto":"MOTO",
                            "link":"'.$link_moto.'",
                        }}],
                    "template_id":"d-bac3948551b74e188e147681d12e8979"
                }';

                $client_grid2 = new Client([
                    'body'=>$send,
                    'verify' => false,
                    'Connection: keep-alive',
                ]);
                $headers = [
                    'Authorization' => 'Bearer '. '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',        
                    'Content-type'  => 'application/json',
                ];
                $response_grid = $client_grid2->request('POST', 'https://api.sendgrid.com/v3/mail/send', ['headers' => $headers,]);

                session(['lm' => $link_moto]);
            }
            if(Session::has('link_multi'))
            {
                $link_multi = session('link_multi');
                $send = '{
                    "from":{
                    "email":"seguroscencosud@connect-sos.com",
                    },
                    "personalizations":[
                    {
                        "to":[{
                                "email":"'.$email.'"
                            }],
                        "dynamic_template_data":{
                
                            "name":"'.$nombre.'",
                            "producto":"MULTIASISTENCIAS",
                            "link":"'.$link_multi.'",
                        }}],
                    "template_id":"d-bac3948551b74e188e147681d12e8979"
                }';
                $client_grid3 = new Client([
                    'body'=>$send,
                    'verify' => false,
                    'Connection: keep-alive',
                ]);
                $headers = [
                    'Authorization' => 'Bearer '. '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',        
                    'Content-type'  => 'application/json',
                ];
                $response_grid = $client_grid3->request('POST', 'https://api.sendgrid.com/v3/mail/send', ['headers' => $headers,]);

                session(['lms' => $link_multi]);
            }

            #Aqui se rastrea si exite nodo instrument - value que hace referencia a una suscripción - value tiene el token de la suscrición --
            try {
                $token_subscription = json_encode($status_resp["subscription"]["instrument"][0]["value"], true);
                #dd("SI HAY TOKEN AHORA GUARDARLO Y HACER RECAUDO POR VALOR ESCOGIDO - VALIDAR COMO ENVIAR A ZONICK BACK");

                if (function_exists('random_bytes')) {
                    $nonce = bin2hex(random_bytes(16));
                    } elseif (function_exists('openssl_random_pseudo_bytes')) {
                    $nonce = bin2hex(openssl_random_pseudo_bytes(16));
                    } else {
                    $nonce = mt_rand();
                    }
                    $nonceBase64 = base64_encode($nonce);
        
                $seed = date('c');
                $secretKey = 'aIMRMh8tpKiU5UGN';
                $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));
        
                $req["auth"]["seed"] = $seed;
                $req["auth"]["nonce"] = $nonceBase64;
                $req["auth"]["tranKey"] = $tranKey;
                $req["expiration"] = $tomorrow;

                $json = '{
                    "auth": {
                        "login": "825e1b4e038b3c380aefc33181bed268",
                        "tranKey": "'.$tranKey.'",
                        "nonce": "'.$nonceBase64.'",
                        "seed": "'.$seed.'"
                    },
                    "instrument": {
                    "token": {
                        "token": "'.$token_subscription.'"
                        }
                    },
                    "payer": {
                        "document": "'.$documento.'",
                        "documentType": "'.$tipdoc.'",
                        "name": "'.$nombre.'",
                        "surname": "'.$apellido.'",
                        "email": "'.$email.'"
                    },
                    "payment": {
                        "reference": "'.$referencia_transaccion.'",
                        "description": "Pago con suscripción '.$referencia_transaccion.'",
                        "amount": {
                            "currency": "COP",
                            "total": "'.$totalpagar.'"
                        }
                    }
                }';
                    $req = json_decode($json,true);

                    $client = new Client([
                        // You can set any number of default request options.
                        'header'  => 'Content-type: application/json',
                        'method'=>'POST',
                        'json'=>$req,
                        'verify' => false,
                        'timeout' => 1000,
                    ]);
                    
                    $response = $client->request('post', 'https://secure.placetopay.com/redirection/api/collect/', [ 'body' => json_encode($req) ]);
                    $body2 = json_decode($response2->getBody()->getContents(), true);
                    $status_resp = json_encode($body2['status']['status'], true);
                    $status_pay_token = str_replace('"',"", $status_resp);        


                    if ($producto == "AUTOS"){
                        $peticions = Auto::where('requestId',$requestId)->update(['token' => $token_subscription]);
                        $peticions = Auto::where('requestId',$requestId)->update(['estado_transaccion' => $status_pay_token]);
        
                    }
                    if ($producto ==  "MOTOS"){
                        $peticions = Moto::where('requestId',$requestId)->update(['token' => $token_subscription]);
                        $peticions = Moto::where('requestId',$requestId)->update(['estado_transaccion' => $status_pay_token]);
        
                    }
                    if ($producto == "MULTI"){
                        $peticions = Multiasistencia::where('requestId',$requestId)->update(['token' => $token_subscription]);
                        $peticions = Multiasistencia::where('requestId',$requestId)->update(['estado_transaccion' => $status_pay_token]);
        
                    }

                    $mensaje2="Tu suscripción a sido registrada exitosamente. Te llegará un correo electrónico con el link de descarga de tu producto adquirido.";
            
            } catch (\Exception $e) {
                    #dd("NO HAY REGISTRO DE SUSCRIPCION Y GENERACION DE TOKEN DIRIGIR A CONFIRMACION DE PAGO");
                    $error = $e;
                    
            }
                
            }
        elseif ($status == "REJECTED"){

            $mensaje="TU PAGO HA SIDO RECHAZADO";
            $tittle = "PAGO RECHAZADO";
            $mensaje2="";
            $response="Transacción Rechazada";
            $imagen="https://connect-static-files.s3.amazonaws.com/pagos/Rechazado.png";
            $cert = "";

            $cartCollection = Cart::getContent();
            foreach(Cart::getContent()->sort() as $cartCollection){
            //dd($cartCollection->name);


            if ($producto == "AUTOS"){
                $peticions = Auto::where('requestId',$requestId)->update(['estado_transaccion' => "REJECTED"]);

            }
            if ($producto ==  "MOTOS"){
                $peticions = Moto::where('requestId',$requestId)->update(['estado_transaccion' => "REJECTED"]);

            }
            if ($producto == "MULTI"){
                $peticions = Multiasistencia::where('requestId',$requestId)->update(['estado_transaccion' => "REJECTED"]);

            }}

        }
        elseif ($status == "PENDING"){
            $tittle = "PAGO PENDIENTE";
            $mensaje="TU PAGO SE ENCUENTRA EN ESTADO PENDIENTE";
            $mensaje2="En este momento su pago presenta un proceso de pago cuya transacción se encuentra PENDIENTE de recibir
            confirmación por parte de su entidad financiera, por favor espere unos minutos y vuelva
            a consultar más tarde para verificar si su pago fue confirmado de forma exitosa. Si
            desea mayor información sobre el estado actual de su operación puede comunicarse a
            nuestras líneas de atención al cliente 6511102 en Bogota o 3057342036 a nivel nacional.";
            $response="Transacción Pendiente";
            $imagen="https://connect-static-files.s3.amazonaws.com/pagos/Pending.png";
            $cert = "";

            $cartCollection = Cart::getContent();
            foreach(Cart::getContent()->sort() as $cartCollection){
            //dd($cartCollection->name);
            
            if ($producto == "AUTOS"){
                $peticions = Auto::where('requestId',$requestId)->update(['estado_transaccion' => "PENDING_PAY"]);

            }
            if ($producto == "MOTOS"){
                $peticions = Moto::where('requestId',$requestId)->update(['estado_transaccion' => "PENDING_PAY"]);

            }
            if ($producto == "MULTI"){
                $peticions = Multiasistencia::where('requestId',$requestId)->update(['estado_transaccion' => "PENDING_PAY"]);

            }}
        }


        Cart::clear();
        $date = Carbon::now();
        $date = $date->format('d/m/Y');

        return view('layouts/confirmasist', compact('mensaje2','mensaje','imagen', 'value', 'celphone', 'description', 'date', 'refer', 'response', 'tittle', 'cert'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsCart  $modelsCart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsCart $modelsCart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsCart  $modelsCart
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsCart $modelsCart)
    {
        //
    }
}
