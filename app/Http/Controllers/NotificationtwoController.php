<?php

namespace App\Http\Controllers;

use App\Models\Peticion;
use App\ModelsNotificaciontwo;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class NotificationtwoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)

    {
        /**if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        } */

        $bodyContent = $request->all();

        $status = $bodyContent['status']['status'];
        
        $requestId = $bodyContent["requestId"];
        $reference = $bodyContent["reference"];

        $peticions = Peticion::where('requestId',$requestId)->where('referencia', $reference)
                                ->update(['estado_transaccion' => $status]);   //=> $status_resp

        $peticions = Peticion::where('user_id','1')->where('referencia', $reference)
                                ->get('requestID')->last();
        
        $cambio = json_encode($peticions["requestID"], true);
        $requestId = str_replace('"',"", $cambio);

        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
          } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
          } else {
            $nonce = mt_rand();
          }
          $nonceBase64 = base64_encode($nonce);

        $seed = date('c');
        $secretKey = 'css83lpw0bBc5jGw'; // trankey  
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $auth = '
        {
            "auth": {
                "login": "743eeee7c453349c31c09d82714f5c83",
                "tranKey": "'.$tranKey.'",
                "nonce": "'.$nonceBase64.'",
                "seed": "'.$seed.'"
            }
        }
        ';

        $req = json_decode($auth,true);

        $req["auth"]["tranKey"] = $tranKey;
        $req["auth"]["nonce"] = $nonceBase64;
        $req["auth"]["seed"] = $seed;

        $client = new Client([
            // You can set any number of default request options.
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req,
            'timeout' => 600,
        ]);
        
        $response2 = $client->request('post', 'https://test.placetopay.com/redirection/api/session/'.$requestId, [ 'body' => json_encode($req) ]);

        $body2 = json_decode($response2->getBody()->getContents(), true);
        $cambior = json_encode($body2['status']['status'], true);

        return view('layouts/notificationtwo');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsNotificaciontwo  $modelsNotificaciontwo
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsNotificaciontwo $modelsNotificaciontwo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsNotificaciontwo  $modelsNotificaciontwo
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsNotificaciontwo $modelsNotificaciontwo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsNotificaciontwo  $modelsNotificaciontwo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsNotificaciontwo $modelsNotificaciontwo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsNotificaciontwo  $modelsNotificaciontwo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsNotificaciontwo $modelsNotificaciontwo)
    {
        //
    }
}
