<?php

namespace App\Http\Controllers;

use Mail;
use App\User;
use App\ModelsMail;
use Illuminate\Http\Request;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $name = 'John Retavisca';
        $mail = 'john.retavisca@connect-sos.com';
        $passOriginal = 'Cenco2020*';
        Mail::send('mail', ['pass'=>$passOriginal, 'name'=>$name, 'mail'=>$mail], function($message) use ($name, $mail) {
            $message->from('seguroscencosud@connect-sos.com', 'Programa de seguros cencosud');
            $message->to($mail, $name);
            $message->subject('Bienvenido al Programa de seguros cencosud');
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users = User::all();
        return $users;
        /*foreach ($users as $user ) {
            Mail::send('mail', ['pass'=>$passOriginal, 'name'=>$name, 'mail'=>$mail], function($message) use ($name, $mail) {
                $message->from('administrado@gidacademy.com', 'Administrador GIDACADEMY');
                $message->to($mail, $name);
                $message->subject('Bienvenido a GIDAcademy');
            });
        }*/
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsMail  $modelsMail
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsMail $modelsMail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsMail  $modelsMail
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsMail $modelsMail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsMail  $modelsMail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsMail $modelsMail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsMail  $modelsMail
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsMail $modelsMail)
    {
        //
    }
}
