<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Entrada;
use Illuminate\Http\Request;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function store(Request $request)
    {

    $validator= Validator::make($request->all(),[
        'usuario' => 'required|min:6|max:70',
        'contenido' => 'required|min:5|max:255',

    ]);
}


}

class TaskController extends Controller
{

    public function service(Request $request)
    {
        return view('layouts/service');
    }

}