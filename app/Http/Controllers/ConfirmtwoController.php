<?php

namespace App\Http\Controllers;

use App\Models\Data_pay;
use App\Models\Peticion;
use App\Models\Confirmtwo;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ConfirmtwoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = session('product');
        $documentsession = session('documentsession');
        $referencia = session('referencia');

        $peticions = Peticion::where('user_id','1')
                                ->where('documento', $documentsession)
                                ->where('poliza', $product)
                                ->where('referencia', $referencia)
                                ->get('requestID')->last();

        $cambio = json_encode($peticions["requestID"], true);
        $requestId = str_replace('"',"", $cambio);

        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
          } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
          } else {
            $nonce = mt_rand();
          }
          $nonceBase64 = base64_encode($nonce);

        $seed = date('c');
        $secretKey = 'aIMRMh8tpKiU5UGN'; // trankey  PROD aIMRMh8tpKiU5UGN
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));
        //prod: login 825e1b4e038b3c380aefc33181bed268
        $auth = '
        {
            "auth": {
                "login": "825e1b4e038b3c380aefc33181bed268", 
                "tranKey": "'.$tranKey.'",
                "nonce": "'.$nonceBase64.'",
                "seed": "'.$seed.'"
            }
        }
        ';

        $req = json_decode($auth,true);

        $req["auth"]["tranKey"] = $tranKey;
        $req["auth"]["nonce"] = $nonceBase64;
        $req["auth"]["seed"] = $seed;

        $client = new Client([
            // You can set any number of default request options.
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req,
            
            'timeout' => 600,
        ]);
        
        $response2 = $client->request('post', 'https://secure.placetopay.com/redirection/api/session/'.$requestId, [ 'body' => json_encode($req) ]);

        $body2 = json_decode($response2->getBody()->getContents(), true);
        $status_resp = json_encode($body2['status']['status'], true);
        $status = str_replace('"',"", $status_resp);

        $value = json_encode($body2['request']['payment']['amount']['total'], true);
        $celphone_resp = json_encode($body2['request']['buyer']['mobile'], true);
        $celphone = str_replace('"',"", $celphone_resp);

        $description_resp = json_encode($body2['request']['payment']['description'], true);
        $description = str_replace('"',"", $description_resp);

        $date = date("d-m-Y");

        $refer = json_encode($body2['request']['payment']['reference'], true);

        /* $documento = json_encode($body2["request"]["buyer"]["document"], true);
        $tipo_documento = json_encode($body2["request"]["buyer"]["documentType"], true);
        $nombre = json_encode($body2["request"]["buyer"]["name"], true);
        $apellido = json_encode($body2["request"]["buyer"]["name"], true);
        $email = json_encode($body2["request"]["buyer"]["email"], true);
        $celular = json_encode($body2["request"]["buyer"]["mobile"], true);
        $refere = json_encode($body2["request"]["payment"]["reference"], true);
        $descripcion = json_encode($body2["request"]["payment"]["description"], true);
        $currency = json_encode($body2["request"]["payment"]["amount"]["currency"], true);
        $total = json_encode($body2["request"]["payment"]["amount"]["total"], true);
        $valor_total = "$".+json_encode($body2["request"]["payment"]["amount"]["total"], true);
        $mensaje = json_encode($body2['status']['message'], true);
        

        $numero_transaccion = json_encode($body2['payment'][0]['internalReference'], true);
        $entidad_recaudadora = json_encode($body2['payment'][0]['issuerName'], true);
        $metodo_pago = json_encode($body2['payment'][0]['paymentMethodName'], true);
        $autorizacion = json_encode($body2['payment'][0]['authorization'], true);*/
        

        if ($status == "APPROVED"){
            $mensaje="TU PAGO HA SIDO EXITOSO";
            $mensaje2="";
            $response="Transacción Aprobada";
            $imagen="https://connect-static-files.s3.amazonaws.com/pagos/Check-Icon.png";

            $peticions = Peticion::where('requestId',$requestId)->update(['estado_transaccion' => "APPROVED"]);
            
        }
        elseif ($status == "REJECTED"){
            $mensaje="TU PAGO HA SIDO RECHAZADO";
            $mensaje2="";
            $response="Transacción Rechazada";
            $imagen="https://connect-static-files.s3.amazonaws.com/pagos/Rechazado.png";

            $peticions = Peticion::where('requestId',$requestId)->update(['estado_transaccion' => "RECHAZADO"]);
            
        }
        elseif ($status == "PENDING"){
            $mensaje="TU PAGO SE ENCUENTRA EN ESTADO PENDIENTE";
            $mensaje2="En este momento su pago presenta un proceso de pago cuya transacción se encuentra PENDIENTE de recibir
            confirmación por parte de su entidad financiera, por favor espere unos minutos y vuelva
            a consultar más tarde para verificar si su pago fue confirmado de forma exitosa. Si
            desea mayor información sobre el estado actual de su operación puede comunicarse a
            nuestras líneas de atención al cliente 6511102 en Bogota o 3057342036 a nivel nacional.";
            $response="Transacción Pendiente";
            $imagen="https://connect-static-files.s3.amazonaws.com/pagos/Pending.png";
            $peticions = Peticion::where('requestId',$requestId)->update(['estado_transaccion' => "PENDING_PAY"]);
        }

        return view('layouts/confirmtwo', compact('mensaje2','mensaje','imagen', 'value', 'celphone', 'description', 'date', 'refer', 'response'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsConfirmtwo  $modelsConfirmtwo
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsConfirmtwo $modelsConfirmtwo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsConfirmtwo  $modelsConfirmtwo
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsConfirmtwo $modelsConfirmtwo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsConfirmtwo  $modelsConfirmtwo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsConfirmtwo $modelsConfirmtwo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsConfirmtwo  $modelsConfirmtwo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsConfirmtwo $modelsConfirmtwo)
    {
        //
    }
}
