<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $user = DB::table('users')
        ->where('id',auth()->id())
        ->get();

        $array = array();
        foreach($user as $t){
            $array[] = $t->document;
        }
        $cambio = json_encode($array);
        $cambio1 = str_replace("[","", $cambio);
        $cambio2 = str_replace("]","", $cambio1);
        $document = str_replace('"',"", $cambio2);

        $clientes = DB::table('clientes')
        ->where('nitcliente', $document)
        ->get();

        $clientess = array();
        foreach($clientes as $t){
            $clientess[] = $t;
        }

        //return($array2);
        return view('home', compact('clientess','document'));
    }
}
