<?php

namespace App\Http\Controllers;

use App\ModelsCreate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CreateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts/create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pol = $request->input('poliza');
        $doc = $request->input('documentoHidden');
        $product = '';
        $polDef = '';

        if ($pol == 'AU ESTADO CENCOSUD') {
            $product = 'AUTOMOVILES';
            $polDef = 'AUTOS ESTADO';
        } else if ($pol == 'AU SURA CENCOSUD') {
            $product = 'AUTOMOVILES';
            $polDef = 'AUTOS SURA';
        } else if ($pol == 'AU LIBERTY') {
            $product = 'AUTOMOVILES';
            $polDef = 'LIBERTY CENCOSUD';
        } else if ($pol == 'AU SOLI CENCOSUD') {
            $product = 'AUTOMOVILES';
            $polDef = 'AUTOS SOLIDARIA';
        } else if ($pol == 'AU AXA CENCOSUD') {
            $product = 'AUTOMOVILES';
            $polDef = 'AUTOS AXA';
        } else if ($pol == 'AU MUNDIAL CENCOSUD') {
            $product = 'AUTOMOVILES';
            $polDef = 'RC MUNDIAL';
        } else if ($pol == 'AP EDUCATIVA') {
            $product = 'ACCIDENTES PERSONALES';
            $polDef = 'AP EDUCATIVA';
        } else if ($pol == 'AUTOS LIVIANOS ANDI') {
            $product = 'ASISTENCIA AUTOMÓVILES';
            $polDef = 'AUTOS LIVIANOS';
        } else if ($pol == 'MOTOS ANDI') {
            $product = 'ASISTENCIA AUTOMÓVILES';
            $polDef = 'MOTOS';
        } else if ($pol == 'MULTIASISTENCIA ANDI') {
            $product = 'ASISTENCIA AUTOMÓVILES';
            $polDef = 'MULTIASISTENCIA';
        } else if ($pol == 'AP BOLSO PROTEGIDO') {
            $product = 'ACCIDENTES PERSONALES';
            $polDef = 'AP BOLSO PROTEGIDO';
        } else if ($pol == 'AP EDAD DORADA') {
            $product = 'ACCIDENTES PERSONALES';
            $polDef = 'AP EDAD DORADA';
        } else if ($pol == 'AP EDUCATIVA') {
            $product = 'ACCIDENTES PERSONALES';
            $polDef = 'AP EDUCATIVA';
        } else if ($pol == 'AP RENTA') {
            $product = 'ACCIDENTES PERSONALES';
            $polDef = 'AP RENTA';
        } else if ($pol == 'AP PASAJEROS') {
            $product = 'ACCIDENTES PERSONALES';
            $polDef = 'AP PASAJEROS';
        } else if ($pol == 'AP SALUD') {
            $product = 'ACCIDENTES PERSONALES';
            $polDef = 'AP SALUD';
        } else if ($pol == 'AP AUTOCONTENIDO') {
            $product = 'ACCIDENTES PERSONALES';
            $polDef = 'AP AUTOCONTENIDO';
        } else if ($pol == 'AP BOLSO PROTEGIDO P') {
            $product = 'ACCIDENTES PERSONALES';
            $polDef = 'AP BOLSO PROTEGIDO P';
        } else if ($pol == 'AP MASCOTAS') {
            $product = 'ACCIDENTES PERSONALES';
            $polDef = 'AP MASCOTAS';
        }

        $user = DB::table('users')
        ->where('id',auth()->id())
        ->get();

        $nombre = array();
        foreach($user as $t){
            $nombre[] = $t->name;
        }
        $cambio = json_encode($nombre);
        $cambio1 = str_replace("[","", $cambio);
        $cambio2 = str_replace("]","", $cambio1);
        $nombreDef = str_replace('"',"", $cambio2);

        $cartera = DB::table('carteras')
        ->where('CODCLI',$doc)
        ->where('NUMPOL',$polDef)
        ->get();

        $carteras = array();
        foreach($cartera as $t){
            $carteras[] = $t;
        }

        //return $carteras;
        return view('layouts/create', compact('carteras','product','nombreDef'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsCreate  $modelsCreate
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsCreate $modelsCreate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsCreate  $modelsCreate
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsCreate $modelsCreate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsCreate  $modelsCreate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsCreate $modelsCreate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsCreate  $modelsCreate
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsCreate $modelsCreate)
    {
        //
    }
}
