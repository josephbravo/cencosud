<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Peticion;
use App\Models\Asistencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class AsistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date = Carbon::now();
        $max = $date->subYears(18)->format('Y-m-d'); // pasar tambien en ADU y Familia
        $min = $date->subYears(47)->format('Y-m-d'); // pasar tambien Familia
        $ocupacions = DB::table('ocupacions')->where('Estado','A')->get();
        $mensaje="";
        $url = "";
        session()->forget('data');
        session()->forget('link_moto');
        session()->forget('link_auto');
        session()->forget('link_multi');

        return view('layouts/asistencia_resp', compact('url','mensaje','max','min','ocupacions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nombre = $request->input('nombre');
        $apellido = $request->input('apellido');
        $email = $request->input('email');
        $producto = $request->input('producto');
    
        if ($producto == "AUTO"){
            $pagar = "1000";
            
            //save  in database

        }
        if ($producto == "MOTO"){
            $pagar = "1000";
            
            //save  in database

        }

        if ($producto == "MULTI"){
            $pagar = "1000";
            //save  in database

        }

        // WEB SERVICES COS - DATAS TO SEND.
        //Pruebas: f24afe$7220e84234+ad41a817a*6b63b7f ... Produccion: f24afe$7&20e/34+ad$$a817a*6333b7f

        $datas = '
        {
            "nombre": "'.$nombre.'",
            "apellido": "'.$apellido.'",
            "documento": "null",
            "correo": "'.$email.'",
            "celular": "null",
            "producto": "'.$producto.'",
            "valor_producto": "'.$pagar.'",
            "token_key" : "f24afe$7&20e/34+ad$$a817a*6333b7f"
            }
        ';
        $req_datas = json_decode($datas,true);

        $client = new Client([
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req_datas,
            'timeout' => 600,
        ]);
        
        //Pruebas: https://app.outsourcingcos.com/cencosudprueba/recibedatos  Producción: https://app.outsourcingcos.com/cencosud/recibedatos
        $response = $client->request('post', 'https://app.outsourcingcos.com/cencosud/recibedatos', ['body' => json_encode($req_datas)]);
        #$body = json_decode($response->getBody()->getContents(), true);
        #print("RESPUESTA COS".var_dump($body));

        /**
        $date = Carbon::now();
        $digitals = new Digital;
        $digitals->documento=$documento;
        $digitals->nombres=$nombre;
        $digitals->apellidos=$apellido;
        $digitals->celular=$celular;
        $digitals->email=$email;
        $digitals->producto=$producto;
        $digitals->poliza=$pagar;
        $digitals->created_at=$date->format('d-m-Y H:i:s');
        $digitals->updated_at=$date->format('d-m-Y H:i:s');
        $digitals->save();
        if ($producto == "ADU"){
            return redirect()->away('https://pagoscencosud.connect-sos.com/adu#openModalThree');
        }
        else if ($producto == "MST"){
            return redirect()->away('https://pagoscencosud.connect-sos.com/mascota#openModalThree');
        }
        else if ($producto == "FML"){
            return redirect()->away('https://pagoscencosud.connect-sos.com/familia#openModalThree');
        }
        */
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsAsistencia  $modelsAsistencia
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsAsistencia $modelsAsistencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsAsistencia  $modelsAsistencia
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsAsistencia $modelsAsistencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsAsistencia  $modelsAsistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsAsistencia $modelsAsistencia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsAsistencia  $modelsAsistencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsAsistencia $modelsAsistencia)
    {
        //
    }
}
