<?php

namespace App\Http\Controllers;

use App\ModelsControl_Pay;
use Illuminate\Http\Request;

class Control_PayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $json = '';
        return view('layouts/control_pay', compact('json'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsControl_Pay  $modelsControl_Pay
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsControl_Pay $modelsControl_Pay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsControl_Pay  $modelsControl_Pay
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsControl_Pay $modelsControl_Pay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsControl_Pay  $modelsControl_Pay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsControl_Pay $modelsControl_Pay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsControl_Pay  $modelsControl_Pay
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsControl_Pay $modelsControl_Pay)
    {
        //
    }
}
