<?php

namespace App\Http\Controllers;

use App\ModelsQuestiontwo;
use Illuminate\Http\Request;

class QuestiontwoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsQuestiontwo  $modelsQuestiontwo
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsQuestiontwo $modelsQuestiontwo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsQuestiontwo  $modelsQuestiontwo
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsQuestiontwo $modelsQuestiontwo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsQuestiontwo  $modelsQuestiontwo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsQuestiontwo $modelsQuestiontwo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsQuestiontwo  $modelsQuestiontwo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsQuestiontwo $modelsQuestiontwo)
    {
        //
    }
}
