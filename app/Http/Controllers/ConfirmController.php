<?php

namespace App\Http\Controllers;

use App\Models\Data_pay;
use App\Models\Peticion;
use App\Models\Confirm;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ConfirmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requestId = session('requestid_peticion');

        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
          } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
          } else {
            $nonce = mt_rand();
          }
          $nonceBase64 = base64_encode($nonce);

        $seed = date('c');
        $secretKey = 'aIMRMh8tpKiU5UGN'; // trankey  
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $auth = '
        {
            "auth": {
                "login": "825e1b4e038b3c380aefc33181bed268",
                "tranKey": "'.$tranKey.'",
                "nonce": "'.$nonceBase64.'",
                "seed": "'.$seed.'"
            }
        }
        ';

        $req = json_decode($auth,true);

        $req["auth"]["tranKey"] = $tranKey;
        $req["auth"]["nonce"] = $nonceBase64;
        $req["auth"]["seed"] = $seed;

        $client = new Client([
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req,
            
            'timeout' => 600,
        ]);
        
        $response2 = $client->request('post', 'https://secure.placetopay.com/redirection/api/session/'.$requestId, [ 'body' => json_encode($req) ]);

        $body2 = json_decode($response2->getBody()->getContents(), true);
        
        $status_resp = json_encode($body2['status']['status'], true);
        $status = str_replace('"',"", $status_resp);

        $value = json_encode($body2['request']['payment']['amount']['total'], true);
        $celphone_resp = json_encode($body2['request']['buyer']['mobile'], true);
        $celphone = str_replace('"',"", $celphone_resp);

        $description_resp = json_encode($body2['request']['payment']['description'], true);
        $description = str_replace('"',"", $description_resp);

        $date = date("d-m-Y");

        $refer = json_encode($body2['request']['payment']['reference'], true);

        if ($status == "APPROVED"){
            $mensaje="TU PAGO HA SIDO EXITOSO";
            $response="Transacción Aprobada";
            $imagen="https://connect-static-files.s3.amazonaws.com/pagos/Check-Icon.png";

            $peticions = Peticion::where('requestId',$requestId)->update(['estado_transaccion' => "APPROVED"]);
            
        }
        elseif ($status == "REJECTED"){
            $mensaje="TU PAGO HA SIDO RECHAZADO";
            $response="Transacción Rechazada";
            $imagen="https://connect-static-files.s3.amazonaws.com/pagos/Rechazado.png";

            $peticions = Peticion::where('requestId',$requestId)->update(['estado_transaccion' => "RECHAZADO"]);
            
        }
         
        elseif ($status == "PENDING") {
            $mensaje="TU PAGO SE ENCUENTRA EN ESTADO PENDIENTE";
            $response="Transacción Pendiente";
            $imagen="https://connect-static-files.s3.amazonaws.com/pagos/Pending.png";
            $peticions = Peticion::where('requestId',$requestId)->update(['estado_transaccion' => "PENDING_PAY"]);
        }

        return view('layouts/confirm', compact('mensaje','imagen', 'value', 'celphone', 'description', 'date', 'refer', 'response'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsConfirm  $modelsConfirm
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsConfirm $modelsConfirm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsConfirm  $modelsConfirm
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsConfirm $modelsConfirm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsConfirm  $modelsConfirm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsConfirm $modelsConfirm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsConfirm  $modelsConfirm
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsConfirm $modelsConfirm)
    {
        //
    }
}
