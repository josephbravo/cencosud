<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Peticion;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class PeticionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $date = Carbon::now();
        $doc = $request->input('documentoHidden');
        $pol = $request->input('polizaHidden');
        $prod = $request->input('productoHidden');
        $tot = $request->input('tot');
        $prima = $request->input('prima');
        $iva = $request->input('iva');
        $total = str_replace(".","", $tot);
        $total = str_replace(",","", $total);
        $json = '';

        $tot = str_replace(",","", $tot);

        if ($pol == 'AUTOS SURA' || $pol == 'AUTOS ESTADO' || $pol == 'AUTOS SOLIDARIA' || $pol == 'AUTOS AXA' || $pol == 'RC MUNDIAL'){
          $poliza = 'AUTOS';
        } else {
          $poliza = 'AP';
        } 
        
        $referencia = $poliza.$doc.$date->format('dmYHis');

        $user = DB::table('users')
        ->where('id',auth()->id())
        ->get();

        $nombre = array();
        foreach($user as $t){
            $nombre[] = $t->name;
        }
        $cambio = json_encode($nombre);
        $cambio1 = str_replace("[","", $cambio);
        $cambio2 = str_replace("]","", $cambio1);
        $nombreDef = str_replace('"',"", $cambio2);

        $apellido = array();
        foreach($user as $t){
            $apellido[] = $t->lastname;
        }
        $cambio = json_encode($apellido);
        $cambio1 = str_replace("[","", $cambio);
        $cambio2 = str_replace("]","", $cambio1);
        $apellidoDef = str_replace('"',"", $cambio2);

        $celular = array();
        foreach($user as $t){
            $celular[] = $t->celular;
        }
        $cambio = json_encode($celular);
        $cambio1 = str_replace("[","", $cambio);
        $cambio2 = str_replace("]","", $cambio1);
        $celularDef = str_replace('"',"", $cambio2);

        $email = array();
        foreach($user as $t){
            $email[] = $t->email;
        }
        $cambio = json_encode($email);
        $cambio1 = str_replace("[","", $cambio);
        $cambio2 = str_replace("]","", $cambio1);
        $emailDef = str_replace('"',"", $cambio2);

        $doc = intval($doc);
        
        //return intval($doc).$pol.$prod;
        return view('layouts/peticion', compact('referencia','doc','json','emailDef','celularDef','apellidoDef','nombreDef','total','iva','prima','tot','prod','pol'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $date = Carbon::now();
        $tomorrow = $date->add(1, 'day');
  
        $documento = $request->input('documento');
        $nombres = $request->input('nombres');
        $apellidos = $request->input('apellidos');
        $email = $request->input('email');
        $poliza = $request->input('poliza');
        $celular = $request->input('celular');
        $factura = $request->input('factura');
        $prima = $request->input('prima');
        $iva = $request->input('iva');
        $pagar = $request->input('pagar');
        $referencia = $request->input('referencia');

        $status_pay = Peticion::where('documento',$documento)->where('user_id',auth()->id())
                                ->get('estado_transaccion')->last();
        $status= json_encode($status_pay["estado_transaccion"], true);
        $status_pay = str_replace('"',"", $status);

        if ($status_pay == "PENDING_PAY") {
            $state = Peticion::where('documento',$documento)
                                ->where('user_id',auth()->id())
                                ->get('referencia')->last();
            $ref= json_encode($state["referencia"], true);
            $refer = str_replace('"',"", $ref);
            return view('layouts/control_pay', compact('refer'));
            }

        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
            } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
            } else {
            $nonce = mt_rand();
            }
            $nonceBase64 = base64_encode($nonce);
          
        $seed = date('c');
        $secretKey = 'aIMRMh8tpKiU5UGN'; // trankey
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));
        $req["auth"]["seed"] = $seed;
        $req["auth"]["nonce"] = $nonceBase64;
        $req["auth"]["tranKey"] = $tranKey;
        $req["expiration"] = $tomorrow;

        $json = '{
          "buyer": {
            "name": "'.$nombres.'",
            "surname": "'.$apellidos.'",
            "email": "'.$email.'",
            "document": "'.$documento.'",
            "documentType": "CC",
            "mobile": "'.$celular.'"
          },
          "payment": {
            "reference": "'.$referencia.'",
            "description": "Pago a producto seguros Cencosud",
            "amount": {
              "taxes": [
                  {
                      "kind": "valueAddedTax",
                      "amount": "'.$iva.'"
                  }
                ],
                "details": [  
                    {
                        "kind": "subtotal",
                        "amount": "'.$prima.'"
                    }
                ],
              "currency": "COP",
              "total": "'.$pagar.'"
              }
          },
          "expiration": "'.$tomorrow.'",
          "ipAddress": "201.184.3.213",
          "returnUrl": "https://pagoscencosud.connect-sos.com/confirm",
          "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36",
          "paymentMethod": "",
          "skipResult": "True",
          
          "auth": {
            "login": "825e1b4e038b3c380aefc33181bed268",
            "tranKey": "'.$tranKey.'",
            "nonce": "'.$nonceBase64.'",
            "seed": "'.$seed.'"
           }
          }';

        $req = json_decode($json,true);

        $client = new Client([
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req,
            'timeout' => 600,
        ]);

        $response = $client->request('post', 'https://secure.placetopay.com/redirection/api/session/', [ 'body' => json_encode($req) ]);

        $response = json_decode($response->getBody()->getContents(), true);

        $response_encode = json_encode($response["processUrl"], true);
        
        $cambio1 = str_replace("\/\/","//", $response_encode);
        $cambio2 = str_replace("\/","/", $cambio1);
        $cambio3 = str_replace('"',"", $cambio2);
        $url = $cambio3;

        $referen = $poliza.$documento.$date->format('dmYHis');

        $requestId = json_encode($response["requestId"], true);
        session(['requestid_peticion' => $requestId]);

        $peticiones = new Peticion;
        $peticiones->requestId=$requestId;
        $peticiones->documento=$documento;
        $peticiones->nombres=$nombres;
        $peticiones->apellidos=$apellidos;
        $peticiones->email=$email;
        $peticiones->poliza=$poliza;
        $peticiones->celular=$celular;
        $peticiones->factura=$factura;
        $peticiones->pagar=$pagar;
        $peticiones->created_at=$date;
        $peticiones->updated_at=$date;
        $peticiones->url=$url;
        $peticiones->user_id = "1";
        $peticiones->estado_transaccion="PENDING";
        $peticiones->referencia=$referen;

        $peticiones->save();

        return redirect()->away($url);
        //return view('layouts/peticion', compact('json'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsPeticion  $modelsPeticion
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsPeticion $modelsPeticion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsPeticion  $modelsPeticion
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsPeticion $modelsPeticion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsPeticion  $modelsPeticion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsPeticion $modelsPeticion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsPeticion  $modelsPeticion
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsPeticion $modelsPeticion)
    {
        //
    }
}