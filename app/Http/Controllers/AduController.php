<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\ModelsAdu;
use App\Models\Peticion;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class AduController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $date = Carbon::now();
      $max = $date->subYears(18)->format('Y-m-d'); // pasar tambien en ADU y Familia
      $min = $date->subYears(47)->format('Y-m-d'); // pasar tambien Familia
      $ocupacions = DB::table('ocupacions')->where('Estado','A')->get();
      $mensaje="";
      $url = "";

      //return view('layouts/adu', compact('url','mensaje','max','min','ocupacions'));
      return view('layouts/adu_resp', compact('url','mensaje','max','min','ocupacions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = Carbon::now();
        $tomorrow = $date->subHour(4);
        $minutes = $date->subMinute(50);
  
        $documento = $request->input('documento');
        $tipdocumento = $request->input('tipdocumento');
        $fechanto = $request->input('date');
        $genero = $request->input('genero');
        $direccion = $request->input('direccion');
        $regimen = $request->input('regimen');
        $afiliacion = $request->input('afiliacion');
        $eps = $request->input('eps');
        $nombre = $request->input('nombre');
        $apellido = $request->input('apellido');
        $email = $request->input('email');
        $celular = $request->input('celular');
        $pagar = $request->input('valor');
        $profesion = $request->input('profesion');
        $product = "ADU";
        

        session(['documentsession' => $documento]);
        session(['product' => $product]);

        $status_pay = Peticion::where('documento',$documento)->get('estado_transaccion')->last();
        $status= json_encode($status_pay["estado_transaccion"], true);
        $status_pay = str_replace('"',"", $status);

        if ($status_pay == "PENDING_PAY") {
            return view('layouts/control_pay');
        }


        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
          } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
          } else {
            $nonce = mt_rand();
          }
          $nonceBase64 = base64_encode($nonce);

        $seed = date('c');
        $secretKey = 'aIMRMh8tpKiU5UGN'; // trankey  
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $req["auth"]["seed"] = $seed;
        $req["auth"]["nonce"] = $nonceBase64;
        $req["auth"]["tranKey"] = $tranKey;
        $req["expiration"] = $tomorrow;

        $json = '{
            "buyer": {
              "name": "'.$nombre.'",
              "surname": "'.$apellido.'",
              "email": "'.$email.'",
              "document": "'.$documento.'",
              "documentType": "'.$tipdocumento.'",
              "mobile": "'.$celular.'"
            },
            "payment": {
              "reference": "'.$product.$documento.$date->format('dmYHis').'",
              "description": "Pago a producto seguros Cencosud",
              "amount": {
                "currency": "COP",
                "total": '.$pagar.'
              }
            },
            "expiration": "'.$minutes.'",
            "ipAddress": "201.184.3.213",
            "returnUrl": "https://pagoscencosud.connect-sos.com/confirmtwo",
            "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36",
            "paymentMethod": "",
            "skipResult": "True",
            
            "auth": {
              "login": "825e1b4e038b3c380aefc33181bed268",
              "tranKey": "'.$tranKey.'",
              "nonce": "'.$nonceBase64.'",
              "seed": "'.$seed.'"
             }
            }';

        $req = json_decode($json,true);

        $client = new Client([
            // You can set any number of default request options.
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req,
            
            'timeout' => 600,
        ]);

        $response = $client->request('post', 'https://secure.placetopay.com/redirection/api/session/', [ 'body' => json_encode($req) ]);

        $response = json_decode($response->getBody()->getContents(), true);

        $response_encode = json_encode($response["processUrl"], true);
        $requestId = json_encode($response["requestId"], true);
        $cambio1 = str_replace("\/\/","//", $response_encode);
        $cambio2 = str_replace("\/","/", $cambio1);
        $cambio3 = str_replace('"',"", $cambio2);

        $url = $cambio3;
        $user_id = "1";

        $peticiones = new Peticion;
        $peticiones->estado_transaccion="PENDING";
        $peticiones->requestId=$requestId;
        $peticiones->documento=$documento;
        $peticiones->nombres=$nombre;
        $peticiones->apellidos=$apellido;
        $peticiones->genero=$genero;
        $peticiones->direccion=$direccion;
        $peticiones->profesion=$profesion;
        $peticiones->email=$email;
        $peticiones->poliza=$product;
        $peticiones->celular=$celular;
        $peticiones->factura=$product;
        $peticiones->pagar=$pagar;
        $peticiones->created_at=$date->format('d-m-Y H:i:s');
        $peticiones->updated_at=$date->format('d-m-Y H:i:s');
        $peticiones->url=$url;
        $peticiones->referencia=$product.$documento.$date->format('dmYHis');
        $peticiones->eps=$eps;
        $peticiones->regimen=$regimen;
        $peticiones->tipoafiliacion=$afiliacion;
        $peticiones->tipodocumento=$tipdocumento;
        $peticiones->fechanacimiento=$fechanto;
        $peticiones->user_id='1';
        $peticiones->save();

        $referencia = $product.$documento.$date->format('dmYHis');
        session(['referencia' => $referencia]);

        return redirect()->away($url);

        /**$json = $valor;
        return view('layouts/adu', compact('json')); */
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsAdu  $modelsAdu
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsAdu $modelsAdu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsAdu  $modelsAdu
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsAdu $modelsAdu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsAdu  $modelsAdu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsAdu $modelsAdu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsAdu  $modelsAdu
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsAdu $modelsAdu)
    {
        //
    }
}
