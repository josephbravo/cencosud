<?php

namespace App\Http\Controllers;

use App\ModelsRiesgo;
use Illuminate\Http\Request;

class RiesgoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $json = '';
        return view('layouts/riesgo', compact('json'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsRiesgo  $modelsRiesgo
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsRiesgo $modelsRiesgo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsRiesgo  $modelsRiesgo
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsRiesgo $modelsRiesgo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsRiesgo  $modelsRiesgo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsRiesgo $modelsRiesgo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsRiesgo  $modelsRiesgo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsRiesgo $modelsRiesgo)
    {
        //
    }
}
