<?php

namespace App\Http\Controllers;

use App\ModelsQuestion;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $json = '';
        return view('layouts/question', compact('json'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsQuestion  $modelsQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsQuestion $modelsQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsQuestion  $modelsQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsQuestion $modelsQuestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsQuestion  $modelsQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsQuestion $modelsQuestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsQuestion  $modelsQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsQuestion $modelsQuestion)
    {
        //
    }
}
