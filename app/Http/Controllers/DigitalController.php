<?php

namespace App\Http\Controllers;

use App\Models\Digital;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use GuzzleHttp\Client;

class DigitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $documento = $request->input('documento');
        $tipdocumento = $request->input('tipdocumento');
        $genero = $request->input('genero');
        $producto = $request->input('producto');
        $nombre = $request->input('nombre');
        $apellido = $request->input('apellido');
        $email = $request->input('email');
        $celular = $request->input('telefono');
        $pagar = $request->input('valor');

        $date = Carbon::now();

        $digitals = new Digital;
        
        $digitals->documento=$documento;
        $digitals->nombres=$nombre;
        $digitals->apellidos=$apellido;
        $digitals->celular=$celular;
        $digitals->email=$email;
        $digitals->producto=$producto;
        $digitals->poliza=$pagar;
        $digitals->created_at=$date->format('d-m-Y H:i:s');
        $digitals->updated_at=$date->format('d-m-Y H:i:s');

        if ($producto == "ADU"){
            $producto_select = "Adu";
        } else if ($producto == "MST"){
            $producto_select = "Mascotas";
        } else if ($producto == "FML"){
            $producto_select = "Familia";
        }
        
        $digitals->save();

        // WEB SERVICES COS - DATAS TO SEND.
        //Pruebas: f24afe$7220e84234+ad41a817a*6b63b7f ... Produccion: f24afe$7&20e/34+ad$$a817a*6333b7f
        $datas = '
        {
            "nombre": "'.$nombre.'",
            "apellido": "'.$apellido.'",
            "documento": "'.$documento.'",
            "correo": "'.$email.'",
            "celular": "'.$celular.'",
            "producto": "ASISTENCIAS",
            "valor_producto": "'.$pagar.'",
            "linea":"'.$producto_select.'",
            "token_key" : "f24afe$7&20e/34+ad$$a817a*6333b7f"
            }
        ';

        $req_datas = json_decode($datas,true);

        $client = new Client([
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req_datas,
            'timeout' => 600,
            'verify' => false,
        ]);
        
        //Pruebas: https://app.outsourcingcos.com/cencosudprueba/recibedatos  Producción: https://app.outsourcingcos.com/cencosud/recibedatos
        $response = $client->request('post', 'https://app.outsourcingcos.com/cencosud/recibedatos', ['body' => json_encode($req_datas)]);
        #$body = json_decode($response->getBody()->getContents(), true);
        #print("RESPUESTA COS".var_dump($body));

        if ($producto == "ADU"){
            return redirect()->away('https://pagoscencosud.connect-sos.com/adu#openModalThree');
        }
        else if ($producto == "MST"){
            return redirect()->away('https://pagoscencosud.connect-sos.com/mascota#openModalThree');
        }
        else if ($producto == "FML"){
            return redirect()->away('https://pagoscencosud.connect-sos.com/familia#openModalThree');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_refer(Request $request)
    {
        if($request->ajax()){
            $login = '
            {
                "login": {
                    "user": "CENCOSUD",
                    "pass": "F3MccICphHkk/vnXyzbeEOxZR09YMbvfKx/uCdyHr3LWudzCXFjzzna+/aVb8GZIUBNF8L2kFDaFKeBV+yyq9Q==",
                    "country": 7,
                    "remote_addr": ""
                }
                }
            ';

            $req_login = json_decode($login,true);

            $client = new Client([
                'header'  => 'Content-type: application/json',
                'method'=>'POST',
                'json'=>$req_login,
                'timeout' => 600,
                'verify' => false,
            ]);

            $resp_content = $client->request('post', 'https://api.mapfre.com/srv/warranties/login', ['body' => json_encode($req_login)],['verify' => false]);
            $resp_content = json_decode($resp_content->getBody()->getContents(), true);
            //print("RESPUESTA".json_encode($resp_content));

            $token = json_encode($resp_content["root"]["token"], true);
            $cambio1 = str_replace("\/\/","//", $token);
            $cambio2 = str_replace("\/","/", $cambio1);
            $token_id = str_replace('"',"", $cambio2);
            //dd("RESPUESTA".$token_id);
            //print("RESPUESTA".$token_id);

            session(['token_id' => $token_id]);


            $getXMLProduct =' {
                "getXMLProduct": {
                "token": "'.$token_id.'", 
                "idRegProduct": "749",
                "deliveryDate": "2021-03-19",
                "terms": -1
                }
            }';

            $req_getXMLProduct = json_decode($getXMLProduct,true);

            $client = new Client([
                'header'  => 'Content-type: application/json',
                'method'=>'POST',
                'json'=>$req_getXMLProduct,
                'timeout' => 600,
                'verify' => false,
            ]);

            $resp_content2 = $client->request('post', 'https://apisb.mapfre.com/srv/warranties/getXMLProduct', ['body' => json_encode($req_getXMLProduct)], ['verify' => false]);
            $resp_content2 = json_decode($resp_content2->getBody()->getContents(), true);
            
            //$option = json_encode($resp_content2["root"]["riskData"]["cmbMarca"]["option"], true);
            //$cambio_option = str_replace("\/\/","//", $option);
            //$cambio_marca = str_replace("\/","/", $cambio_option);
            //$marca = str_replace('"',"", $cambio_marca);
            //dd("RESPUESTA".$marca);

            $marca = json_encode($resp_content2["root"]["riskData"]["cmbMarca"]["option"], true);

            $marca_conf='{"option":'.$marca.'}';

            return $marca_conf;
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ajax(Request $request)
    {   
        if($request->ajax()){

        $token_id = session('token_id');

        $getOptions = '
        {
            "getOptions": {
                "token": "'.$token_id.'",
                "idHTMLField": "cmbModelo",
                "idReg": "'.$request->input('idmarca').'",
                "idRegProduct": "-1"
            }
            }
        ';
        
        session(['idmarca_auto' => $request->input('idmarca')]);
        $req_getOptions = json_decode($getOptions,true);

        $client = new Client([
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req_getOptions,
            'timeout' => 600,
            'verify' => false,
        ]);

        $resp_content = $client->request('post', 'https://apisb.mapfre.com/srv/warranties/getOptions', ['body' => json_encode($req_getOptions)], ['verify' => false]);
        $resp_content = json_decode($resp_content->getBody()->getContents(), true);
        //print("RESPUESTA".json_encode($resp_content));
        $option = json_encode($resp_content["root"], true);
        //print("RESPUESTA".json_encode($resp_content));
        
        //return response()->json(["Marca id select" => $request->input('idmarca')]);
        //return $request->input('idmarca');
        return $option;

        }
    }
        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cos_andi(Request $request)
    {
        $nombre = $request->input('nombre');
        $apellido = $request->input('apellido');
        $celular = $request->input('celular');
        $email = $request->input('email');
        $documento = $request->input('documento');
        $producto = "ASISTENCIAS";
        $producto_cliente = $request->input('producto');
        $pagar = $request->input('valor');

        $date = Carbon::now();

        $digitals = new Digital;
        
        $digitals->documento=$documento;
        $digitals->nombres=$nombre;
        $digitals->apellidos=$apellido;
        $digitals->celular=$celular;
        $digitals->email=$email;
        $digitals->producto=$producto_cliente;
        $digitals->poliza=$pagar;
        $digitals->created_at=$date->format('d-m-Y H:i:s');
        $digitals->updated_at=$date->format('d-m-Y H:i:s');
        
        $digitals->save();

        // WEB SERVICES COS - DATAS TO SEND.
        //Pruebas: f24afe$7220e84234+ad41a817a*6b63b7f ... Produccion: f24afe$7&20e/34+ad$$a817a*6333b7f
        $datas = '
        {
            "nombre": "'.$nombre.'",
            "apellido": "'.$apellido.'",
            "documento": "'.$documento.'",
            "correo": "'.$email.'",
            "celular": "'.$celular.'",
            "producto": "'.$producto.'",
            "linea":"'.$producto_cliente.'",
            "valor_producto": "'.$pagar.'",
            "token_key" : "f24afe$7&20e/34+ad$$a817a*6333b7f"
            }
        ';

        $req_datas = json_decode($datas,true);

        $client = new Client([
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req_datas,
            'timeout' => 600,
            'verify' => false,
        ]);
        
        //Pruebas: https://app.outsourcingcos.com/cencosudprueba/recibedatos  Producción: https://app.outsourcingcos.com/cencosud/recibedatos
        $response = $client->request('post', 'https://app.outsourcingcos.com/cencosud/recibedatos', ['body' => json_encode($req_datas)]);
        #$body = json_decode($response->getBody()->getContents(), true);
        #print("RESPUESTA COS".var_dump($body));
        //PRO: https://pagoscencosud.connect-sos.com/auto#openModalThree  Test: http://127.0.0.1:8000/auto#openModalThree
        //PRO: https://pagoscencosud.connect-sos.com/moto#openModalThree  Test: http://127.0.0.1:8000/moto#openModalThree
        //PRO: https://pagoscencosud.connect-sos.com/multiasistencia#openModalThree  Test: http://127.0.0.1:8000/multiasistencia#openModalThree

        if ($producto_cliente == "AUTO"){
            return redirect()->away('https://pagoscencosud.connect-sos.com/auto#openModalThree');
        }
        else if ($producto_cliente == "MOTO"){
            return redirect()->away('https://pagoscencosud.connect-sos.com/moto#openModalThree');
        }
        else if ($producto_cliente == "MULTIASISTENCIAS"){
            return redirect()->away('https://pagoscencosud.connect-sos.com/multiasistencia#openModalThree');
        }
        
    }

        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cos_andi_home(Request $request)
    {
        $nombre = $request->input('nombre');
        $apellido = $request->input('apellido');
        $celular = $request->input('celular');
        $email = "-";
        $producto = "ASISTENCIAS";
        $pagar = $request->input('valor');
        $documento = "-";
        
        $date = Carbon::now();

        $digitals = new Digital;
        
        $digitals->documento=$documento;
        $digitals->nombres=$nombre;
        $digitals->apellidos=$apellido;
        $digitals->celular=$celular;
        $digitals->email=$email;
        $digitals->producto="$producto";
        $digitals->poliza=$pagar;
        $digitals->created_at=$date->format('d-m-Y H:i:s');
        $digitals->updated_at=$date->format('d-m-Y H:i:s');
        
        $digitals->save();

        // WEB SERVICES COS - DATAS TO SEND.
        //Pruebas: f24afe$7220e84234+ad41a817a*6b63b7f ... Produccion: f24afe$7&20e/34+ad$$a817a*6333b7f
        $datas = '
        {
            "nombre": "'.$nombre.'",
            "apellido": "'.$apellido.'",
            "documento": "'.$documento.'",
            "correo": "'.$email.'",
            "celular": "'.$celular.'",
            "producto": "'.$producto.'",
            "valor_producto": "'.$pagar.'",
            "token_key" : "f24afe$7&20e/34+ad$$a817a*6333b7f"
            }
        ';

        $req_datas = json_decode($datas,true);

        $client = new Client([
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req_datas,
            'timeout' => 600,
            'verify' => false,
        ]);
        
        //Pruebas: https://app.outsourcingcos.com/cencosudprueba/recibedatos  Producción: https://app.outsourcingcos.com/cencosud/recibedatos
        $response = $client->request('post', 'https://app.outsourcingcos.com/cencosud/recibedatos', ['body' => json_encode($req_datas)]);
        //PRO: https://pagoscencosud.connect-sos.com/asistencia#openModalThree
        //TEST: http://127.0.0.1:8000/asistencia#openModalTwo

        return redirect()->away('https://pagoscencosud.connect-sos.com/asistencia#openModalTwo');
        

    }
}
