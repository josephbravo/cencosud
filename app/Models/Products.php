<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable =  [
    'id','producto', 'precio', 'meses', 'total'
    ];
}
